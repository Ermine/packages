From f35d7abd4d5d861d71db46a07d351eeb1754d101 Mon Sep 17 00:00:00 2001
From: Samuel Holland <samuel@sholland.org>
Date: Fri, 8 Sep 2017 22:11:14 -0500
Subject: [PATCH 04/13] Remove musl_root and CRT fallback from musl targets

---
 compiler/rustc_codegen_ssa/src/back/link.rs   |  6 +--
 compiler/rustc_target/src/spec/crt_objects.rs | 25 -----------
 .../rustc_target/src/spec/linux_musl_base.rs  |  4 --
 config.example.toml                           | 17 --------
 src/bootstrap/cc_detect.rs                    | 24 +----------
 src/bootstrap/compile.rs                      | 42 +------------------
 src/bootstrap/config.rs                       | 10 -----
 src/bootstrap/configure.py                    | 32 --------------
 src/bootstrap/lib.rs                          | 19 ---------
 src/bootstrap/sanity.rs                       | 23 ----------
 .../dist-i586-gnu-i586-i686-musl/Dockerfile   |  2 -
 .../host-x86_64/dist-various-1/Dockerfile     |  8 ----
 .../host-x86_64/dist-various-2/Dockerfile     |  3 +-
 .../host-x86_64/dist-x86_64-musl/Dockerfile   |  1 -
 .../host-x86_64/test-various/Dockerfile       |  1 -
 15 files changed, 4 insertions(+), 213 deletions(-)

diff --git a/compiler/rustc_codegen_ssa/src/back/link.rs b/compiler/rustc_codegen_ssa/src/back/link.rs
index b603a87874602..1a2c3d219dd3a 100644
--- a/compiler/rustc_codegen_ssa/src/back/link.rs
+++ b/compiler/rustc_codegen_ssa/src/back/link.rs
@@ -1687,7 +1687,7 @@ fn detect_self_contained_mingw(sess: &Session) -> bool {
 /// Various toolchain components used during linking are used from rustc distribution
 /// instead of being found somewhere on the host system.
 /// We only provide such support for a very limited number of targets.
-fn self_contained(sess: &Session, crate_type: CrateType) -> bool {
+fn self_contained(sess: &Session, _crate_type: CrateType) -> bool {
     if let Some(self_contained) = sess.opts.cg.link_self_contained.explicitly_set {
         if sess.target.link_self_contained == LinkSelfContainedDefault::False {
             sess.emit_err(errors::UnsupportedLinkSelfContained);
@@ -1698,10 +1698,6 @@ fn self_contained(sess: &Session, crate_type: CrateType) -> bool {
     match sess.target.link_self_contained {
         LinkSelfContainedDefault::False => false,
         LinkSelfContainedDefault::True => true,
-        // FIXME: Find a better heuristic for "native musl toolchain is available",
-        // based on host and linker path, for example.
-        // (https://github.com/rust-lang/rust/pull/71769#issuecomment-626330237).
-        LinkSelfContainedDefault::Musl => sess.crt_static(Some(crate_type)),
         LinkSelfContainedDefault::Mingw => {
             sess.host == sess.target
                 && sess.target.vendor != "uwp"
diff --git a/compiler/rustc_target/src/spec/crt_objects.rs b/compiler/rustc_target/src/spec/crt_objects.rs
index c126390f5a908..63e3074b5b7c2 100644
--- a/compiler/rustc_target/src/spec/crt_objects.rs
+++ b/compiler/rustc_target/src/spec/crt_objects.rs
@@ -63,28 +63,6 @@ pub(super) fn all(obj: &'static str) -> CrtObjects {
     ])
 }
 
-pub(super) fn pre_musl_self_contained() -> CrtObjects {
-    new(&[
-        (LinkOutputKind::DynamicNoPicExe, &["crt1.o", "crti.o", "crtbegin.o"]),
-        (LinkOutputKind::DynamicPicExe, &["Scrt1.o", "crti.o", "crtbeginS.o"]),
-        (LinkOutputKind::StaticNoPicExe, &["crt1.o", "crti.o", "crtbegin.o"]),
-        (LinkOutputKind::StaticPicExe, &["rcrt1.o", "crti.o", "crtbeginS.o"]),
-        (LinkOutputKind::DynamicDylib, &["crti.o", "crtbeginS.o"]),
-        (LinkOutputKind::StaticDylib, &["crti.o", "crtbeginS.o"]),
-    ])
-}
-
-pub(super) fn post_musl_self_contained() -> CrtObjects {
-    new(&[
-        (LinkOutputKind::DynamicNoPicExe, &["crtend.o", "crtn.o"]),
-        (LinkOutputKind::DynamicPicExe, &["crtendS.o", "crtn.o"]),
-        (LinkOutputKind::StaticNoPicExe, &["crtend.o", "crtn.o"]),
-        (LinkOutputKind::StaticPicExe, &["crtendS.o", "crtn.o"]),
-        (LinkOutputKind::DynamicDylib, &["crtendS.o", "crtn.o"]),
-        (LinkOutputKind::StaticDylib, &["crtendS.o", "crtn.o"]),
-    ])
-}
-
 pub(super) fn pre_mingw_self_contained() -> CrtObjects {
     new(&[
         (LinkOutputKind::DynamicNoPicExe, &["crt2.o", "rsbegin.o"]),
@@ -130,7 +108,6 @@ pub(super) fn post_wasi_self_contained() -> CrtObjects {
 pub enum LinkSelfContainedDefault {
     False,
     True,
-    Musl,
     Mingw,
 }
 
@@ -141,7 +118,6 @@ impl FromStr for LinkSelfContainedDefault {
         Ok(match s {
             "false" => LinkSelfContainedDefault::False,
             "true" | "wasm" => LinkSelfContainedDefault::True,
-            "musl" => LinkSelfContainedDefault::Musl,
             "mingw" => LinkSelfContainedDefault::Mingw,
             _ => return Err(()),
         })
@@ -153,7 +129,6 @@ impl ToJson for LinkSelfContainedDefault {
         match *self {
             LinkSelfContainedDefault::False => "false",
             LinkSelfContainedDefault::True => "true",
-            LinkSelfContainedDefault::Musl => "musl",
             LinkSelfContainedDefault::Mingw => "mingw",
         }
         .to_json()
diff --git a/compiler/rustc_target/src/spec/linux_musl_base.rs b/compiler/rustc_target/src/spec/linux_musl_base.rs
index 61553e71b4500..67d18c886eba5 100644
--- a/compiler/rustc_target/src/spec/linux_musl_base.rs
+++ b/compiler/rustc_target/src/spec/linux_musl_base.rs
@@ -1,13 +1,9 @@
-use crate::spec::crt_objects::{self, LinkSelfContainedDefault};
 use crate::spec::TargetOptions;
 
 pub fn opts() -> TargetOptions {
     let mut base = super::linux_base::opts();
 
     base.env = "musl".into();
-    base.pre_link_objects_self_contained = crt_objects::pre_musl_self_contained();
-    base.post_link_objects_self_contained = crt_objects::post_musl_self_contained();
-    base.link_self_contained = LinkSelfContainedDefault::Musl;
 
     // These targets statically link libc by default
     base.crt_static_default = true;
diff --git a/config.example.toml b/config.example.toml
index 0c65b25fe1389..40a6baca0b903 100644
--- a/config.example.toml
+++ b/config.example.toml
@@ -563,14 +563,6 @@ changelog-seen = 2
 # behavior -- this may lead to miscompilations or other bugs.
 #description = ""
 
-# The root location of the musl installation directory. The library directory
-# will also need to contain libunwind.a for an unwinding implementation. Note
-# that this option only makes sense for musl targets that produce statically
-# linked binaries.
-#
-# Defaults to /usr on musl hosts. Has no default otherwise.
-#musl-root = <platform specific> (path)
-
 # By default the `rustc` executable is built with `-Wl,-rpath` flags on Unix
 # platforms to ensure that the compiler is usable by default from the build
 # directory (as it links to a number of dynamic libraries). This may not be
@@ -767,15 +759,6 @@ changelog-seen = 2
 # only use static libraries. If unset, the target's default linkage is used.
 #crt-static = <platform-specific> (bool)
 
-# The root location of the musl installation directory. The library directory
-# will also need to contain libunwind.a for an unwinding implementation. Note
-# that this option only makes sense for musl targets that produce statically
-# linked binaries.
-#musl-root = build.musl-root (path)
-
-# The full path to the musl libdir.
-#musl-libdir = musl-root/lib
-
 # The root location of the `wasm32-wasi` sysroot. Only used for the
 # `wasm32-wasi` target. If you are building wasm32-wasi target, make sure to
 # create a `[target.wasm32-wasi]` section and move this field there.
diff --git a/src/bootstrap/cc_detect.rs b/src/bootstrap/cc_detect.rs
index ade3bfed11f9c..52007fffca178 100644
--- a/src/bootstrap/cc_detect.rs
+++ b/src/bootstrap/cc_detect.rs
@@ -41,8 +41,6 @@ fn cc2ar(cc: &Path, target: TargetSelection) -> Option<PathBuf> {
         Some(PathBuf::from(ar))
     } else if target.contains("msvc") {
         None
-    } else if target.contains("musl") {
-        Some(PathBuf::from("ar"))
     } else if target.contains("openbsd") {
         Some(PathBuf::from("ar"))
     } else if target.contains("vxworks") {
@@ -166,7 +164,7 @@ fn set_compiler(
     compiler: Language,
     target: TargetSelection,
     config: Option<&Target>,
-    build: &Build,
+    _build: &Build,
 ) {
     match &*target.triple {
         // When compiling for android we may have the NDK configured in the
@@ -202,26 +200,6 @@ fn set_compiler(
             }
         }
 
-        "mips-unknown-linux-musl" => {
-            if cfg.get_compiler().path().to_str() == Some("gcc") {
-                cfg.compiler("mips-linux-musl-gcc");
-            }
-        }
-        "mipsel-unknown-linux-musl" => {
-            if cfg.get_compiler().path().to_str() == Some("gcc") {
-                cfg.compiler("mipsel-linux-musl-gcc");
-            }
-        }
-
-        t if t.contains("musl") => {
-            if let Some(root) = build.musl_root(target) {
-                let guess = root.join("bin/musl-gcc");
-                if guess.exists() {
-                    cfg.compiler(guess);
-                }
-            }
-        }
-
         _ => {}
     }
 }
diff --git a/src/bootstrap/compile.rs b/src/bootstrap/compile.rs
index 14c3ef79a78f2..07b159b4f8264 100644
--- a/src/bootstrap/compile.rs
+++ b/src/bootstrap/compile.rs
@@ -269,38 +269,7 @@ fn copy_self_contained_objects(
 
     // Copies the libc and CRT objects.
     //
-    // rustc historically provides a more self-contained installation for musl targets
-    // not requiring the presence of a native musl toolchain. For example, it can fall back
-    // to using gcc from a glibc-targeting toolchain for linking.
-    // To do that we have to distribute musl startup objects as a part of Rust toolchain
-    // and link with them manually in the self-contained mode.
-    if target.contains("musl") {
-        let srcdir = builder.musl_libdir(target).unwrap_or_else(|| {
-            panic!("Target {:?} does not have a \"musl-libdir\" key", target.triple)
-        });
-        for &obj in &["libc.a", "crt1.o", "Scrt1.o", "rcrt1.o", "crti.o", "crtn.o"] {
-            copy_and_stamp(
-                builder,
-                &libdir_self_contained,
-                &srcdir,
-                obj,
-                &mut target_deps,
-                DependencyType::TargetSelfContained,
-            );
-        }
-        let crt_path = builder.ensure(llvm::CrtBeginEnd { target });
-        for &obj in &["crtbegin.o", "crtbeginS.o", "crtend.o", "crtendS.o"] {
-            let src = crt_path.join(obj);
-            let target = libdir_self_contained.join(obj);
-            builder.copy(&src, &target);
-            target_deps.push((target, DependencyType::TargetSelfContained));
-        }
-
-        if !target.starts_with("s390x") {
-            let libunwind_path = copy_llvm_libunwind(builder, target, &libdir_self_contained);
-            target_deps.push((libunwind_path, DependencyType::TargetSelfContained));
-        }
-    } else if target.ends_with("-wasi") {
+    if target.ends_with("-wasi") {
         let srcdir = builder
             .wasi_root(target)
             .unwrap_or_else(|| {
@@ -397,15 +366,6 @@ pub fn std_cargo(builder: &Builder<'_>, target: TargetSelection, stage: u32, car
             .arg("--manifest-path")
             .arg(builder.src.join("library/sysroot/Cargo.toml"));
 
-        // Help the libc crate compile by assisting it in finding various
-        // sysroot native libraries.
-        if target.contains("musl") {
-            if let Some(p) = builder.musl_libdir(target) {
-                let root = format!("native={}", p.to_str().unwrap());
-                cargo.rustflag("-L").rustflag(&root);
-            }
-        }
-
         if target.ends_with("-wasi") {
             if let Some(p) = builder.wasi_root(target) {
                 let root = format!("native={}/lib/wasm32-wasi", p.to_str().unwrap());
diff --git a/src/bootstrap/config.rs b/src/bootstrap/config.rs
index fe932fd6bd30b..a4a141fff6287 100644
--- a/src/bootstrap/config.rs
+++ b/src/bootstrap/config.rs
@@ -262,8 +262,6 @@ pub struct Config {
     pub print_step_rusage: bool,
     pub missing_tools: bool,
 
-    // Fallback musl-root for all targets
-    pub musl_root: Option<PathBuf>,
     pub prefix: Option<PathBuf>,
     pub sysconfdir: Option<PathBuf>,
     pub datadir: Option<PathBuf>,
@@ -536,8 +534,6 @@ pub struct Target {
     pub profiler: Option<bool>,
     pub rpath: Option<bool>,
     pub crt_static: Option<bool>,
-    pub musl_root: Option<PathBuf>,
-    pub musl_libdir: Option<PathBuf>,
     pub wasi_root: Option<PathBuf>,
     pub qemu_rootfs: Option<PathBuf>,
     pub no_std: bool,
@@ -945,7 +941,6 @@ define_config! {
         default_linker: Option<String> = "default-linker",
         channel: Option<String> = "channel",
         description: Option<String> = "description",
-        musl_root: Option<String> = "musl-root",
         rpath: Option<bool> = "rpath",
         verbose_tests: Option<bool> = "verbose-tests",
         optimize_tests: Option<bool> = "optimize-tests",
@@ -994,8 +989,6 @@ define_config! {
         profiler: Option<bool> = "profiler",
         rpath: Option<bool> = "rpath",
         crt_static: Option<bool> = "crt-static",
-        musl_root: Option<String> = "musl-root",
-        musl_libdir: Option<String> = "musl-libdir",
         wasi_root: Option<String> = "wasi-root",
         qemu_rootfs: Option<String> = "qemu-rootfs",
         no_std: Option<bool> = "no-std",
@@ -1367,7 +1360,6 @@ impl Config {
             set(&mut config.llvm_tools_enabled, rust.llvm_tools);
             config.rustc_parallel = rust.parallel_compiler.unwrap_or(false);
             config.rustc_default_linker = rust.default_linker;
-            config.musl_root = rust.musl_root.map(PathBuf::from);
             config.save_toolstates = rust.save_toolstates.map(PathBuf::from);
             set(
                 &mut config.deny_warnings,
@@ -1534,8 +1526,6 @@ impl Config {
                 target.ranlib = cfg.ranlib.map(PathBuf::from);
                 target.linker = cfg.linker.map(PathBuf::from);
                 target.crt_static = cfg.crt_static;
-                target.musl_root = cfg.musl_root.map(PathBuf::from);
-                target.musl_libdir = cfg.musl_libdir.map(PathBuf::from);
                 target.wasi_root = cfg.wasi_root.map(PathBuf::from);
                 target.qemu_rootfs = cfg.qemu_rootfs.map(PathBuf::from);
                 target.sanitizers = cfg.sanitizers;
diff --git a/src/bootstrap/configure.py b/src/bootstrap/configure.py
index e8eebdfb5a581..17e8c093d9cd2 100755
--- a/src/bootstrap/configure.py
+++ b/src/bootstrap/configure.py
@@ -110,38 +110,6 @@ def v(*args):
   "aarch64-linux-android NDK standalone path")
 v("x86_64-linux-android-ndk", "target.x86_64-linux-android.android-ndk",
   "x86_64-linux-android NDK standalone path")
-v("musl-root", "target.x86_64-unknown-linux-musl.musl-root",
-  "MUSL root installation directory (deprecated)")
-v("musl-root-x86_64", "target.x86_64-unknown-linux-musl.musl-root",
-  "x86_64-unknown-linux-musl install directory")
-v("musl-root-i586", "target.i586-unknown-linux-musl.musl-root",
-  "i586-unknown-linux-musl install directory")
-v("musl-root-i686", "target.i686-unknown-linux-musl.musl-root",
-  "i686-unknown-linux-musl install directory")
-v("musl-root-arm", "target.arm-unknown-linux-musleabi.musl-root",
-  "arm-unknown-linux-musleabi install directory")
-v("musl-root-armhf", "target.arm-unknown-linux-musleabihf.musl-root",
-  "arm-unknown-linux-musleabihf install directory")
-v("musl-root-armv5te", "target.armv5te-unknown-linux-musleabi.musl-root",
-  "armv5te-unknown-linux-musleabi install directory")
-v("musl-root-armv7", "target.armv7-unknown-linux-musleabi.musl-root",
-  "armv7-unknown-linux-musleabi install directory")
-v("musl-root-armv7hf", "target.armv7-unknown-linux-musleabihf.musl-root",
-  "armv7-unknown-linux-musleabihf install directory")
-v("musl-root-aarch64", "target.aarch64-unknown-linux-musl.musl-root",
-  "aarch64-unknown-linux-musl install directory")
-v("musl-root-mips", "target.mips-unknown-linux-musl.musl-root",
-  "mips-unknown-linux-musl install directory")
-v("musl-root-mipsel", "target.mipsel-unknown-linux-musl.musl-root",
-  "mipsel-unknown-linux-musl install directory")
-v("musl-root-mips64", "target.mips64-unknown-linux-muslabi64.musl-root",
-  "mips64-unknown-linux-muslabi64 install directory")
-v("musl-root-mips64el", "target.mips64el-unknown-linux-muslabi64.musl-root",
-  "mips64el-unknown-linux-muslabi64 install directory")
-v("musl-root-riscv32gc", "target.riscv32gc-unknown-linux-musl.musl-root",
-  "riscv32gc-unknown-linux-musl install directory")
-v("musl-root-riscv64gc", "target.riscv64gc-unknown-linux-musl.musl-root",
-  "riscv64gc-unknown-linux-musl install directory")
 v("qemu-armhf-rootfs", "target.arm-unknown-linux-gnueabihf.qemu-rootfs",
   "rootfs in qemu testing, you probably don't want to use this")
 v("qemu-aarch64-rootfs", "target.aarch64-unknown-linux-gnu.qemu-rootfs",
diff --git a/src/bootstrap/lib.rs b/src/bootstrap/lib.rs
index ec092d1b586b4..54185224977d1 100644
--- a/src/bootstrap/lib.rs
+++ b/src/bootstrap/lib.rs
@@ -1256,25 +1256,6 @@ impl Build {
         }
     }
 
-    /// Returns the "musl root" for this `target`, if defined
-    fn musl_root(&self, target: TargetSelection) -> Option<&Path> {
-        self.config
-            .target_config
-            .get(&target)
-            .and_then(|t| t.musl_root.as_ref())
-            .or_else(|| self.config.musl_root.as_ref())
-            .map(|p| &**p)
-    }
-
-    /// Returns the "musl libdir" for this `target`.
-    fn musl_libdir(&self, target: TargetSelection) -> Option<PathBuf> {
-        let t = self.config.target_config.get(&target)?;
-        if let libdir @ Some(_) = &t.musl_libdir {
-            return libdir.clone();
-        }
-        self.musl_root(target).map(|root| root.join("lib"))
-    }
-
     /// Returns the sysroot for the wasi target, if defined
     fn wasi_root(&self, target: TargetSelection) -> Option<&Path> {
         self.config.target_config.get(&target).and_then(|t| t.wasi_root.as_ref()).map(|p| &**p)
diff --git a/src/bootstrap/sanity.rs b/src/bootstrap/sanity.rs
index 8f5ba42736b1f..73192fef219e2 100644
--- a/src/bootstrap/sanity.rs
+++ b/src/bootstrap/sanity.rs
@@ -11,7 +11,6 @@
 use std::collections::HashMap;
 use std::env;
 use std::ffi::{OsStr, OsString};
-use std::fs;
 use std::path::PathBuf;
 use std::process::Command;
 
@@ -205,28 +204,6 @@ than building it.
             }
         }
 
-        // Make sure musl-root is valid
-        if target.contains("musl") {
-            // If this is a native target (host is also musl) and no musl-root is given,
-            // fall back to the system toolchain in /usr before giving up
-            if build.musl_root(*target).is_none() && build.config.build == *target {
-                let target = build.config.target_config.entry(*target).or_default();
-                target.musl_root = Some("/usr".into());
-            }
-            match build.musl_libdir(*target) {
-                Some(libdir) => {
-                    if fs::metadata(libdir.join("libc.a")).is_err() {
-                        panic!("couldn't find libc.a in musl libdir: {}", libdir.display());
-                    }
-                }
-                None => panic!(
-                    "when targeting MUSL either the rust.musl-root \
-                            option or the target.$TARGET.musl-root option must \
-                            be specified in config.toml"
-                ),
-            }
-        }
-
         // Some environments don't want or need these tools, such as when testing Miri.
         // FIXME: it would be better to refactor this code to split necessary setup from pure sanity
         // checks, and have a regular flag for skipping the latter. Also see
diff --git a/src/ci/docker/host-x86_64/dist-i586-gnu-i586-i686-musl/Dockerfile b/src/ci/docker/host-x86_64/dist-i586-gnu-i586-i686-musl/Dockerfile
index a62f98b21d225..f949736e866c4 100644
--- a/src/ci/docker/host-x86_64/dist-i586-gnu-i586-i686-musl/Dockerfile
+++ b/src/ci/docker/host-x86_64/dist-i586-gnu-i586-i686-musl/Dockerfile
@@ -58,8 +58,6 @@ COPY scripts/sccache.sh /scripts/
 RUN sh /scripts/sccache.sh
 
 ENV RUST_CONFIGURE_ARGS \
-      --musl-root-i586=/musl-i586 \
-      --musl-root-i686=/musl-i686 \
       --disable-docs
 
 # Newer binutils broke things on some vms/distros (i.e., linking against
diff --git a/src/ci/docker/host-x86_64/dist-various-1/Dockerfile b/src/ci/docker/host-x86_64/dist-various-1/Dockerfile
index 4576e6d4fa256..e13d579b217f3 100644
--- a/src/ci/docker/host-x86_64/dist-various-1/Dockerfile
+++ b/src/ci/docker/host-x86_64/dist-various-1/Dockerfile
@@ -172,14 +172,6 @@ ENV CFLAGS_armv5te_unknown_linux_musleabi="-march=armv5te -marm -mfloat-abi=soft
     CC_riscv64gc_unknown_none_elf=false
 
 ENV RUST_CONFIGURE_ARGS \
-      --musl-root-armv5te=/musl-armv5te \
-      --musl-root-arm=/musl-arm \
-      --musl-root-armhf=/musl-armhf \
-      --musl-root-armv7hf=/musl-armv7hf \
-      --musl-root-mips=/musl-mips \
-      --musl-root-mipsel=/musl-mipsel \
-      --musl-root-mips64=/musl-mips64 \
-      --musl-root-mips64el=/musl-mips64el \
       --disable-docs
 
 ENV SCRIPT \
diff --git a/src/ci/docker/host-x86_64/dist-various-2/Dockerfile b/src/ci/docker/host-x86_64/dist-various-2/Dockerfile
index 0f5df95a0dd4b..3e0bc3fd33d28 100644
--- a/src/ci/docker/host-x86_64/dist-various-2/Dockerfile
+++ b/src/ci/docker/host-x86_64/dist-various-2/Dockerfile
@@ -135,7 +135,6 @@ ENV TARGETS=$TARGETS,x86_64-unknown-uefi
 RUN ln -s /usr/include/asm-generic /usr/local/include/asm
 
 ENV RUST_CONFIGURE_ARGS --enable-extended --enable-lld --disable-docs \
-  --set target.wasm32-wasi.wasi-root=/wasm32-wasi \
-  --musl-root-armv7=/musl-armv7
+  --set target.wasm32-wasi.wasi-root=/wasm32-wasi
 
 ENV SCRIPT python3 ../x.py dist --host='' --target $TARGETS
diff --git a/src/ci/docker/host-x86_64/dist-x86_64-musl/Dockerfile b/src/ci/docker/host-x86_64/dist-x86_64-musl/Dockerfile
index 6f04dcad9a54f..cf1a4a632e05f 100644
--- a/src/ci/docker/host-x86_64/dist-x86_64-musl/Dockerfile
+++ b/src/ci/docker/host-x86_64/dist-x86_64-musl/Dockerfile
@@ -37,7 +37,6 @@ RUN sh /scripts/sccache.sh
 ENV HOSTS=x86_64-unknown-linux-musl
 
 ENV RUST_CONFIGURE_ARGS \
-      --musl-root-x86_64=/usr/local/x86_64-linux-musl \
       --enable-extended \
       --enable-sanitizers \
       --enable-profiler \
diff --git a/src/ci/docker/host-x86_64/test-various/Dockerfile b/src/ci/docker/host-x86_64/test-various/Dockerfile
index 1dc7b79872438..9a498beded280 100644
--- a/src/ci/docker/host-x86_64/test-various/Dockerfile
+++ b/src/ci/docker/host-x86_64/test-various/Dockerfile
@@ -41,7 +41,6 @@ COPY scripts/sccache.sh /scripts/
 RUN sh /scripts/sccache.sh
 
 ENV RUST_CONFIGURE_ARGS \
-  --musl-root-x86_64=/usr/local/x86_64-linux-musl \
   --set build.nodejs=/node-v15.14.0-linux-x64/bin/node \
   --set rust.lld
 
