# Maintainer: Samuel Holland <samuel@sholland.org>
pkgname=rust
pkgver=1.65.0
_bootver=1.64.0-r0
_llvmver=14
pkgrel=0
pkgdesc="The Rust Programming Language"
url="https://www.rust-lang.org"
arch="all"
options="!check"  # Failures on aarch64 and ppc64.
license="(Apache-2.0 OR MIT) AND (NCSA OR MIT) AND BSD-2-Clause AND BSD-3-Clause"
depends="$pkgname-std=$pkgver-r$pkgrel gcc musl-dev"
makedepends="
	curl-dev
	llvm$_llvmver-dev
	llvm$_llvmver-test-utils
	openssl-dev
	python3
	cargo-bootstrap=$_bootver
	rust-bootstrap=$_bootver
	rustfmt-bootstrap=$_bootver
	zlib-dev
	"
provides="$pkgname-bootstrap=$pkgver-r$pkgrel"
subpackages="
	$pkgname-dbg
	$pkgname-std
	$pkgname-analysis
	$pkgname-doc
	$pkgname-gdb::noarch
	$pkgname-lldb::noarch
	$pkgname-src::noarch
	cargo
	cargo-clippy:_cargo_clippy
	cargo-fmt:_cargo_fmt
	cargo-doc:_cargo_doc:noarch
	cargo-bash-completion:_cargo_bashcomp:noarch
	cargo-zsh-completion:_cargo_zshcomp:noarch
	rustfmt
	"
source="https://static.rust-lang.org/dist/rustc-$pkgver-src.tar.xz
	0001-Fix-LLVM-build.patch
	0002-Fix-linking-to-zlib-when-cross-compiling.patch
	0003-Fix-rustdoc-when-cross-compiling-on-musl.patch
	0004-Use-static-native-libraries-when-linking-static-exec.patch
	0005-Remove-musl_root-and-CRT-fallback-from-musl-targets.patch
	0006-Prefer-libgcc_eh-over-libunwind-for-musl.patch
	0007-Link-libssp_nonshared.a-on-all-musl-targets.patch
	0008-test-failed-doctest-output-Fix-normalization.patch
	0009-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
	0010-test-use-extern-for-plugins-Don-t-assume-multilib.patch
	0011-Ignore-broken-and-non-applicable-tests.patch
	0012-Link-stage-2-tools-dynamically-to-libstd.patch
	0013-Move-debugger-scripts-to-usr-share-rust.patch
	0014-Add-foxkit-target-specs.patch
	0015-Use-OpenPOWER-ABI-on-BE-PowerPC-64-musl.patch
	powerpc-atomics.patch
	"
builddir="$srcdir/rustc-$pkgver-src"
_rlibdir="/usr/lib/rustlib/$CTARGET/lib"

build() {
	cat > config.toml <<- EOF
		changelog-seen = 2
		[build]
		doc-stage = 2
		build-stage = 2
		test-stage = 2
		build = "$CBUILD"
		host = [ "$CHOST" ]
		target = [ "$CTARGET" ]
		cargo = "/usr/bin/cargo"
		rustc = "/usr/bin/rustc"
		rustfmt = "/usr/bin/rustfmt"
		docs = true
		compiler-docs = false
		submodules = false
		python = "python3"
		locked-deps = true
		vendor = true
		extended = true
		tools = [ "analysis", "cargo", "clippy", "rustfmt", "src" ]
		verbose = 1
		sanitizers = false
		profiler = false
		cargo-native-static = false
		[install]
		prefix = "/usr"
		[rust]
		optimize = true
		debug = false
		codegen-units = 1
		debuginfo-level = 2
		debuginfo-level-rustc = 0
		debuginfo-level-tests = 0
		backtrace = true
		incremental = false
		parallel-compiler = false
		channel = "stable"
		description = "Adelie ${pkgver}-r${pkgrel}"
		rpath = false
		verbose-tests = true
		optimize-tests = true
		codegen-tests = true
		dist-src = false
		lld = false
		use-lld = false
		llvm-tools = false
		backtrace-on-ice = true
		remap-debuginfo = false
		jemalloc = false
		llvm-libunwind = "no"
		new-symbol-mangling = true
		[target.$CTARGET]
		cc = "$CTARGET-gcc"
		cxx = "$CTARGET-g++"
		ar = "ar"
		ranlib = "ranlib"
		linker = "$CTARGET-gcc"
		llvm-config = "/usr/lib/llvm$_llvmver/bin/llvm-config"
		crt-static = false
		[dist]
		src-tarball = false
		compression-formats = [ "xz" ]
	EOF

	LLVM_LINK_SHARED=1 \
	RUST_BACKTRACE=1 \
	python3 x.py dist -j ${JOBS:-2}
}

check() {
	LLVM_LINK_SHARED=1 \
	python3 x.py test -j ${JOBS:-2} --no-doc --no-fail-fast || true
}

package() {
	cd "$builddir"/build/dist

	tar xf rust-$pkgver-$CTARGET.tar.xz
	rust-$pkgver-$CTARGET/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--sysconfdir="$pkgdir"/etc \
		--disable-ldconfig
	tar xf rust-src-$pkgver.tar.xz
	rust-src-$pkgver/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--disable-ldconfig

	rm "$pkgdir"/usr/lib/rustlib/components \
	   "$pkgdir"/usr/lib/rustlib/install.log \
	   "$pkgdir"/usr/lib/rustlib/manifest-* \
	   "$pkgdir"/usr/lib/rustlib/rust-installer-version \
	   "$pkgdir"/usr/lib/rustlib/uninstall.sh
}

std() {
	pkgdesc="Standard library for Rust"
	depends="musl-utils"

	_mv "$pkgdir"$_rlibdir/*.so "$subpkgdir"$_rlibdir

	mkdir -p "$subpkgdir"/etc/ld.so.conf.d
	echo "$_rlibdir" > "$subpkgdir"/etc/ld.so.conf.d/$pkgname.conf
}

analysis() {
	pkgdesc="Compiler analysis data for the Rust standard library"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"${_rlibdir%/*}/analysis "$subpkgdir"${_rlibdir%/*}
}

gdb() {
	pkgdesc="GDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname gdb"
	install_if="$pkgname=$pkgver-r$pkgrel gdb"

	_mv "$pkgdir"/usr/bin/rust-gdb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/bin/rust-gdbgui "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/gdb_*.py "$subpkgdir"/usr/share/rust
}

lldb() {
	pkgdesc="LLDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname lldb py3-lldb"
	install_if="$pkgname=$pkgver-r$pkgrel lldb"

	_mv "$pkgdir"/usr/bin/rust-lldb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/lldb_*.py "$subpkgdir"/usr/share/rust
}

src() {
	pkgdesc="$pkgdesc (source code)"
	depends=""

	_mv "$pkgdir"/usr/lib/rustlib/src/rust "$subpkgdir"/usr/src
	rmdir -p "$pkgdir"/usr/lib/rustlib/src 2>/dev/null || true

	mkdir -p "$subpkgdir"/usr/lib/rustlib/src
	ln -s ../../../src/rust "$subpkgdir"/usr/lib/rustlib/src/rust
}

cargo() {
	pkgdesc="The Rust package manager"
	provides="cargo-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel $pkgname"

	_mv "$pkgdir"/usr/bin/cargo "$subpkgdir"/usr/bin
}

_cargo_clippy() {
	pkgdesc="A collection of Rust lints (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo"

	_mv "$pkgdir"/usr/bin/cargo-clippy \
	    "$pkgdir"/usr/bin/clippy-driver \
	    "$subpkgdir"/usr/bin
}

_cargo_fmt() {
	pkgdesc="Format Rust code (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo rustfmt"
	install_if="cargo=$pkgver-r$pkgrel rustfmt=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/cargo-fmt "$subpkgdir"/usr/bin
}

_cargo_bashcomp() {
	pkgdesc="Bash completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel bash-completion"

	_mv "$pkgdir"/etc/bash_completion.d/cargo \
	    "$subpkgdir"/usr/share/bash-completion/completions
	rmdir -p "$pkgdir"/etc/bash_completion.d 2>/dev/null || true
}

_cargo_zshcomp() {
	pkgdesc="ZSH completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel zsh"

	_mv "$pkgdir"/usr/share/zsh/site-functions/_cargo \
	    "$subpkgdir"/usr/share/zsh/site-functions/_cargo
	rmdir -p "$pkgdir"/usr/share/zsh/site-functions 2>/dev/null || true
}

_cargo_doc() {
	pkgdesc="The Rust package manager (documentation)"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel docs"

	# XXX: This is hackish!
	_mv "$pkgdir"/../$pkgname-doc/usr/share/man/man1/cargo* \
	    "$subpkgdir"/usr/share/man/man1
}

rustfmt() {
	pkgdesc="Format Rust code"
	provides="rustfmt-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/rustfmt "$subpkgdir"/usr/bin
}

_mv() {
	local dest; for dest; do true; done  # get last argument
	mkdir -p "$dest"
	mv "$@"
}

sha512sums="3d0369ed3028209c4ecb9b9e7b5f5e3a20be8cc05199675df4f091d62a96c0734bc1dbd7630928fe162792392ec6d0daf9ceed10771531ce022200c7b631e3be  rustc-1.65.0-src.tar.xz
48506ec79a21e344bedd2ddb5767075159b9162576b1c5b040ebc73898cf6d181053694c8364e57b04e6567c927c3f8e93545b58b624a3f7271839ef6449a4a4  0001-Fix-LLVM-build.patch
c97cb96fb4889e6cc5c86ffa1cc6f67db91466204f010aba9f2f2e72914f24939e52c68dbe135b84fcecb01529110fa03e9fa134e824875406dbaa45ef3929c0  0002-Fix-linking-to-zlib-when-cross-compiling.patch
2c04869bd83a0dfa77e26404db36088bab14afda0ba27eee726760f5ea9de40cf16018accdf80b5d4c86ab5e1fb91c5c7f67e8dc4c0734d984f330bfd4b52fed  0003-Fix-rustdoc-when-cross-compiling-on-musl.patch
90b6297eb410ec4ceaf94dc76b100312f93aeec0b46762506cf9631a765c5ee401689c855ab446820b1c1016673766e014c70e05c81ca39649884f9fa0ef66a8  0004-Use-static-native-libraries-when-linking-static-exec.patch
29cc0bc521cf617e2e04518a4cec7557e33634557ad789526cebaf4e1d95bf7e93e5705e1e0805ad5b0f9761db5dcdbf5bc4f96356859d76cd31ec0c8295fa0e  0005-Remove-musl_root-and-CRT-fallback-from-musl-targets.patch
56d12d6f04e67f1c4fc0e0ef3f7a05172e3476db0112b64152c13c929649387a47c14c70e01b9e3805820d5a1f093ee38f7c4931db27f7a751f558451a02d54a  0006-Prefer-libgcc_eh-over-libunwind-for-musl.patch
8a500ba447662e8b271461506627f1c931e247c1b1478482b72859c40e5a7e12dc1dd1cc8e9b21bd5b5bef74f810ed83323190f7135d2fd9e29aabb7b8d84d2f  0007-Link-libssp_nonshared.a-on-all-musl-targets.patch
801caf594b5df06c2540542947622f9e98db79c8f0c43733a3b4532b2917b2192c81431d8d2bff2bc77c271bf257b59965b279ee52ff3cba67d8037a066b737f  0008-test-failed-doctest-output-Fix-normalization.patch
45bcce759138df475f8fbeb8089420bd38a399f5a018ebdf6d5a79e7c714c9ec770c765c32dda0ab4e368c5dd226f474b6894d70b14e41bea57284eeeb1a2f58  0009-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
23f622841ba2030b02f52ed560c4be6800469e8021aa71842a702860c97b90edadf5de593ea4f6b9a158d7d0e4c83b52b419f770a8dac2b69633d643468c54d5  0010-test-use-extern-for-plugins-Don-t-assume-multilib.patch
47f79fee22e163d71e1c4f1817cdb408dd821f241d7c0cc8ee4d401ab63c09ec1851ad5df3224cd62e6d6f0a7c74b206191bcba0af3d8089f692d1b742bfc998  0011-Ignore-broken-and-non-applicable-tests.patch
f1f17ec4be3cb059e7d3e64a3f1e4d099314785c1c4b1ca8a5fcce5f1a0ec5e194d941f649a27196caa4df9d0efb149d4c9d9b0d074d6d4e657f5aab13f55671  0012-Link-stage-2-tools-dynamically-to-libstd.patch
943a15f4b51618ec4c93703623b972a673af3b7cd3221452261762703ade592f95a79bdca098117f3e3d7f190782a2169d75d00ae7f5385d8c3983b27da681fa  0013-Move-debugger-scripts-to-usr-share-rust.patch
346ee97e1909d165e6b38b6f55913e3c693d285ed910a70a82dc79868857a4bc19b0d372cf047232176bbe53e65e9857c5295d4200f3ed451ccb4e83d1fb7bad  0014-Add-foxkit-target-specs.patch
7fee0667793d5d5ee5cb600e24129c81de50511814e8f1a2f16bb47350087c5f42a01671415b10b01e414e9ff03008892a35c6cb616737175217bdccd1fa3f1e  0015-Use-OpenPOWER-ABI-on-BE-PowerPC-64-musl.patch
0dfaed3a4ee2c0d10894d855cfe70eab7a917bbf4b27c820e5b4d3040d219b4bbec582acef2762c558d65ba3770e5144d51c618328a4d1a192ebb4c0f9674cd8  powerpc-atomics.patch"
