# Maintainer: Samuel Holland <samuel@sholland.org>
pkgname=rust
pkgver=1.64.0
_bootver=1.63.0-r0
_llvmver=14
pkgrel=0
pkgdesc="The Rust Programming Language"
url="https://www.rust-lang.org"
arch="all"
options="!check"  # Failures on aarch64 and ppc64.
license="(Apache-2.0 OR MIT) AND (NCSA OR MIT) AND BSD-2-Clause AND BSD-3-Clause"
depends="$pkgname-std=$pkgver-r$pkgrel gcc musl-dev"
makedepends="
	curl-dev
	llvm$_llvmver-dev
	llvm$_llvmver-test-utils
	openssl-dev
	python3
	cargo-bootstrap=$_bootver
	rust-bootstrap=$_bootver
	rustfmt-bootstrap=$_bootver
	zlib-dev
	"
provides="$pkgname-bootstrap=$pkgver-r$pkgrel"
subpackages="
	$pkgname-dbg
	$pkgname-std
	$pkgname-analysis
	$pkgname-doc
	$pkgname-gdb::noarch
	$pkgname-lldb::noarch
	$pkgname-src::noarch
	cargo
	cargo-clippy:_cargo_clippy
	cargo-fmt:_cargo_fmt
	cargo-doc:_cargo_doc:noarch
	cargo-bash-completion:_cargo_bashcomp:noarch
	cargo-zsh-completion:_cargo_zshcomp:noarch
	rls
	rustfmt
	"
source="https://static.rust-lang.org/dist/rustc-$pkgver-src.tar.xz
	0001-Fix-LLVM-build.patch
	0002-Fix-linking-to-zlib-when-cross-compiling.patch
	0003-Fix-rustdoc-when-cross-compiling-on-musl.patch
	0004-Use-static-native-libraries-when-linking-static-exec.patch
	0005-Remove-musl_root-and-CRT-fallback-from-musl-targets.patch
	0006-Prefer-libgcc_eh-over-libunwind-for-musl.patch
	0007-Link-libssp_nonshared.a-on-all-musl-targets.patch
	0008-test-failed-doctest-output-Fix-normalization.patch
	0009-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
	0010-test-use-extern-for-plugins-Don-t-assume-multilib.patch
	0011-Ignore-broken-and-non-applicable-tests.patch
	0012-Link-stage-2-tools-dynamically-to-libstd.patch
	0013-Move-debugger-scripts-to-usr-share-rust.patch
	0014-Add-foxkit-target-specs.patch
	0015-Use-OpenPOWER-ABI-on-BE-PowerPC-64-musl.patch
	0040-rls-atomics.patch
	powerpc-atomics.patch
	"
builddir="$srcdir/rustc-$pkgver-src"
_rlibdir="/usr/lib/rustlib/$CTARGET/lib"

build() {
	cat > config.toml <<- EOF
		changelog-seen = 2
		[build]
		doc-stage = 2
		build-stage = 2
		test-stage = 2
		build = "$CBUILD"
		host = [ "$CHOST" ]
		target = [ "$CTARGET" ]
		cargo = "/usr/bin/cargo"
		rustc = "/usr/bin/rustc"
		rustfmt = "/usr/bin/rustfmt"
		docs = true
		compiler-docs = false
		submodules = false
		python = "python3"
		locked-deps = true
		vendor = true
		extended = true
		tools = [ "analysis", "cargo", "clippy", "rls", "rustfmt", "src" ]
		verbose = 1
		sanitizers = false
		profiler = false
		cargo-native-static = false
		[install]
		prefix = "/usr"
		[rust]
		optimize = true
		debug = false
		codegen-units = 1
		debuginfo-level = 2
		debuginfo-level-rustc = 0
		debuginfo-level-tests = 0
		backtrace = true
		incremental = false
		parallel-compiler = false
		channel = "stable"
		description = "Adelie ${pkgver}-r${pkgrel}"
		rpath = false
		verbose-tests = true
		optimize-tests = true
		codegen-tests = true
		dist-src = false
		lld = false
		use-lld = false
		llvm-tools = false
		backtrace-on-ice = true
		remap-debuginfo = false
		jemalloc = false
		llvm-libunwind = "no"
		new-symbol-mangling = true
		[target.$CTARGET]
		cc = "$CTARGET-gcc"
		cxx = "$CTARGET-g++"
		ar = "ar"
		ranlib = "ranlib"
		linker = "$CTARGET-gcc"
		llvm-config = "/usr/lib/llvm$_llvmver/bin/llvm-config"
		crt-static = false
		[dist]
		src-tarball = false
		compression-formats = [ "xz" ]
	EOF

	LLVM_LINK_SHARED=1 \
	RUST_BACKTRACE=1 \
	python3 x.py dist -j ${JOBS:-2}
}

check() {
	LLVM_LINK_SHARED=1 \
	python3 x.py test -j ${JOBS:-2} --no-doc --no-fail-fast || true
}

package() {
	cd "$builddir"/build/dist

	tar xf rust-$pkgver-$CTARGET.tar.xz
	rust-$pkgver-$CTARGET/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--sysconfdir="$pkgdir"/etc \
		--disable-ldconfig
	tar xf rust-src-$pkgver.tar.xz
	rust-src-$pkgver/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--disable-ldconfig

	rm "$pkgdir"/usr/lib/rustlib/components \
	   "$pkgdir"/usr/lib/rustlib/install.log \
	   "$pkgdir"/usr/lib/rustlib/manifest-* \
	   "$pkgdir"/usr/lib/rustlib/rust-installer-version \
	   "$pkgdir"/usr/lib/rustlib/uninstall.sh
}

std() {
	pkgdesc="Standard library for Rust"
	depends="musl-utils"

	_mv "$pkgdir"$_rlibdir/*.so "$subpkgdir"$_rlibdir

	mkdir -p "$subpkgdir"/etc/ld.so.conf.d
	echo "$_rlibdir" > "$subpkgdir"/etc/ld.so.conf.d/$pkgname.conf
}

analysis() {
	pkgdesc="Compiler analysis data for the Rust standard library"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"${_rlibdir%/*}/analysis "$subpkgdir"${_rlibdir%/*}
}

gdb() {
	pkgdesc="GDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname gdb"
	install_if="$pkgname=$pkgver-r$pkgrel gdb"

	_mv "$pkgdir"/usr/bin/rust-gdb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/bin/rust-gdbgui "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/gdb_*.py "$subpkgdir"/usr/share/rust
}

lldb() {
	pkgdesc="LLDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname lldb py3-lldb"
	install_if="$pkgname=$pkgver-r$pkgrel lldb"

	_mv "$pkgdir"/usr/bin/rust-lldb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/lldb_*.py "$subpkgdir"/usr/share/rust
}

src() {
	pkgdesc="$pkgdesc (source code)"
	depends=""

	_mv "$pkgdir"/usr/lib/rustlib/src/rust "$subpkgdir"/usr/src
	rmdir -p "$pkgdir"/usr/lib/rustlib/src 2>/dev/null || true

	mkdir -p "$subpkgdir"/usr/lib/rustlib/src
	ln -s ../../../src/rust "$subpkgdir"/usr/lib/rustlib/src/rust
}

cargo() {
	pkgdesc="The Rust package manager"
	provides="cargo-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel $pkgname"

	_mv "$pkgdir"/usr/bin/cargo "$subpkgdir"/usr/bin
}

_cargo_clippy() {
	pkgdesc="A collection of Rust lints (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo"

	_mv "$pkgdir"/usr/bin/cargo-clippy \
	    "$pkgdir"/usr/bin/clippy-driver \
	    "$subpkgdir"/usr/bin
}

_cargo_fmt() {
	pkgdesc="Format Rust code (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo rustfmt"
	install_if="cargo=$pkgver-r$pkgrel rustfmt=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/cargo-fmt "$subpkgdir"/usr/bin
}

_cargo_bashcomp() {
	pkgdesc="Bash completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel bash-completion"

	_mv "$pkgdir"/etc/bash_completion.d/cargo \
	    "$subpkgdir"/usr/share/bash-completion/completions
	rmdir -p "$pkgdir"/etc/bash_completion.d 2>/dev/null || true
}

_cargo_zshcomp() {
	pkgdesc="ZSH completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel zsh"

	_mv "$pkgdir"/usr/share/zsh/site-functions/_cargo \
	    "$subpkgdir"/usr/share/zsh/site-functions/_cargo
	rmdir -p "$pkgdir"/usr/share/zsh/site-functions 2>/dev/null || true
}

_cargo_doc() {
	pkgdesc="The Rust package manager (documentation)"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel docs"

	# XXX: This is hackish!
	_mv "$pkgdir"/../$pkgname-doc/usr/share/man/man1/cargo* \
	    "$subpkgdir"/usr/share/man/man1
}

rls() {
	pkgdesc="The Rust language server"
	license="Apache-2.0 OR MIT"
	depends="$pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/rls "$subpkgdir"/usr/bin
}

rustfmt() {
	pkgdesc="Format Rust code"
	provides="rustfmt-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/rustfmt "$subpkgdir"/usr/bin
}

_mv() {
	local dest; for dest; do true; done  # get last argument
	mkdir -p "$dest"
	mv "$@"
}

sha512sums="919f40acd8c6eaaef399aa3248503bea19feb96697ab221aaede9ee789ce340b47cb899d1e0e41a31e5d7756653968a10d2faaa4aee83294c9f1243949b43516  rustc-1.64.0-src.tar.xz
5365df6b12998295525c9e27fd0184f650f842f4440174a69893e0019d69927bcd00ea9ce34d64156ea25631f845ff2b8a5741996452a661778f7006d3eebc02  0001-Fix-LLVM-build.patch
eaa1f1380ab3d76eba557fa65b6127248b2506e0c3291d814b774e952046a2c0a9f1c70ccf7f59101415bc34e9db546ef6260910e521ee958c99acbecf80426f  0002-Fix-linking-to-zlib-when-cross-compiling.patch
30992ee8b8def58bb1c226539b1eebe210c20ad8f9067a3aa01bac0bee9c56eb2310fe95e299b7341251ecd3aa7917bece29f8051dc0b846e7dc5420e3aa046a  0003-Fix-rustdoc-when-cross-compiling-on-musl.patch
6253ffcf264fa1c360ced90e0a1b9a993e27ec60d622258bbf6a32e860b72f687242dd813500f7e0aebf9e243508fc7dd2cbdeffdec1974c5c4c6790e5b38598  0004-Use-static-native-libraries-when-linking-static-exec.patch
6d47a835526696e9598e9fb10449d8441354855648dc39e75ae873e4cc5f3574fe77ec1e52048a448b1c1e95644cde618953eda4d8fe3eeca6e17d0f5e25c17b  0005-Remove-musl_root-and-CRT-fallback-from-musl-targets.patch
56d12d6f04e67f1c4fc0e0ef3f7a05172e3476db0112b64152c13c929649387a47c14c70e01b9e3805820d5a1f093ee38f7c4931db27f7a751f558451a02d54a  0006-Prefer-libgcc_eh-over-libunwind-for-musl.patch
8a500ba447662e8b271461506627f1c931e247c1b1478482b72859c40e5a7e12dc1dd1cc8e9b21bd5b5bef74f810ed83323190f7135d2fd9e29aabb7b8d84d2f  0007-Link-libssp_nonshared.a-on-all-musl-targets.patch
801caf594b5df06c2540542947622f9e98db79c8f0c43733a3b4532b2917b2192c81431d8d2bff2bc77c271bf257b59965b279ee52ff3cba67d8037a066b737f  0008-test-failed-doctest-output-Fix-normalization.patch
45bcce759138df475f8fbeb8089420bd38a399f5a018ebdf6d5a79e7c714c9ec770c765c32dda0ab4e368c5dd226f474b6894d70b14e41bea57284eeeb1a2f58  0009-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
23f622841ba2030b02f52ed560c4be6800469e8021aa71842a702860c97b90edadf5de593ea4f6b9a158d7d0e4c83b52b419f770a8dac2b69633d643468c54d5  0010-test-use-extern-for-plugins-Don-t-assume-multilib.patch
73282af6c50155a29c2c98117a1ce6d28be42d04c25dc8ea2388de6923361fb7ae44d16bb81c57bb3d326d434f11ae6cdf72b8a0293421e8d0eaa931398e52d3  0011-Ignore-broken-and-non-applicable-tests.patch
93c7de0ed8e842c6fbc7510cb407df3d0650b4c99d3eb52ee8b1b7470a8a4c203c8706f96450b4f7af7b6987eff16079d43ffa112aae710ee4249c8382d5546c  0012-Link-stage-2-tools-dynamically-to-libstd.patch
f522045164be93fccaa711d7d8852c73fe8b205d806cd4ead3b17f1c63b9b813d5bf177e0016d9812fc8b14ae5f24d4b357f0b4c0086cd8f66d7842f5883be3b  0013-Move-debugger-scripts-to-usr-share-rust.patch
63c995cf444e822246b2d57e2996c6bdc8f612eca1786677c1cefb8fa391f432b3829db7c8757f44f727a5793ffc4d02f231cc23ee6bdeef1c48f7c331f97791  0014-Add-foxkit-target-specs.patch
7fee0667793d5d5ee5cb600e24129c81de50511814e8f1a2f16bb47350087c5f42a01671415b10b01e414e9ff03008892a35c6cb616737175217bdccd1fa3f1e  0015-Use-OpenPOWER-ABI-on-BE-PowerPC-64-musl.patch
a2a05a64ca57dcde23c11ed8bd6ce530d80b99464c895c256b4e9cba02997121205a091de43f516563d5d618ea522d89639507ba94961807ada743a3099fe8d8  0040-rls-atomics.patch
e2a16c00658944e840825fee841e5e18cb5600bd26d33a508989e31d12ae192fdddd26ac9c04dd7167a072062f135e889aad0c0f170f03de8886ceaea3c0bffa  powerpc-atomics.patch"
