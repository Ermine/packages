# Maintainer: Samuel Holland <samuel@sholland.org>
pkgname=rust
pkgver=1.71.1
_bootver=1.70.0-r0
_llvmver=14
pkgrel=0
pkgdesc="The Rust Programming Language"
url="https://www.rust-lang.org"
arch="all"
options="!check"  # Failures on aarch64 and ppc64.
license="(Apache-2.0 OR MIT) AND (NCSA OR MIT) AND BSD-2-Clause AND BSD-3-Clause"
depends="$pkgname-std=$pkgver-r$pkgrel gcc musl-dev"
makedepends="
	curl-dev
	llvm$_llvmver-dev
	llvm$_llvmver-test-utils
	openssl-dev
	python3
	cargo-bootstrap=$_bootver
	rust-bootstrap=$_bootver
	rustfmt-bootstrap=$_bootver
	zlib-dev
	"
provides="$pkgname-bootstrap=$pkgver-r$pkgrel"
subpackages="
	$pkgname-dbg
	$pkgname-std
	$pkgname-analysis
	$pkgname-doc
	$pkgname-gdb::noarch
	$pkgname-lldb::noarch
	$pkgname-src::noarch
	cargo
	cargo-clippy:_cargo_clippy
	cargo-fmt:_cargo_fmt
	cargo-doc:_cargo_doc:noarch
	cargo-bash-completion:_cargo_bashcomp:noarch
	cargo-zsh-completion:_cargo_zshcomp:noarch
	rustfmt
	"
source="https://static.rust-lang.org/dist/rustc-$pkgver-src.tar.xz
	0001-Fix-LLVM-build.patch
	0002-Fix-linking-to-zlib-when-cross-compiling.patch
	0003-Fix-rustdoc-when-cross-compiling-on-musl.patch
	0004-Remove-musl_root-and-CRT-fallback-from-musl-targets.patch
	0005-Prefer-libgcc_eh-over-libunwind-for-musl.patch
	0006-Link-libssp_nonshared.a-on-all-musl-targets.patch
	0007-test-failed-doctest-output-Fix-normalization.patch
	0008-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
	0009-test-use-extern-for-plugins-Don-t-assume-multilib.patch
	0010-Ignore-broken-and-non-applicable-tests.patch
	0011-Link-stage-2-tools-dynamically-to-libstd.patch
	0012-Move-debugger-scripts-to-usr-share-rust.patch
	0013-Add-foxkit-target-specs.patch
	powerpc-atomics.patch
	ppc-libc-hugetlb.patch
	"
builddir="$srcdir/rustc-$pkgver-src"
_rlibdir="/usr/lib/rustlib/$CTARGET/lib"

build() {
	cat > config.toml <<- EOF
		changelog-seen = 2
		[build]
		doc-stage = 2
		build-stage = 2
		test-stage = 2
		build = "$CBUILD"
		host = [ "$CHOST" ]
		target = [ "$CTARGET" ]
		cargo = "/usr/bin/cargo"
		rustc = "/usr/bin/rustc"
		rustfmt = "/usr/bin/rustfmt"
		docs = true
		compiler-docs = false
		submodules = false
		python = "python3"
		locked-deps = true
		vendor = true
		extended = true
		tools = [ "analysis", "cargo", "clippy", "rustfmt", "src" ]
		verbose = 1
		sanitizers = false
		profiler = false
		cargo-native-static = false
		[install]
		prefix = "/usr"
		[rust]
		optimize = true
		debug = false
		codegen-units = 1
		debuginfo-level = 1
		debuginfo-level-rustc = 0
		debuginfo-level-tests = 0
		backtrace = true
		incremental = false
		parallel-compiler = false
		channel = "stable"
		description = "Adelie ${pkgver}-r${pkgrel}"
		rpath = false
		verbose-tests = true
		optimize-tests = true
		codegen-tests = true
		dist-src = false
		lld = false
		use-lld = false
		llvm-tools = false
		backtrace-on-ice = true
		remap-debuginfo = false
		jemalloc = false
		llvm-libunwind = "no"
		new-symbol-mangling = true
		[target.$CTARGET]
		cc = "$CTARGET-gcc"
		cxx = "$CTARGET-g++"
		ar = "ar"
		ranlib = "ranlib"
		linker = "$CTARGET-gcc"
		llvm-config = "/usr/lib/llvm$_llvmver/bin/llvm-config"
		crt-static = false
		[dist]
		src-tarball = false
		compression-formats = [ "xz" ]
	EOF

	LLVM_LINK_SHARED=1 \
	RUST_BACKTRACE=1 \
	python3 x.py dist -j ${JOBS:-2}
}

check() {
	LLVM_LINK_SHARED=1 \
	python3 x.py test -j ${JOBS:-2} --no-doc --no-fail-fast || true
}

package() {
	cd "$builddir"/build/dist

	tar xf rust-$pkgver-$CTARGET.tar.xz
	rust-$pkgver-$CTARGET/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--sysconfdir="$pkgdir"/etc \
		--disable-ldconfig
	tar xf rust-src-$pkgver.tar.xz
	rust-src-$pkgver/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--disable-ldconfig

	rm "$pkgdir"/usr/lib/rustlib/components \
	   "$pkgdir"/usr/lib/rustlib/install.log \
	   "$pkgdir"/usr/lib/rustlib/manifest-* \
	   "$pkgdir"/usr/lib/rustlib/rust-installer-version \
	   "$pkgdir"/usr/lib/rustlib/uninstall.sh
}

std() {
	pkgdesc="Standard library for Rust"
	depends="musl-utils"

	_mv "$pkgdir"$_rlibdir/*.so "$subpkgdir"$_rlibdir

	mkdir -p "$subpkgdir"/etc/ld.so.conf.d
	echo "$_rlibdir" > "$subpkgdir"/etc/ld.so.conf.d/$pkgname.conf
}

analysis() {
	pkgdesc="Compiler analysis data for the Rust standard library"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"${_rlibdir%/*}/analysis "$subpkgdir"${_rlibdir%/*}
}

gdb() {
	pkgdesc="GDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname gdb"
	install_if="$pkgname=$pkgver-r$pkgrel gdb"

	_mv "$pkgdir"/usr/bin/rust-gdb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/bin/rust-gdbgui "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/gdb_*.py "$subpkgdir"/usr/share/rust
}

lldb() {
	pkgdesc="LLDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname lldb py3-lldb"
	install_if="$pkgname=$pkgver-r$pkgrel lldb"

	_mv "$pkgdir"/usr/bin/rust-lldb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/lldb_*.py "$subpkgdir"/usr/share/rust
}

src() {
	pkgdesc="$pkgdesc (source code)"
	depends=""

	_mv "$pkgdir"/usr/lib/rustlib/src/rust "$subpkgdir"/usr/src
	rmdir -p "$pkgdir"/usr/lib/rustlib/src 2>/dev/null || true

	mkdir -p "$subpkgdir"/usr/lib/rustlib/src
	ln -s ../../../src/rust "$subpkgdir"/usr/lib/rustlib/src/rust
}

cargo() {
	pkgdesc="The Rust package manager"
	provides="cargo-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel $pkgname"

	_mv "$pkgdir"/usr/bin/cargo "$subpkgdir"/usr/bin
}

_cargo_clippy() {
	pkgdesc="A collection of Rust lints (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo"

	_mv "$pkgdir"/usr/bin/cargo-clippy \
	    "$pkgdir"/usr/bin/clippy-driver \
	    "$subpkgdir"/usr/bin
}

_cargo_fmt() {
	pkgdesc="Format Rust code (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo rustfmt"
	install_if="cargo=$pkgver-r$pkgrel rustfmt=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/cargo-fmt "$subpkgdir"/usr/bin
}

_cargo_bashcomp() {
	pkgdesc="Bash completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel bash-completion"

	_mv "$pkgdir"/etc/bash_completion.d/cargo \
	    "$subpkgdir"/usr/share/bash-completion/completions
	rmdir -p "$pkgdir"/etc/bash_completion.d 2>/dev/null || true
}

_cargo_zshcomp() {
	pkgdesc="ZSH completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel zsh"

	_mv "$pkgdir"/usr/share/zsh/site-functions/_cargo \
	    "$subpkgdir"/usr/share/zsh/site-functions/_cargo
	rmdir -p "$pkgdir"/usr/share/zsh/site-functions 2>/dev/null || true
}

_cargo_doc() {
	pkgdesc="The Rust package manager (documentation)"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel docs"

	# XXX: This is hackish!
	_mv "$pkgdir"/../$pkgname-doc/usr/share/man/man1/cargo* \
	    "$subpkgdir"/usr/share/man/man1
}

rustfmt() {
	pkgdesc="Format Rust code"
	provides="rustfmt-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/rustfmt "$subpkgdir"/usr/bin
}

_mv() {
	local dest; for dest; do true; done  # get last argument
	mkdir -p "$dest"
	mv "$@"
}

sha512sums="fd0e5a16bdbeb539184513583089e55f681cb772810df357b6b1464853f7022ac02edab3dd155b2262ed0047e2a25dea3808dd078dcdfce9d399384465009db4  rustc-1.71.1-src.tar.xz
997a2e657708e0bb2adc2e565c63245de378eaeb17f645a96bf1c34f2deae9767bd1ce8241c9c625cf22da0227e77945590d9ac78fea172e14503d9a49bb611f  0001-Fix-LLVM-build.patch
95d76d4de26cfb49968e50f78d772d5229bab42bb3d5126c844f61d17a5ac5e8bb8f0fbc06c8f45e64eceda760d7654fb8bdf70e88708dad749c5562f3923974  0002-Fix-linking-to-zlib-when-cross-compiling.patch
c948ff8a183a5688335f3dd17b384fb98d70e1dc34bbaaedbd7a83ef7cab880b846e3c64c96c76c69cac176cfd5773e281ec35fee45636a68689ed3585139ca2  0003-Fix-rustdoc-when-cross-compiling-on-musl.patch
3edcfa8c233fbb23f144eb597b41914eb5bd61863e533281aa4dc16e9f1684e5030cfc0350043210b625128a0a040d71797e01427dd65dd0dca26c0ec0da6bf8  0004-Remove-musl_root-and-CRT-fallback-from-musl-targets.patch
33c1d5b2f051f4ccfde4cdd9cfed4a97b23ccc50aac2eed300b989fff8e1ad55e0ab140c128d33fca25b5b54cfb33b948bc6a139f93d232661e07bf4eba1dc24  0005-Prefer-libgcc_eh-over-libunwind-for-musl.patch
3cf4663242bfd6c67a644a2c6e9c49d26fb2dc93b8907daa86880adb9f57a3db222121a68004aae3f048da59d123ace301e28c63ea80a80f61f4b63f8089e497  0006-Link-libssp_nonshared.a-on-all-musl-targets.patch
5cf31dfaa0604bbc45ac85a78f0d2326c23a57f66e7c4fa1efe5b9f5967c8a3120dad921c70cdbcdf5e6038c22accf56cd34c0c15e576dda7691c2de3258d7cc  0007-test-failed-doctest-output-Fix-normalization.patch
bd032bda35b5881e04ea3fe335818fee63087ca26f76f2c1e494c135d9186de021e77a8c36d47f60dffe4e85217ab24775601031d6608a08559d4b27f1ca6da8  0008-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
390e3b3569054ee4719959c0fc9a4aa1b28bae9f4cb54abae28f0313caa7c9ee2393e0877d6ed49ca54673fa162ab8e29113d97ed8f758d0f541cf57aa55e9cb  0009-test-use-extern-for-plugins-Don-t-assume-multilib.patch
faa6f2c3c94140798b476a794d77a304c0f3891a65c1ffe99666606cfe822d5651af6b8c011265a7f1114ade355994761d71aac550b8dd8617f44d500435e8e9  0010-Ignore-broken-and-non-applicable-tests.patch
4f475f0c6774a67a3297c3ded23453fd664885598d0d8f5807299d8807d1bbce5ff7c676ab96ef2eaf40c314d07584a019f297f8d0c75af32a0b9b4ecc9d7f85  0011-Link-stage-2-tools-dynamically-to-libstd.patch
fc4b384ccff9d4a2a8e407c9944b16bd6d69a8c69f8cbe9f366793a1d1198bdc2b7c82f58706240bd8c418d4bdc88b73d1656ba2a1f0a7b280e53e3ea40fb3fb  0012-Move-debugger-scripts-to-usr-share-rust.patch
81d24ec33264e595dda481c0876fd55cad1fc11815d5634dfa674254109c7d815b5a42147cfb3eae07ff44ae6029022a04bbebdb207bc7701ab0e05bc777b115  0013-Add-foxkit-target-specs.patch
e10b528a97d205eb32512d0d7f18f2dada984abd87f5370c0b6d12cd9e892f3ab75384fadd748a0a5c32057f13c32f96061bedd05abd512da8eae58236913aef  powerpc-atomics.patch
263cacaeafd3ff7034bb48375635f524c963dadb9cba9cbca3b9b5333f2ddd4d8b0054b730bdbfce3fd060266ce643464ecda0b253073099259acee246bf8b00  ppc-libc-hugetlb.patch"
