# Maintainer: Samuel Holland <samuel@sholland.org>
pkgname=rust
pkgver=1.61.0
_bootver=1.60.0-r0
_llvmver=14
pkgrel=0
pkgdesc="The Rust Programming Language"
url="https://www.rust-lang.org"
arch="all"
options="!check"  # Failures on aarch64 and ppc64.
license="(Apache-2.0 OR MIT) AND (NCSA OR MIT) AND BSD-2-Clause AND BSD-3-Clause"
depends="$pkgname-std=$pkgver-r$pkgrel gcc musl-dev"
makedepends="
	curl-dev
	llvm$_llvmver-dev
	llvm$_llvmver-test-utils
	openssl-dev
	python3
	cargo-bootstrap=$_bootver
	rust-bootstrap=$_bootver
	rustfmt-bootstrap=$_bootver
	zlib-dev
	"
provides="$pkgname-bootstrap=$pkgver-r$pkgrel"
subpackages="
	$pkgname-dbg
	$pkgname-std
	$pkgname-analysis
	$pkgname-doc
	$pkgname-gdb::noarch
	$pkgname-lldb::noarch
	$pkgname-src::noarch
	cargo
	cargo-clippy:_cargo_clippy
	cargo-fmt:_cargo_fmt
	cargo-doc:_cargo_doc:noarch
	cargo-bash-completion:_cargo_bashcomp:noarch
	cargo-zsh-completion:_cargo_zshcomp:noarch
	rls
	rustfmt
	"
source="https://static.rust-lang.org/dist/rustc-$pkgver-src.tar.xz
	0001-Fix-LLVM-build.patch
	0002-Fix-linking-to-zlib-when-cross-compiling.patch
	0003-Fix-rustdoc-when-cross-compiling-on-musl.patch
	0004-Use-static-native-libraries-when-linking-static-exec.patch
	0005-Remove-musl_root-and-CRT-fallback-from-musl-targets.patch
	0006-Prefer-libgcc_eh-over-libunwind-for-musl.patch
	0007-Link-libssp_nonshared.a-on-all-musl-targets.patch
	0008-test-failed-doctest-output-Fix-normalization.patch
	0009-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
	0010-test-use-extern-for-plugins-Don-t-assume-multilib.patch
	0011-Ignore-broken-and-non-applicable-tests.patch
	0012-Link-stage-2-tools-dynamically-to-libstd.patch
	0013-Move-debugger-scripts-to-usr-share-rust.patch
	0014-Add-foxkit-target-specs.patch
	0015-Use-OpenPOWER-ABI-on-BE-PowerPC-64-musl.patch
	0040-rls-atomics.patch
	powerpc-atomics.patch
	"
builddir="$srcdir/rustc-$pkgver-src"
_rlibdir="/usr/lib/rustlib/$CTARGET/lib"

build() {
	cat > config.toml <<- EOF
		changelog-seen = 2
		[build]
		doc-stage = 2
		build-stage = 2
		test-stage = 2
		build = "$CBUILD"
		host = [ "$CHOST" ]
		target = [ "$CTARGET" ]
		cargo = "/usr/bin/cargo"
		rustc = "/usr/bin/rustc"
		rustfmt = "/usr/bin/rustfmt"
		docs = true
		compiler-docs = false
		submodules = false
		python = "python3"
		locked-deps = true
		vendor = true
		extended = true
		tools = [ "analysis", "cargo", "clippy", "rls", "rustfmt", "src" ]
		verbose = 1
		sanitizers = false
		profiler = false
		cargo-native-static = false
		[install]
		prefix = "/usr"
		[rust]
		optimize = true
		debug = false
		codegen-units = 1
		debuginfo-level = 2
		debuginfo-level-rustc = 0
		debuginfo-level-tests = 0
		backtrace = true
		incremental = false
		parallel-compiler = false
		channel = "stable"
		description = "Adelie ${pkgver}-r${pkgrel}"
		rpath = false
		verbose-tests = true
		optimize-tests = true
		codegen-tests = true
		dist-src = false
		lld = false
		use-lld = false
		llvm-tools = false
		backtrace-on-ice = true
		remap-debuginfo = false
		jemalloc = false
		llvm-libunwind = "no"
		new-symbol-mangling = true
		[target.$CTARGET]
		cc = "$CTARGET-gcc"
		cxx = "$CTARGET-g++"
		ar = "ar"
		ranlib = "ranlib"
		linker = "$CTARGET-gcc"
		llvm-config = "/usr/lib/llvm$_llvmver/bin/llvm-config"
		crt-static = false
		[dist]
		src-tarball = false
		compression-formats = [ "xz" ]
	EOF

	LLVM_LINK_SHARED=1 \
	RUST_BACKTRACE=1 \
	python3 x.py dist -j ${JOBS:-2}
}

check() {
	LLVM_LINK_SHARED=1 \
	python3 x.py test -j ${JOBS:-2} --no-doc --no-fail-fast
}

package() {
	cd "$builddir"/build/dist

	tar xf rust-$pkgver-$CTARGET.tar.xz
	rust-$pkgver-$CTARGET/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--sysconfdir="$pkgdir"/etc \
		--disable-ldconfig
	tar xf rust-src-$pkgver.tar.xz
	rust-src-$pkgver/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--disable-ldconfig

	rm "$pkgdir"/usr/lib/rustlib/components \
	   "$pkgdir"/usr/lib/rustlib/install.log \
	   "$pkgdir"/usr/lib/rustlib/manifest-* \
	   "$pkgdir"/usr/lib/rustlib/rust-installer-version \
	   "$pkgdir"/usr/lib/rustlib/uninstall.sh
}

std() {
	pkgdesc="Standard library for Rust"
	depends="musl-utils"

	_mv "$pkgdir"$_rlibdir/*.so "$subpkgdir"$_rlibdir

	mkdir -p "$subpkgdir"/etc/ld.so.conf.d
	echo "$_rlibdir" > "$subpkgdir"/etc/ld.so.conf.d/$pkgname.conf
}

analysis() {
	pkgdesc="Compiler analysis data for the Rust standard library"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"${_rlibdir%/*}/analysis "$subpkgdir"${_rlibdir%/*}
}

gdb() {
	pkgdesc="GDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname gdb"
	install_if="$pkgname=$pkgver-r$pkgrel gdb"

	_mv "$pkgdir"/usr/bin/rust-gdb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/bin/rust-gdbgui "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/gdb_*.py "$subpkgdir"/usr/share/rust
}

lldb() {
	pkgdesc="LLDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname lldb py3-lldb"
	install_if="$pkgname=$pkgver-r$pkgrel lldb"

	_mv "$pkgdir"/usr/bin/rust-lldb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/lldb_*.py "$subpkgdir"/usr/share/rust
}

src() {
	pkgdesc="$pkgdesc (source code)"
	depends=""

	_mv "$pkgdir"/usr/lib/rustlib/src/rust "$subpkgdir"/usr/src
	rmdir -p "$pkgdir"/usr/lib/rustlib/src 2>/dev/null || true

	mkdir -p "$subpkgdir"/usr/lib/rustlib/src
	ln -s ../../../src/rust "$subpkgdir"/usr/lib/rustlib/src/rust
}

cargo() {
	pkgdesc="The Rust package manager"
	provides="cargo-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel $pkgname"

	_mv "$pkgdir"/usr/bin/cargo "$subpkgdir"/usr/bin
}

_cargo_clippy() {
	pkgdesc="A collection of Rust lints (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo"

	_mv "$pkgdir"/usr/bin/cargo-clippy \
	    "$pkgdir"/usr/bin/clippy-driver \
	    "$subpkgdir"/usr/bin
}

_cargo_fmt() {
	pkgdesc="Format Rust code (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo rustfmt"
	install_if="cargo=$pkgver-r$pkgrel rustfmt=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/cargo-fmt "$subpkgdir"/usr/bin
}

_cargo_bashcomp() {
	pkgdesc="Bash completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel bash-completion"

	_mv "$pkgdir"/etc/bash_completion.d/cargo \
	    "$subpkgdir"/usr/share/bash-completion/completions
	rmdir -p "$pkgdir"/etc/bash_completion.d 2>/dev/null || true
}

_cargo_zshcomp() {
	pkgdesc="ZSH completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel zsh"

	_mv "$pkgdir"/usr/share/zsh/site-functions/_cargo \
	    "$subpkgdir"/usr/share/zsh/site-functions/_cargo
	rmdir -p "$pkgdir"/usr/share/zsh/site-functions 2>/dev/null || true
}

_cargo_doc() {
	pkgdesc="The Rust package manager (documentation)"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel docs"

	# XXX: This is hackish!
	_mv "$pkgdir"/../$pkgname-doc/usr/share/man/man1/cargo* \
	    "$subpkgdir"/usr/share/man/man1
}

rls() {
	pkgdesc="The Rust language server"
	license="Apache-2.0 OR MIT"
	depends="$pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/rls "$subpkgdir"/usr/bin
}

rustfmt() {
	pkgdesc="Format Rust code"
	provides="rustfmt-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/rustfmt "$subpkgdir"/usr/bin
}

_mv() {
	local dest; for dest; do true; done  # get last argument
	mkdir -p "$dest"
	mv "$@"
}

sha512sums="9bbdcc1c37f7b889be0c8d195963b4c3b65267a63ea309315eda12bd3b2cd36e0060301805dfc7da440c0a8702d085ccb0bf763155b27035a078769333637fb3  rustc-1.61.0-src.tar.xz
c9bd75fbfe4a752ebc27cffbdf0fdb2e46d42d4ab76820328ce4cdac7ff955694a6c7b96c03f40f36617e943e7c62481974ce2a500d06f0c9bce7873ff49e877  0001-Fix-LLVM-build.patch
fa9b0eb2e149ebda40db28e4d41d4cc9da899818ced379f6469a9ac638928ced09473d8d5fefe776cca3107d98de3e33db9dee5d8b9ca234586c004d46d6070c  0002-Fix-linking-to-zlib-when-cross-compiling.patch
7d174cd27e618212e534459455b9017636f56e9a9db81413a714f718a988a9d1fc7b4c0b98d4a2f0b1971d662c7df98be370e0d10e81d3e50d37589fe3b671fa  0003-Fix-rustdoc-when-cross-compiling-on-musl.patch
d1a63e5b2b3a93f58cde29f6e54390a1a6860e23bdd19a550ab8a5da6f4c0c23ff7fb9926f4efccf3a5f85ae78a443ec0442d27ccbc712ff0dc6f6b10a63c56d  0004-Use-static-native-libraries-when-linking-static-exec.patch
a497b9a0b6a08293187f8a80384e5f8c594d219ee1a67e31caa34c3f459910db7ed1b1bfa9cadfa7da4eef71fea8ca263126158bc5e658825533869c32f0f5b4  0005-Remove-musl_root-and-CRT-fallback-from-musl-targets.patch
65116731811c178e980b1d980d524645897bc3f81e9f9ad8051235fa4bad7bbf20519173057e9d7b3f9c8459cc35dd2902c7ccd9e5070587848ea9886793ea35  0006-Prefer-libgcc_eh-over-libunwind-for-musl.patch
8a500ba447662e8b271461506627f1c931e247c1b1478482b72859c40e5a7e12dc1dd1cc8e9b21bd5b5bef74f810ed83323190f7135d2fd9e29aabb7b8d84d2f  0007-Link-libssp_nonshared.a-on-all-musl-targets.patch
9242b001e8c4a9ae5a7a726e43724c3e07813e3105fae50efab5f289f15fb6493cb6f15694f5a0289d5a7effc0704f2fe715af981c50d54a805393c0c72e2cf7  0008-test-failed-doctest-output-Fix-normalization.patch
45bcce759138df475f8fbeb8089420bd38a399f5a018ebdf6d5a79e7c714c9ec770c765c32dda0ab4e368c5dd226f474b6894d70b14e41bea57284eeeb1a2f58  0009-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
23f622841ba2030b02f52ed560c4be6800469e8021aa71842a702860c97b90edadf5de593ea4f6b9a158d7d0e4c83b52b419f770a8dac2b69633d643468c54d5  0010-test-use-extern-for-plugins-Don-t-assume-multilib.patch
73282af6c50155a29c2c98117a1ce6d28be42d04c25dc8ea2388de6923361fb7ae44d16bb81c57bb3d326d434f11ae6cdf72b8a0293421e8d0eaa931398e52d3  0011-Ignore-broken-and-non-applicable-tests.patch
a7f59baa8b067bcbb5cbffa5340a42e57ccf60488e8dc22b693401eb8798fcf1f6b8fa174b68f3697822a158536549ce9acbacd190f3f421caa1cbbd5e874bb0  0012-Link-stage-2-tools-dynamically-to-libstd.patch
475cf44411ec6d98443c41197fc9085a0a700910b9705e3c274c9d636f8c5a755a79bf435f2a2924c555122ecafaab0c30f058c13a17bbc8ac8ef9ba2f8342c2  0013-Move-debugger-scripts-to-usr-share-rust.patch
b594dfe12fef3b4da434cce7c163d752a6fbd9ade66b389a8620d09030208a71158159115eb001d05bde4d10b4ad4f27ff93613a71c0c4f597532331127c9592  0014-Add-foxkit-target-specs.patch
7fee0667793d5d5ee5cb600e24129c81de50511814e8f1a2f16bb47350087c5f42a01671415b10b01e414e9ff03008892a35c6cb616737175217bdccd1fa3f1e  0015-Use-OpenPOWER-ABI-on-BE-PowerPC-64-musl.patch
a2a05a64ca57dcde23c11ed8bd6ce530d80b99464c895c256b4e9cba02997121205a091de43f516563d5d618ea522d89639507ba94961807ada743a3099fe8d8  0040-rls-atomics.patch
493dc1abef477be1190c6cf4a49420a976d35ae6799853b77bdec399db94ccc20a90bf49bc69c2fd37bcabdefd7119553cb5ae77969a2242c969d6f62d7473d6  powerpc-atomics.patch"
