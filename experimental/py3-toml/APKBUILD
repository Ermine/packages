# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: 
pkgname=py3-toml
_pkgname=toml
_p="${_pkgname#?}"
_p="${_pkgname%"$_p"}"
pkgver=0.10.0
pkgrel=0
pkgdesc="Python Library for Tom's Obvious, Minimal Language"
url="https://pypi.python.org/pypi/toml"
arch="noarch"
# Certified net clean
license="MIT"
depends="python3"
makedepends=""
checkdepends="py3-pytest"
_testver="df90d31"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/$_p/$_pkgname/$_pkgname-$pkgver.tar.gz
	https://dev.sick.bike/dist/$pkgname-test-$_testver.tar.gz
	test.toml"
builddir="$srcdir/$_pkgname-$pkgver"

unpack() {
	default_unpack
	cd "$builddir"
	tar -zxf "$srcdir/$pkgname-test-$_testver.tar.gz"
	mv toml-test-* toml-test

	# This should be included in the next release
	cp "$srcdir/test.toml" "$builddir"
}

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$builddir" pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="26f26c38ce9cd48305218c2c34c5a5407b00aefc25a933f044bb7be22c23cfdfa3b8cf2da952d17760c4b9038df62e405fa039cc7b63dd3e94c9c312f04f9182  py3-toml-0.10.0.tar.gz
5c52dd75e75dd1f7ba787eaaadfebf28161aba056730b9b75e5e88745fa773195ae01c0b588d70b30f97b3c4e07e8108803ebba564f05f1f027315ff0f2ac401  py3-toml-test-df90d31.tar.gz
47510e59f9e0bdaaf4052fc5edb570a36b8b173ed7b8e5fac52804f6619f409e545ae3c91f838dbaeefe3c7cfd1a835b699164ac631feaa61de971d2381ac246  test.toml"
