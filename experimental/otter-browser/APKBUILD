# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=otter-browser
pkgver=1.0.03
pkgrel=0
pkgdesc="A free, open browser focused on user privacy"
url="https://otter-browser.org/"
arch="all"
options="!check"  # No test suite
license="GPL-3.0+"
depends=""
makedepends="cmake qt5-qtbase-dev qt5-qttools-dev openssl-dev gstreamer-dev
	qt5-qtmultimedia-dev qt5-qtdeclarative-dev qt5-qtsvg-dev hunspell-dev
	qt5-qtxmlpatterns-dev qt5-qtwebkit-dev"
langdir="/usr/share/otter-browser/locale"
subpackages="$pkgname-doc $pkgname-lang"
source="https://sourceforge.net/projects/$pkgname/files/$pkgname-$pkgver/$pkgname-$pkgver.tar.bz2"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-Bbuild
	make -C build
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="7d78b6927d9eb0503ca9a16cf5435505d941e615a9b77f2707b40f14a8a0c84250e88a33b2cc6ab3b1cce5b46930d23814135932ae38cbd0a8100452a5b2daf6  otter-browser-1.0.03.tar.bz2"
