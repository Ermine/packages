#!/usr/bin/perl

use strict;
use warnings;
use feature qw(:5.34);

open my $cargo, '<', $ARGV[0] or die "can't open $ARGV[0]: $!\n";
my $data = '';
# This block reads in the entire file.
{
	local $/;
	$data = <$cargo>;
}

my @lines = split /(\r?\n){2,}/, $data;
my @cargo;

# Parse the file; we just want the name and version.
foreach my $line (@lines) {
	$line =~ /^name = "(.*?)"$/m;
	my $name = $1;
	next unless defined($name);

	$line =~ /^version = "(.*?)"$/m;
	my $version = $1;
	next unless defined($version);

	next unless $line =~ /^source /m;
	$line =~ /^source = "(.*?)"$/m;
	my $source = $1;
	next unless defined($source);

	if ($source ne 'registry+https://github.com/rust-lang/crates.io-index') {
		warn "Warning: $name lacks a supported registry.\n";
	}
	push @cargo, "$name $version";
}

open my $file, '>', 'crates.txt' or die "can't open output file: $!\n";
# Print the data we pulled out above to a file.
foreach my $pkg (sort @cargo) {
	say $file $pkg;
}
