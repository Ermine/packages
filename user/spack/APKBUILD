# Contributor: Zach van Rijn <me@zv.io>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=spack
pkgver=0.23.0_p1
pkgrel=0
pkgdesc="A flexible package manager for supercomputers."
url="https://spack.io/"
arch="all"
license="Apache-2.0 OR MIT"
options="!dbg !strip !fhs"
makedepends="python3-dev"
depends="$makedepends
	gcc g++ gfortran
	bzip2 file git gnupg gzip libarchive-tools make patch unzip xz
	"
subpackages=""
# https://git.adelielinux.org/adelie/docs/-/wikis/Developer-Resources/Maintaining-Spack
source="https://git.adelielinux.org/adelie/$pkgname/-/archive/adelie-v$pkgver/$pkgname-adelie-v$pkgver.tar.bz2
	clingo-explicit-libatomic.patch
	openssl-explicit-linux-headers.patch

	default-no-buildcache.patch
	default-adelie-upstream-git.patch

	config.yaml
	"
install="$pkgname.post-install"
builddir="${srcdir}/${pkgname}-adelie-v${pkgver}"

prepare()
{
	default_prepare

	# disable binary bootstrap
	sed -i etc/spack/defaults/bootstrap.yaml -e '/github-actions/d';

	# delete windows files
	find bin \( -name '*.ps1' -o -name '*.py' -o -name '*.bat' \) -delete;
}

check() {
	##
	# On first run, Spack >=0.17.0 will bootstrap "clingo",
	# Spack's new "concretizer". The output from this process
	# is not kept, however it demonstrates Spack's functionality.
	#
	# The '--deprecated' flag is to prevent an interactive prompt
	# warning about "deprecated" packages and/or versions. Order
	# is important; it must come after the 'install' verb.
	#
	export SPACK_DISABLE_LOCAL_CONFIG=true
	export SPACK_USER_CONFIG_PATH=${builddir}/tmp
	export SPACK_USER_CACHE_PATH=${builddir}/tmp
	bin/spack --debug install --deprecated zlib

	rm -fr var/spack/cache
	rm -fr etc/spack/linux # caches compilers.yaml
}

package() {
	install -m755 -d "${pkgdir}"/opt/spack;
	for k in bin etc lib share var; do
		mv "${k}" "${pkgdir}"/opt/spack;
	done

	for k in LICENSE-APACHE LICENSE-MIT NOTICE; do
		install -Dm644 ${k} "${pkgdir}"/usr/share/licenses/${pkgname}/${k};
	done

	# lower precedence than /opt/spack/etc/spack/config.yaml
	install -D -m644 "${srcdir}"/config.yaml "$pkgdir"/etc/spack/config.yaml;
}

sha512sums="c40eb2ce32a7a432474c76ec76cda3a2830b7e9668d1dd76bf491d09d4bea6902e24f1fbff3c39ac67d2c0dd1f89560662a2828f9516ed99df8dc8bbd01afe6d  spack-adelie-v0.23.0_p1.tar.bz2
a84df22016109bb8d86b5215b698251a5b10873042361c8bab79e4235e00f381a2d66cbfd84ce6b7933125971f3248008124ef7f7ad85451bf72b815c5b56aa0  clingo-explicit-libatomic.patch
8ded405082eec192187fb72896c68200985ee1226b2ef7da19ab43f31138972d5b4000cf997cb6e22664a7c100b52b842df07c44e188f87d4250f698d1885ea6  openssl-explicit-linux-headers.patch
eec3e7dd818c39daea7b8d63b04a6c71d9eb3af32dd899cddc3642152252f4e2eebfd53c3c95365117f9a4826f6f29553f44373948e5c474388021302c8a5ce9  default-no-buildcache.patch
eb09ce82d2c5c63c2cc63b2f82db2888f15ce82100311614f5a73773be2f605337811c4580e0d4eb2f87b730e7ce4afa6dcdc3f3f52321f63a29a2ad533ea5d4  default-adelie-upstream-git.patch
864ae8e73cd80560ed9c4ce03bc6ae8f58b928da348a261617e42b6a680f8cf18f48b120401fe7b4a3f3af260675922044dbd9abbc79b1d2e90f821c00569d5e  config.yaml"
