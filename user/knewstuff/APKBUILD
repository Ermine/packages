# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=knewstuff
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework for discovering and downloading plugins, themes, and more"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires X11.
license="LGPL-2.0+"
depends="kirigami2"
depends_dev="qt5-qtbase-dev attica-dev kconfig-dev kservice-dev kxmlgui-dev
	openssl-dev"
docdepends="attica-doc kcoreaddons-doc kxmlgui-doc kservice-doc kconfig-doc
	kconfigwidgets-doc kcodecs-doc kwidgetsaddons-doc kauth-doc"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev karchive-dev kcompletion-dev kcoreaddons-dev kio-dev
	ki18n-dev kiconthemes-dev kirigami2-dev kitemviews-dev kpackage-dev
	ktextwidgets-dev kwidgetsaddons-dev syndication-dev $docdepends"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/knewstuff-$pkgver.tar.xz
	backport.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# kmoretoolstest requires X11.
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E 'kmoretoolstest'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="6269614a0df2616bd5bd1a8e9c3f8dc16920bbd4cf8fd18a5ccc1e818c292d90533de3dc77ce8fbbb183388ad6fed72a30b78177eddd87624499b3c75d55a226  knewstuff-5.94.0.tar.xz
5c73d39a78658899f4e7b2526608498ff63bf4e6f9b730b0f20f3205017e77fe4a9a96e3ab9e42f0789234a3e0658a89f646bc040488465f07689bc9629105fc  backport.patch"
