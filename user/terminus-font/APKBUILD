# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=terminus-font
pkgver=4.49.1
_pkgver=${pkgver%.*}
pkgrel=0
pkgdesc="Monospace bitmap font (for X11 and console)"
url="http://terminus-font.sourceforge.net/"  # No HTTPS
arch="noarch"
options="!check"  # Fonts have no test suite
license="OFL-1.1"
depends=""
makedepends="bdftopcf python3"
source="https://downloads.sourceforge.net/project/$pkgname/$pkgname-$_pkgver/$pkgname-$pkgver.tar.gz
	75-yes-terminus.conf"

build() {
	./configure \
		--prefix=/usr \
		--psfdir=/usr/share/consolefonts \
		--x11dir=/usr/share/fonts/X11/misc
	make
}

package() {
	make DESTDIR="$pkgdir" install

	# Use custom fontconfig since the shipped one doesn't provide
	# a correct alias
	install -Dm644 "$srcdir"/75-yes-terminus.conf \
		"$pkgdir/etc/fonts/conf.avail/75-yes-terminus.conf"
	install -d "$pkgdir/etc/fonts/conf.d"
	cd "$pkgdir/etc/fonts/conf.d"
	ln -s /etc/fonts/conf.avail/75-yes-terminus.conf .
}

sha512sums="2e3a99960c459a453b5879bebbebeb653f0a22060177bf75c67bbeb7d32cfe1acce9e74a88fdc42e299b6dc402e0708552487abd26e80e985c7d6059b0d445a8  terminus-font-4.49.1.tar.gz
ac7bdf367bbff1e5735b5bd65fc1bc06b55bcb72aaafeb08c7d8e969aabad863474b1104faa42ead7d838433c80cfc50f05578dad03c8081cd82cfda0af86a83  75-yes-terminus.conf"
