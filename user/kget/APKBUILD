# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kget
pkgver=22.04.2
pkgrel=0
pkgdesc="Versatile download manager"
url="https://www.kde.org/applications/internet/kget/"
arch="all"
options="!check"  # Tests don't work without KIO/D-Bus.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kcompletion-dev ki18n-dev
	kconfig-dev kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev kio-dev
	kdoctools-dev kiconthemes-dev kitemviews-dev kcmutils-dev kparts-dev
	kdelibs4support-dev knotifications-dev knotifyconfig-dev kservice-dev
	solid-dev ktextwidgets-dev kwallet-dev kwidgetsaddons-dev kxmlgui-dev
	kwindowsystem-dev gpgme-dev qca-dev boost-dev libktorrent-dev
	libkworkspace-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kget-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="c0fdc051f1882b4a775e1a6b6131351fa7763cc0183bd40dbbde9def39503b1b09da4d174cd5a7c6ac3a6045f3a8ceb26e289fadf374245ef63fd19385743a21  kget-22.04.2.tar.xz"
