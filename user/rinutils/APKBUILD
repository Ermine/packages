# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=rinutils
pkgver=0.8.0
pkgrel=0
pkgdesc="C11 utilities library"
url="https://github.com/shlomif/rinutils"
arch="noarch"
license="Expat"
depends=""
checkdepends="cmocka-dev perl perl-dev perl-env-path perl-inline perl-inline-c
	perl-path-tiny perl-string-shellquote perl-test-differences
	perl-test-trailingspace"
makedepends="cmake"
# Headers-only library: no -dev split.
subpackages=""
source="https://github.com/shlomif/rinutils/releases/download/$pkgver/rinutils-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="06bbcdb5092762bf2ed92d4082eca17e8dc0e822ce8634f8ec8782fe7eccb4b954c081dbba3f1e2f72e0285607a813f020d4277af77accc43135bde50153359d  rinutils-0.8.0.tar.xz"
