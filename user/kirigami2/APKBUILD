# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kirigami2
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework for rapidly designing usable interfaces"
url="https://www.kde.org/"
arch="all"
options="!check"  # Tests require accelerated X11 desktop.
license="LGPL-2.1+"
depends="qt5-qtgraphicaleffects"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtsvg-dev
	qt5-qtquickcontrols2-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen
	graphviz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kirigami2-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="e836f45729f54a5c83c7dd5bb5def3e36f73cd6367456953bab8e3bdd87c6bf9845ff3f4f0458f289c0450a4ac02a8fe1984458a591172e9e87b782ac59f9a6f  kirigami2-5.94.0.tar.xz"
