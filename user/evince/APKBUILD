# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=evince
pkgver=3.36.7
pkgrel=0
pkgdesc="GNOME document viewer"
url="https://wiki.gnome.org/Apps/Evince"
arch="all"
license="GPL-2.0+ AND MIT AND BSD-3-Clause AND LGPL-2.0+ AND X11 AND LGPL-3.0-only AND Public-Domain AND LGPL-2.1+"
depends="gsettings-desktop-schemas gst-plugins-base gst-plugins-good"
makedepends="djvulibre-dev glib-dev gobject-introspection-dev
	gsettings-desktop-schemas-dev gstreamer-dev gst-plugins-base-dev
	gtk+3.0-dev itstool libarchive-dev libexecinfo-dev libgxps-dev
	libsecret-dev libspectre-dev libxml2-dev libxml2-utils poppler-dev
	tiff-dev zlib-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://ftp.gnome.org/pub/gnome/sources/evince/${pkgver%.*}/evince-$pkgver.tar.xz"

# secfixes:
#   3.32.0-r2:
#     - CVE-2019-11459

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--without-gspell \
		--disable-nautilus \
		--enable-introspection \
		--enable-ps
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="dacc26d2d75c3dcadceb0a35c995ae3501bd91e84632cdf87decd359350e74032444707ea152e24c01fb57fe0ef2a426813a33919833cefdeb8699f8c642d910  evince-3.36.7.tar.xz"
