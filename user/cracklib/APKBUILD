# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=cracklib
pkgver=2.9.8
pkgrel=0
pkgdesc="Library for checking passwords against dictionary words"
url=" "
arch="all"
license="LGPL-2.0+"
depends=""
makedepends="autoconf automake libtool"
subpackages="$pkgname-dev $pkgname-lang"
triggers="$pkgname.trigger=/usr/share/dict"
source="https://github.com/cracklib/cracklib/releases/download/v$pkgver/cracklib-$pkgver.tar.bz2
	$pkgname.trigger
	nls.patch
	"

# secfixes:
#   2.9.7-r0:
#   - CVE-2016-6318

prepare() {
	default_prepare
	# autogen.sh is not included in the dist tarball, but only calls:
	autoreconf -f -i
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--without-python
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
	install -D -m644 dicts/cracklib-small "$pkgdir"/usr/share/dict/cracklib-small
}

sha512sums="bba1b82067156f44095b282c70c06a05e58572cde7ad2430dd24c4b42ae98be86708ea86ba8b7104aa5887e16ac90d7cf3ae613b84ab9c0f7602307d78b75371  cracklib-2.9.8.tar.bz2
deef4710a3bf78348adfe699ff995acc21a7671ab03c4dd28da7f38f4a83008af4c97c9c1d4e1e98a47c0148c84146b36477f41f98fb0ee028cc0fadebb85ab9  cracklib.trigger
100edacf91a4c251d4b05c0f550164fefc3fdaae4b715b24b9c42ec8cc17916b0929f96881982441dddf0fc7b8b91c9423bf0518cb571864f302d054c24a0770  nls.patch"
