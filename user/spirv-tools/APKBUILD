# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=spirv-tools
pkgver=2024.3
pkgrel=0
pkgdesc="Utilities for working with SPIR-V modules "
url="https://github.com/KhronosGroup/SPIRV-Tools"
arch="all"
license="Apache-2.0"
depends=""
depends_dev="spirv-headers"
makedepends="$depends_dev cmake python3"
subpackages="$pkgname-dev"
source="spirv-tools-$pkgver.tar.gz::https://github.com/KhronosGroup/SPIRV-Tools/archive/refs/tags/v$pkgver.tar.gz
	endian.patch
	remove-copyright-check-test.patch
	"
builddir="$srcdir/SPIRV-Tools-$pkgver"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DSPIRV-Headers_SOURCE_DIR="/usr" \
		-DSPIRV_TOOLS_BUILD_STATIC=off \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="804e2a076025a9afde2d7b0b6950fa7b7289b9aa911348b10aad45cc3515116b7484e886d49b524315f75e8d1a2009c2f26310352c154d1b23a58864e8535324  spirv-tools-2024.3.tar.gz
d31f1650a9f1d35b4ee95eddd6bd6aceb3490591e269f283b71589cfd3ca69b77467cabf659fdc42ee96433092434137a3be55c69b60b7edbcc40d5ed76357b1  endian.patch
a04f1a4d60d6206cbbd5e97dda58ca05689835520617dfe7bdfe29d66ab987f68080fbee85c039aecab554e01d94b16a99acfc2ffd29d9ef5861d3dd2e2dccb1  remove-copyright-check-test.patch"
