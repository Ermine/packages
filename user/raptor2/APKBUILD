# Contributor: Ariadne Conill <ariadne@dereferenced.org>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=raptor2
pkgver=2.0.15
pkgrel=3
pkgdesc="RDF parser/serializer toolkit for Redland"
url="https://www.librdf.org/raptor"
arch="all"
license="Public-Domain AND (LGPL-2.1+ OR GPL-2.0+ OR Apache-2.0+)"
depends=""
depends_dev="curl-dev libxml2-dev libxslt-dev yajl-dev"
makedepends="$depends_dev autoconf automake libtool gtk-doc"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.librdf.org/source/$pkgname-$pkgver.tar.gz
	CVE-2017-18926.patch
	CVE-2020-25713.patch
	fix-checked-field-issue.patch
	"

prepare() {
	default_prepare
	update_config_sub
	autoreconf -vif
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--target=$CTARGET \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var
	make
}

check() {
	# Note: some tests are shown as FAIL but they are actually XFAIL :)
	# As long as the return status is zero we are fine
	make check -j1
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="563dd01869eb4df8524ec12e2c0a541653874dcd834bd1eb265bc2943bb616968f624121d4688579cdce11b4f00a8ab53b7099f1a0850e256bb0a2c16ba048ee  raptor2-2.0.15.tar.gz
82f2f7ea4b72aa2bf444013a81db3cb17fcce2ae650bdb22eaab00d6d5cf7f950f7a550ffff49348db878f90f2753b407e6026d08d543cd0757c1687c6dad159  CVE-2017-18926.patch
6e7297bb786dd490202f9790e148b298380c1e47faa4970cb7efcd85fcea99eaa5f53e1c66627be9f7f42f6f27a3b1df94357c9db51abf8ed14727343d584f08  CVE-2020-25713.patch
97ab7b8be0471d2b123603a2cf60a07508dcdacc0a14965dcf4dfe42a1b57e8baecd3488471aac7cd39d203cefbbc966c8ec492cfaafda62ebf2c80bf6c85f34  fix-checked-field-issue.patch"
