# Contributor: Peter Bui <pnutzh4x0r@gmail.com>
# Contributor: Matt Smith <mcs@darkregion.net>
# Contributor: zlg <zlg+adelie@zlg.space>
# Maintainer:
pkgname=py3-pygments
_pkgname=Pygments
_p="${_pkgname#?}"
_p="${_pkgname%"$_p"}"
pkgver=2.6.1
pkgrel=0
pkgdesc="A syntax highlighting package written in Python"
url="https://pygments.org/"
arch="noarch"
options="!check" # needs nose, flake8
license="BSD-2-Clause"
depends="python3"
makedepends="python3-dev"
subpackages=""
source="https://files.pythonhosted.org/packages/source/$_p/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	python3 setup.py build
}

# Reactivate once nose and flake8 are packaged
check() {
	sed -i -e 's:python:python3:' Makefile
	make test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

doc() {
	install -m 644 -D doc/pygmentize.1 "$pkgdir"/usr/share/man/man1/pygmentize.1
}

sha512sums="0d793bb6eabe43645541347e44932ee50ccd4309f40e7a327422bcbe57d40a7cb8bbf2fb0911744ed6f316c6c08f4216462be88d79192a2ea004d6da51329932  Pygments-2.6.1.tar.gz"
