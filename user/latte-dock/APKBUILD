# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=latte-dock
pkgver=0.10.8
pkgrel=0
pkgdesc="KDE dock"
url="https://kde.org/applications/utilities/org.kde.latte-dock"
arch="all"
license="GPL-2.0+"
depends="kirigami2 plasma-workspace qqc2-desktop-style qt5-qtgraphicaleffects
	qt5-qtquickcontrols"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	kactivities-dev karchive-dev kcoreaddons-dev kcrash-dev kdbusaddons-dev
	kdeclarative-dev kglobalaccel-dev kguiaddons-dev ki18n-dev
	kiconthemes-dev knewstuff-dev knotifications-dev kwayland-dev
	kwindowsystem-dev kxmlgui-dev plasma-framework-dev kirigami2-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/latte-dock/latte-dock-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="a1148401477d7c2a5123694820443313cd13196a8a2f86815aa89900ac5fb1f8196fc3b99b861ee2c30211071f474965093c0dae42f4b02bb4071c71e8d7bed5  latte-dock-0.10.8.tar.xz"
