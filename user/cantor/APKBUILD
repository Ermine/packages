# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=cantor
pkgver=22.04.2
pkgrel=0
pkgdesc="KDE worksheet interface for popular mathematical applications"
url="https://edu.kde.org/cantor/"
arch="all"
options="!check"  # Tests require X11.
license="GPL-2.0-only"
depends="qt5-qttools r shared-mime-info"
depends_dev="libspectre-dev"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev
	qt5-qtxmlpatterns-dev karchive-dev kcompletion-dev kconfig-dev
	kcoreaddons-dev kcrash-dev kdoctools-dev ki18n-dev kiconthemes-dev
	kio-dev knewstuff-dev kparts-dev kpty-dev ktexteditor-dev
	ktextwidgets-dev kxmlgui-dev $depends_dev poppler-dev poppler-qt5-dev
	qt5-qttools-dev

	analitza-dev gfortran libqalculate-dev python3-dev r r-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/cantor-$pkgver.tar.xz
	rip-out-webengine.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="a452cb1e91c5b75587d520c139109a81c6b78bd17060f7c97ba1ae4fe444c31f3dae34a8a31597345c1b275b508cac349ef6af4b2bdd09cca37da9d8c7400ec5  cantor-22.04.2.tar.xz
d29103817ef512f34e84c6dda26a1e675a1e57d9b9392f309f583e41384569f041a662af207cf7bfde428a379db189c05c0b1f6321e8fa7804bfad3c2ecd4189  rip-out-webengine.patch"
