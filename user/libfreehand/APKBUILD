# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=libfreehand
pkgver=0.1.2
pkgrel=4
pkgdesc="Import filter and tools for Adobe FreeHand documents"
url="https://wiki.documentfoundation.org/DLP/Libraries/libfreehand"
arch="all"
license="MPL-2.0 AND Public-Domain"
depends=""
depends_dev="libxml2-dev lcms2-dev icu-dev"
checkdepends="cppunit-dev"
makedepends="$depends_dev gperf perl doxygen librevenge-dev zlib-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools"
source="http://dev-www.libreoffice.org/src/$pkgname/$pkgname-$pkgver.tar.xz
	icu.patch
	"

prepare() {
	default_prepare
	update_config_sub
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-werror
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

tools() {
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr
}

sha512sums="4112a76ac99999801d97d1b282596d631d8496a5bf65778ab26aa06da86637b1e2b630648a67ea01bf3316ecec9f2715546baff27af090b900267c87a011b963  libfreehand-0.1.2.tar.xz
5c72ee92e1b617dd0ece1a17e84c4b063454811aae484a08d2e15a29c6561068d0127222656647bd0ac05afde8c5c2154d382b4289b86e1956bf173ed4694a76  icu.patch"
