# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kiconthemes
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework for icon theming"
url="https://www.kde.org/"
arch="all"
options="!check"  # requires X11 running
license="LGPL-2.1-only"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtsvg-dev karchive-dev ki18n-dev
	kcoreaddons-dev kconfigwidgets-dev kitemviews-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kiconthemes-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="3cd2c0302a94656322dff1e79083ee4f8fbf29d2acfbfaa4a22cdaec9a7df6fc997c8c00ee98dee2fac1a499795bdb0a4b178b8b9b97b5fe40509661d6e5acaf  kiconthemes-5.94.0.tar.xz"
