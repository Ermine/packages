# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kconfig
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework for managing software configuration"
url="https://www.kde.org/"
arch="all"
options="!check"  # Tests require running D-Bus.
license="LGPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen
	qt5-qtdeclarative-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kconfig-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -R kconfigcore
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="59d7d957313e421a332fb0a83eef6be68455c3de50cee0cd0d1b6b22f2b97ad9fdfddac282f03c4cedc5f364d4b3321c3f3fdadd7824e0523c5260446dc31ac9  kconfig-5.94.0.tar.xz"
