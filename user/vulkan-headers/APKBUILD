# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=vulkan-headers
pkgver=1.3.294
pkgrel=0
pkgdesc="Development files for Vulkan"
url="https://www.vulkan.org/"
arch="noarch"
options="!check !dev"  # no testsuite, headers-only project
license="Apache-2.0 AND MIT"
depends=""
makedepends="cmake"
subpackages=""
source="vulkan-headers-$pkgver.tar.gz::https://github.com/KhronosGroup/Vulkan-Headers/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/Vulkan-Headers-$pkgver"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="a2a2ccd2fa64acebb2ed3d54b9954d3a465b30542b72cff71d321ba49c75f1396519b8510159140d48e4ebae94169a6dccf21a85586079a10b22c4affd0944b4  vulkan-headers-1.3.294.tar.gz"
