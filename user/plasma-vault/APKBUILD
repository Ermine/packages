# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=plasma-vault
pkgver=5.24.5
pkgrel=0
pkgdesc="Secure storage plugin for KDE Plasma desktop"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0-only AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="encfs"
makedepends="cmake extra-cmake-modules kactivities-dev kconfig-dev
	kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev ki18n-dev kio-dev
	kwidgetsaddons-dev libksysguard-dev networkmanager-qt-dev
	plasma-framework-dev qt5-qtbase-dev qt5-qtdeclarative-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-vault-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="6495f94f060f139e91a25cbecf432d5bf9ee5ae949438278966c11cd7e725feb63aee6955dce5b46b816b0c57f5253b2cf215ad70efd592bf6e579ad6acb4cd2  plasma-vault-5.24.5.tar.xz"
