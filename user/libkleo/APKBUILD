# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libkleo
pkgver=22.04.2
pkgrel=0
pkgdesc="KDE encryption library"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0+ AND GPL-2.0-only AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev boost-dev gpgme-dev libgpg-error-dev ki18n-dev kwidgetsaddons-dev
	kitemmodels-dev"
makedepends="$depends_dev cmake extra-cmake-modules kcompletion-dev kconfig-dev
	kcodecs-dev kcoreaddons-dev kpimtextedit-dev kwindowsystem-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkleo-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# These excluded tests require a running X11 session.
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(editdirectoryservicedialog|keyselectioncombo|keyserverconfig|newkeyapprovaldialog)'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="775307703ff913d8dcb2ee34c4b3526c60e85320c4f377e968562d6dc3e2dd3110b5c9477b7a186837abb024db5c488ee42ec86b7e7347d5a2ba5dd031f34968  libkleo-22.04.2.tar.xz"
