# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdelibs4support
pkgver=5.94.0
pkgrel=0
pkgdesc="Legacy support for KDE 4 software"
url="https://www.kde.org/"
arch="all"
options="!check suid"  # Test requires running X11 session.
license="LGPL-2.1+ AND MIT AND LGPL-2.1-only AND LGPL-2.0-only AND (LGPL-2.1-only OR LGPL-3.0-only) AND (LGPL-2.0-only OR LGPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtsvg-dev qt5-qttools-dev kcompletion-dev
	kconfigwidgets-dev kcrash-dev kdesignerplugin kdesignerplugin-dev
	kemoticons-dev kparts-dev kunitconversion-dev kinit kded kded-dev
	kitemmodels-dev knotifications-dev sonnet-dev threadweaver-dev
	kinit-dev"
makedepends="$depends_dev cmake extra-cmake-modules networkmanager-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kdelibs4support-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="8b48b7d67201bc6e0fb67fa00b75dbaf8e35e21f7527b4bc79939c2b689c027bfa75eb60b13686bd8bce710a61682a27cac85481edd6e321063aefb186cdd2f2  kdelibs4support-5.94.0.tar.xz"
