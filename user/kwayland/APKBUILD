# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kwayland
pkgver=5.94.0
pkgrel=0
pkgdesc="Qt wrapper libraries for Wayland"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires running Wayland compositor
license="LGPL-2.1-only OR LGPL-3.0-only"
depends=""
depends_dev="wayland-dev"
makedepends="$depends_dev cmake extra-cmake-modules doxygen graphviz
	qt5-qttools-dev qt5-qtwayland qt5-qtwayland-dev qt5-qtwayland-tools
	wayland-protocols plasma-wayland-protocols"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwayland-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="56d10d1048967d0c54ba88339b31ddef4b5cf3a3aa743f8fac56f5c97292112d7fa998107a854e999a54ac4ab4659e7dbf6d6c496856de4e91c45df9fafdf399  kwayland-5.94.0.tar.xz"
