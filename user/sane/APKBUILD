# Contributor: Fabio Riga <rifabio@dpersonam.me>
# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sane
_pkgname=sane-backends
pkgver=1.3.1
_hash=83bdbb6c9a115184c2d48f1fdc6847db
pkgrel=0
pkgdesc="Scanner access library"
url="http://www.sane-project.org/"
arch="all"
license="GPL-2.0+ AND Public-Domain"
depends=""
makedepends="diffutils file libtool libusb-dev v4l-utils-dev net-snmp-dev
	libpng-dev libjpeg-turbo-dev tiff-dev libgphoto2-dev libieee1284-dev
	libxml2-dev linux-headers ncurses-dev python3-dev"
install="saned.pre-install $pkgname.pre-install"
pkgusers="saned"
pkggroups="scanner"
_backends="abaton agfafocus apple artec artec_eplus48u as6e avision bh canon
	canon630u canon_dr canon_pp cardscan coolscan coolscan2 coolscan3 dc25
	dc210 dc240 dell1600n_net dmc epjitsu epson epson2 epsonds fujitsu
	genesys gphoto2 gt68xx hp hp3500 hp3900 hp4200 hp5400 hp5590 hpsj5s
	hpljm1005 hs2p ibm kodak kodakaio kvs1025 kvs20xx leo lexmark ma1509
	magicolor matsushita microtek microtek2 mustek mustek_pp mustek_usb
	nec net niash p5 pie pieusb pixma plustek plustek_pp ricoh ricoh2
	rts8891 s9036 sceptre sharp sm3600 sm3840 snapscan sp15c st400 stv680
	tamarack teco1 teco2 teco3 test u12 umax umax_pp umax1220u v4l
	xerox_mfp"
case "$CARCH" in
x86*) _backends="$_backends qcam";;
esac

_pkgdesc_dell1600n_net="SANE backend for Dell 1600n (Ethernet only; USB not supported)"
for _backend in $_backends; do
	subpackages="$subpackages $pkgname-backend-$_backend:_backend"
done
subpackages="$pkgname-doc $pkgname-dev $subpackages $pkgname-utils saned
	saned-openrc:openrc:noarch $pkgname-udev::noarch $_pkgname::noarch
	$pkgname-lang"
source="https://gitlab.com/sane-project/backends/uploads/$_hash/$_pkgname-$pkgver.tar.gz
	saned.initd
	include.patch
	network.patch
	pidfile.patch
	check.patch
	BTS-304.patch
	"
builddir="$srcdir"/$_pkgname-$pkgver

build() {
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--with-usb \
		--disable-rpath \
		--disable-locking
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	printf "" > "$pkgdir"/etc/sane.d/dll.conf
	install -Dm644 backend/dll.aliases "$pkgdir"/etc/sane.d/dll.aliases
}

doc() {
	default_doc
	mkdir -p "$subpkgdir"/usr/share/licenses/$_pkgname
	mv "$subpkgdir"/usr/share/doc/$_pkgname/LICENSE \
		"$subpkgdir"/usr/share/licenses/$_pkgname
}

saned() {
	pkgdesc="Network scanner server"
	mkdir -p "$subpkgdir"/etc/sane.d "$subpkgdir"/usr
	mv "$pkgdir"/etc/sane.d/saned.conf "$subpkgdir"/etc/sane.d
	mv "$pkgdir"/usr/sbin "$subpkgdir"/usr/
}

openrc() {
	pkgdesc="Network scanner server (OpenRC runscripts)"
	depends="saned"
	install_if="saned=$pkgver-r$pkgrel openrc"
	install -Dm755 "$srcdir"/saned.initd "$subpkgdir"/etc/init.d/saned
}

utils() {
	pkgdesc="$pkgdesc (utilities)"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr
}

udev() {
	pkgdesc="$pkgdesc (udev rules)"
	install_if="$pkgname=$pkgver-r$pkgrel udev"
	install -Dm644 "$builddir"/tools/udev/libsane.rules \
		"$subpkgdir"/usr/lib/udev/rules.d/49-sane.rules
	sed -i 's|NAME="%k", ||g' "$subpkgdir"/usr/lib/udev/rules.d/49-sane.rules
}

backends() {
	local _backend;
	pkgdesc="$pkgdesc (metapackage)"
	depends="$pkgname-utils saned"
	for _backend in $_backends; do
		[ "$_backend" = "test" ] && continue
		depends="$depends $pkgname-backend-$_backend"
	done
	mkdir -p "$subpkgdir"
}

_backend() {
	local name="${subpkgname#$pkgname-backend-}"
	depends="$pkgname"
	pkgdesc=$(eval echo \$_pkgdesc_$name)
	if [ ! "$pkgdesc" ]; then
		# cut description from man-page
		pkgdesc=$(tr '\n' ' ' < "$builddir"/doc/$pkgname-$name.man)
		pkgdesc=${pkgdesc#*\- }
		pkgdesc=${pkgdesc%% .SH *};
	fi
	mkdir -p "$subpkgdir"/usr/lib/$pkgname \
		"$subpkgdir"/etc/$pkgname.d/dll.d
	mv "$pkgdir"/usr/lib/sane/lib$pkgname-$name.* \
		"$subpkgdir"/usr/lib/sane
	echo "$name" > "$subpkgdir"/etc/sane.d/dll.d/$name
	if [ -f "$pkgdir"/etc/sane.d/$name.conf ]; then
		mv "$pkgdir"/etc/sane.d/$name.conf \
			"$subpkgdir"/etc/sane.d
	fi
	if [ -f "$pkgdir"/usr/bin/$name ]; then
		mkdir -p "$subpkgdir"/usr/bin
		mv "$pkgdir"/usr/bin/$name "$subpkgdir"/usr/bin
	fi
}

sha512sums="c6c12bce5242fcdf208f034cc544605cad36fad60316cb51f0e1f6fe23d1566823778c7af4b0fc94ca4154e2cd3e38a9202073e4a4af05f641c3da081722a535  sane-backends-1.3.1.tar.gz
0a06eaa28b345202f2bdf8361e06f843bb7a010b7d8f80132f742672c94249c43f64031cefa161e415e2e2ab3a53b23070fb63854283f9e040f5ff79394ac7d1  saned.initd
1779ff8beb1ba5f9238c25d819a7f0045f7e257c19b511315feb85650e445ca86450a9e1d7ff8650499d3dae808589a6c2e358d5f3f39a3f40ce4999179b86d6  include.patch
ef5d572bd29463e0690f6b45ddc609045ba6e40ab7c6bdab065b9480eef38884604650fd390d2a3c46b1f13b228e700392a6f635b4d7372130de6b2106208405  network.patch
30ef4a151e896ad415bab5e6a2963611321ff39d4b91d591d2678a5f73b1cd809f90855f970edec3d944aa0f48fb2902c20184794f941312b849e7ab44967030  pidfile.patch
4de6f60452c0451769f5ce41e41ca4c2867a723e0d2bf22796dc8a266359bdc8a9e9542f4ba2dc42b15bd25b1c83d2c339177796043fdbcbc9d73ad4957f723c  check.patch
9c4877335531415df4aa37d797927765076f2e98d7301e057b24d5e45a696b75e86140eec403a599508c270ff63faf29b07ced6591a647ff48d280edcab70208  BTS-304.patch"
