# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=plasma-workspace
pkgver=5.24.5
pkgrel=0
pkgdesc="KDE Plasma 5 workspace"
url="https://www.kde.org/plasma-desktop"
arch="all"
options="!check"  # Test requires X11 accelration.
license="(GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1+ AND GPL-2.0+ AND MIT AND LGPL-2.1-only AND LGPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.0-only"
# startkde shell script calls
depends="kinit qdbus qtpaths xmessage xprop xset xsetroot"
# font installation stuff
depends="$depends mkfontdir"
# QML deps
depends="$depends kquickcharts qt5-qtgraphicaleffects qt5-qtquickcontrols solid"
# other runtime deps / plugins
depends="$depends libdbusmenu-qt kcmutils kde-cli-tools kded kdesu kio-extras
	ksystemstats kwin milou plasma-integration pulseaudio-utils iso-codes
	breeze plasma-framework prison xrdb"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev kdelibs4support-dev
	kitemmodels-dev kservice-dev kwindowsystem-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtscript-dev
	iso-codes-dev libdbusmenu-qt-dev libqalculate-dev libxtst-dev
	xcb-util-image-dev libkworkspace-dev networkmanager-qt-dev

	baloo-dev kactivities-dev kcmutils-dev kcrash-dev kdbusaddons-dev
	kdeclarative-dev kdesu-dev kdoctools-dev kglobalaccel-dev kholidays-dev
	kidletime-dev kjsembed-dev knewstuff-dev knotifyconfig-dev kpackage-dev
	krunner-dev kscreenlocker-dev ktexteditor-dev ktextwidgets-dev
	kwallet-dev kwayland-dev kwin-dev kxmlrpcclient-dev libksysguard-dev
	plasma-framework-dev prison-dev kactivities-stats-dev kpeople-dev
	kirigami2-dev kquickcharts-dev kuserfeedback-dev libkscreen-dev
	breeze-dev fontconfig-dev layer-shell-qt-dev qt5-qtwayland-tools
	libxkbcommon-dev libxft-dev qt5-qtwayland-dev wayland-dev"
provides="user-manager=$pkgver-r$pkgrel"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-workspace-$pkgver.tar.xz
	libkworkspace.patch
	backport1.patch
	backport2.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTING=OFF \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="ae40fe545b03e42e9f8fe2e8885d3853f4b52d4fd2ed3ecf7d24fa59b4924b8f8b389e8713ec6a3a875bc66df3952de9157d4a5631b283105a9329a1a1825996  plasma-workspace-5.24.5.tar.xz
f58b88928fd68518bc0524db35388cb0f0dbc2a55d85fc47e92ce7fcbaf9b155482736e282bd84104ceecc625406845840128c8d0fcd2a4d5a854673964cd94f  libkworkspace.patch
022f1407c538f396cdd1fd9c228a56450ed868e0f16e42333cd10fca238fb5e12b38596f65b06159b573628c290f86d8af8dca136b1acafe4042d4c0d91e8a63  backport1.patch
3cc5228b5f2fdcb21a2e877fccde1e5316a9e2efae1523c68dff4a6a2efa5cc42782340b7ae491a5ea77ef642aa4b7ce35e58068d9e124b286c431105bad8821  backport2.patch"
