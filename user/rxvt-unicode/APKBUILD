# Contributor: Moritz Wilhelmy <mw@barfooze.de>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Jakub Skrzypnik <j.skrzypnik@openmailbox.org>
# Contributor: Ariadne Conill <ariadne@dereferenced.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=rxvt-unicode
pkgver=9.31
pkgrel=1
pkgdesc="Fork of the rxvt terminal emulator with improved unicode support"
url="http://software.schmorp.de/pkg/rxvt-unicode.html"
arch="all"
options="!check"  # No test suite.
license="(GPL-2.0+ OR BSD-2-Clause) AND GPL-2.0+ AND GPL-3.0+"
depends="$pkgname-terminfo"
makedepends="libx11-dev libxft-dev ncurses fontconfig-dev libptytty-dev
	gdk-pixbuf-dev libxrender-dev perl-dev startup-notification-dev
	libxext-dev"
subpackages="$pkgname-doc $pkgname-terminfo::noarch"
source="http://dist.schmorp.de/$pkgname/Attic/$pkgname-$pkgver.tar.bz2
	gentables.patch
	kerning.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--with-terminfo=/usr/share/terminfo \
		--enable-256-color \
		--enable-font-styles \
		--enable-xim \
		--enable-keepscrolling \
		--enable-selectionscrolling \
		--enable-smart-resize \
		--enable-pixbuf \
		--enable-transparency \
		--enable-frills \
		--enable-perl \
		--enable-mousewheel \
		--enable-text-blink \
		--enable-fading \
		--enable-startup-notification \
		--enable-unicode3
	make
}

package() {
	# despite having a separate terminfo subpackage
	# TERMINFO env var is used by rxvt-unicode makefile
	# leaving it as is ~skrzyp
	export TERMINFO="$pkgdir/usr/share/terminfo"
	mkdir -p "$TERMINFO"

	make DESTDIR="$pkgdir" install
}

terminfo() {
	pkgdesc="$pkgdesc (terminfo data)"
	depends=""

	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/terminfo "$subpkgdir"/usr/share/terminfo
}

sha512sums="4d14ecbbb62de1b1c717277f5aae5cfb536e11392f2d4b82c884c1713f437fce8e9dd69a328fa353a55d068d8ee4121a31900f45191acec172d5dc76652b6255  rxvt-unicode-9.31.tar.bz2
a45074b8fe39ffb712bd53b03521a8611fe5887a97ea09c1e85a7086de1042dd0360269803ffe5fcc56425af3c0cc3a55c214b2ef0fcfa2c3a298b4b37d261cb  gentables.patch
e4e9a05e006a555a8ee6df66dd8d4e93beb9f4e07fd4a889f53dc7ca8cbb49f3c8be140b51bcb26de62e505f3852877aff25f03c6872752133255bbeda291fb8  kerning.patch"
