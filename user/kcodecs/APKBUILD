# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcodecs
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework for manipulating strings in differing encodings"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1-only AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules gperf qt5-qttools-dev
	doxygen"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcodecs-$pkgver.tar.xz"

# secfixes:
#   5.74.0-r0:
#     - CVE-2013-0779

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="553d093d3b030552101c212d90fdd1ffdd04feb59d96e9e859ad64b6abbda8d43690c5cfbbf3383cdaa25f9bc7620a6105c839a8446f109386e08cd74504b0c9  kcodecs-5.94.0.tar.xz"
