# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=encfs
pkgver=1.9.5
pkgrel=0
pkgdesc="Encrypted filesystem for FUSE"
url="https://vgough.github.io/encfs/"
arch="all"
license="LGPL-3.0+ AND GPL-3.0+ AND MIT AND Zlib"
depends=""
makedepends="cmake fuse-dev openssl-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://github.com/vgough/encfs/releases/download/v$pkgver/encfs-$pkgver.tar.gz
	cmake.patch
	length.patch
	shell.patch
	typos.patch
	utimensat.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-Bbuild .
	make -C build
}

check() {
	make -C build unittests
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="036e08ca9bc13b44742aebdee49bf7029d0c6b7e59cd6dedc9a09da2af99482859f6a79eddf07e3db296edaf45aafc48fe08488840e765682e9b192dd6ae4c46  encfs-1.9.5.tar.gz
4810156ba2d61aef2f5c0c4cf8528596752309aacba5930aa6becfc6160b3c897df7570408b6431d5f09d6f85cd0fc41bd9127d47bdc9da6eb2e6321a9bda2f6  cmake.patch
fa94088ad23ca9bdc2127f3025dc27fbf148ce94aad0378601cf0e0831039f654d74f2b3259d18ab448df3e3e63e96c59c73275abe0463504f91345d495f4375  length.patch
c25a8c0a65c163ce87c3275261f0dae01df8d382a31a97efdc074a746cc8e8e2564e19568ca84e49c8c0c209406257dcd161784f47ec9fc78251e2c3175a3a82  shell.patch
432199fddc361a8ace6c09a883261d07dc3aead6450584c52e98ddfaf310e5621fc9f3c3da48f012104690cc036a251919100c5d0ad17143e68ba07e826dafd9  typos.patch
2e44c9808e065424c62d81f58798c25f5d749a5f12aa46d62385e610aff563a8730100035ac044d9ea00430cd275ced33184682d1d1b55a46ef8e22d821cc635  utimensat.patch"
