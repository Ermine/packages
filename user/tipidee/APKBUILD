# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=tipidee
pkgver=0.0.5.1
pkgrel=0
pkgdesc="The skarnet.org web server"
url="https://skarnet.org/software/tipidee/"
arch="all"
options="!check"  # No test suite.
license="ISC"
_skalibs_version=2.14.3
_execline_version=2.9.6
_s6_version=2.13.1
_s6net_version=2.7.0
depends="execline>=$_execline_version s6>=$_s6_version s6-networking>=$_s6net_version"
depends_dev="skalibs-dev>=$_skalibs_version"
makedepends="$depends_dev"
subpackages="tipidee-dev tipidee-libs tipidee-doc tipidee-openrc"
source="
	https://skarnet.org/software/tipidee/tipidee-$pkgver.tar.gz
	httpd.run
	httpsd.run
	logger.run
	tipidee.conf
	tipidee.confd
	tipidee.initd
	tipidee.pc.in
	"
install="
	tipidee.pre-install
	tipidee.post-install
	tipidee.post-deinstall
	"
pkggroups="tipidee"
pkgusers="tipideelog tipideed"

build() {
	./configure \
		--enable-shared \
		--disable-allstatic \
		--prefix=/usr \
		--sysconfdir=/etc \
		--libexecdir="/usr/lib/$pkgname"
	make
}

_makeservicedir() {
	name="http${1}d-${2}"
	fn="$pkgdir/var/lib/tipidee/services/$name"
	mkdir -p "$fn/log"
	echo 3 > "$fn/log/notification-fd"
	sed -e "s/@S@/${1}/g; s/@V@/${2}/g;" "$srcdir/logger.run" > "$fn/log/run"
	chmod 0755 "$fn/log/run"
	echo 3 > "$fn/notification-fd"
	sed -e "s/@V@/${2}/g;" "$srcdir/http${1}d.run" > "$fn/run"
	chmod 0755 "$fn/run"
	fn="$pkgdir/var/log/$name"
	mkdir -p "$fn"
	chown tipideelog:tipidee "$fn"
	chmod 02700 "$fn"
}

package() {
	mkdir -p "$pkgdir/usr/share/doc" "$pkgdir/var/lib/tipidee/docroot"
	make DESTDIR="$pkgdir" install
	for i in '' s ; do for j in 4 6 ; do
		_makeservicedir "$i" "$j"
	done ; done
	install -m 0644 -D "$srcdir/tipidee.conf" "$pkgdir/etc/tipidee.conf"
	install -m 0644 -D "$srcdir/tipidee.confd" "$pkgdir/etc/conf.d/tipidee"
	install -m 0755 -D "$srcdir/tipidee.initd" "$pkgdir/etc/init.d/tipidee"
	sed -e "s/@@VERSION@@/$pkgver/g; s/@@SKALIBS_VERSION@@/${_skalibs_version}/g;" "$srcdir/$pkgname.pc.in" > "$srcdir/$pkgname.pc"
	install -D -m 0644 "$srcdir/$pkgname.pc" "$pkgdir/usr/lib/pkgconfig/$pkgname.pc"
	cp -a "$builddir/doc" "$pkgdir/usr/share/doc/$pkgname"
}

dev() {
	default_dev

	# default_dev please stop being stupid
	mv "$subpkgdir/usr/bin/tipidee-config" "$pkgdir/usr/bin/tipidee-config"
}

sha512sums="2f0ae2b3f7c7782785e8498e482bb4cfbb58c0bbe1689b2d3a5a4970783704147e8e5b96737970e2a542ee409e2b179a199a990fbf20b3ea4083cd47d16960cf  tipidee-0.0.5.1.tar.gz
560048b2b767053770e37ce3fd69540f2fad23236f012da115c1f985240f42d0bb7809630d8991e39169e255e3d36ebfdc85f895d16675119a95d3a6e64fd2b2  httpd.run
1c9333785c63662039580504dd36af21d05eb9972e6770af17e87e062bd7d9fa67695f4120221e241e10503ca0fb3b5347f032b6c1eef309f2ff8cd636266f2b  httpsd.run
b5af306fe7040f966b13da84bfb0c4b528683c03205519d08fe429ee826ffad27c9ddb32be8c8bf8b42ce7964ef15572e3c66a96494aed833ee951c769f81bd2  logger.run
bb2681a3000133724bb8fd4918bc3209ea676c2e4f96ad8b298d8e7302aeebd6876341ea205f2d808f34e4c9b5c138bba032694c6069c17026b0ddf23702f9ba  tipidee.conf
7a999200ad21b2b0737ce82136bc8e055d7ab4a865b7727e6dc8df0eaeea6ace11eb5756b64231ce7938d11b5ec218944173ea1df3db059d033bf3f848c9a608  tipidee.confd
5001d079b1ca7920d163cc2d58a5855e2740e72ed85df25414b4719c52133bd0f2f9e69c63c57f31645159189fc046c7d213048fc1cbd0f640336e43874e4482  tipidee.initd
21e71ed438270598de55631c46006bcb66528889477d1a09c76db4160d46651b1c657b31913393115c01aae4853afa4fe425a6fb3985ea8fa403de1d36e3f2f1  tipidee.pc.in"
