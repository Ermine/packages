# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=solid
pkgver=5.94.0
pkgrel=0
pkgdesc="Platform-independent hardware discovery and access"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.0+"
depends="udisks2 upower"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev eudev-dev"
makedepends="$depends_dev cmake extra-cmake-modules flex bison qt5-qttools-dev
	doxygen"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/solid-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="c72da5c52512c97a4ff164e34d06d572a7747f9f434ad574bc3bd106574bef617db48a1b00b2cca6b944f9ab5bf74563e36d25fa7618fd56782506d61ebfea37  solid-5.94.0.tar.xz"
