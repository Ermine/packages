# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sonnet
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework for implementing portable spell check functionality"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules aspell-dev hunspell-dev
	qt5-qtdeclarative-dev qt5-qttools-dev doxygen graphviz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-hunspell"
replaces="sonnet-aspell"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/sonnet-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# Highlighter test requires X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E sonnet-test_highlighter
}

package() {
	make DESTDIR="$pkgdir" install
}

hunspell() {
	pkgdesc="$pkdesc (hunspell backend)"
	install_if="$pkgname=$pkgver-r$pkgrel hunspell"
	mkdir -p "$subpkgdir"/usr/lib/qt5/plugins/kf5/sonnet
	mv "$pkgdir"/usr/lib/qt5/plugins/kf5/sonnet/sonnet_hunspell.so \
		"$subpkgdir"/usr/lib/qt5/plugins/kf5/sonnet/sonnet_hunspell.so
}

sha512sums="4904361fa8b941181c1e18fc39fb614cd9b1177a4d49c6b16ae8c5941e6328f3b803d558d44500ce002a9da92597eecb1a295e956dada994934c4cfe89c62055  sonnet-5.94.0.tar.xz"
