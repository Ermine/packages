# Maintainer: Samuel Holland <samuel@sholland.org>
pkgname=gprbuild
pkgver=24.0.0
pkgrel=0
pkgdesc="An advanced build system for multi-language systems"
url="https://github.com/AdaCore/gprbuild"
arch="all"
options="!check" # No test suite.
license="GPL-3.0+"
depends=""
makedepends="gcc-gnat"
source="https://github.com/AdaCore/gprbuild/archive/v$pkgver/$pkgname-$pkgver.tar.gz
	https://github.com/AdaCore/xmlada/archive/v$pkgver/xmlada-$pkgver.tar.gz
	foxkit.xml
	"

prepare() {
	default_prepare
	ln -s gpr-util-put_resource_usage__unix.adb \
		"$builddir"/gpr/src/gpr-util-put_resource_usage.adb
}

build() {
	xmlada="../xmlada-$pkgver"
	incflags="-Isrc -Igpr/src -I$xmlada/dom -I$xmlada/input_sources \
	          -I$xmlada/sax -I$xmlada/schema -I$xmlada/unicode"
	gcc -c ${CFLAGS} gpr/src/gpr_imports.c -o gpr_imports.o
	for bin in gprbuild gprconfig gprclean gprinstall gprls gprname; do
		gnatmake -j$JOBS $incflags $ADAFLAGS $bin-main \
			-o $bin -cargs $CFLAGS -largs $LDFLAGS gpr_imports.o
	done
	for lib in gprlib gprbind; do
		gnatmake -j$JOBS $incflags $ADAFLAGS $lib \
			-cargs $CFLAGS -largs $LDFLAGS gpr_imports.o
	done
}

package() {
	mkdir -p "$pkgdir"/usr/bin
	cp gprbuild gprconfig gprclean gprinstall gprls gprname \
		"$pkgdir"/usr/bin
	mkdir -p "$pkgdir"/usr/libexec/gprbuild
	cp gprlib gprbind \
		"$pkgdir"/usr/libexec/gprbuild
	mkdir -p "$pkgdir"/usr/share/gpr
	cp share/_default.gpr share/share.gpr \
		"$pkgdir"/usr/share/gpr
	mkdir -p "$pkgdir"/usr/share/gprconfig
	cp "$srcdir"/foxkit.xml \
		"$pkgdir"/usr/share/gprconfig
}

sha512sums="09da4d54f2f151e9f9453e1a459533e2373eb5d4c51831be2bf90964ebcf653752de2f8b271c77f7b599f62146516fc60258ec3c96779fa2b52cd52979c6bd03  gprbuild-24.0.0.tar.gz
4e6773a5fdf9b6ebb5afca913d77aba9f697768f28ef9b23b9277aa4ea4ce09de4e53655dd5559e2326ff847327adb53fa6b776775aa923100a2624df415c97d  xmlada-24.0.0.tar.gz
e369c094963d3dcfb03d7ac0949825531bae6410ef9c4bec774cb0da70d2bd4a784bdec37db5151c0371ce769712ee02fc04f36896ccc8bddcdb585c1ee8dbbc  foxkit.xml"
