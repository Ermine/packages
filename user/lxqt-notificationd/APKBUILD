# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=lxqt-notificationd
pkgver=1.4.0
_lxqt=0.13.0
pkgrel=0
pkgdesc="Daemon for notifications in LXQt"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qttools-dev liblxqt-dev>=${pkgver%.*}
	lxqt-build-tools>=$_lxqt kwindowsystem-dev"
source="https://github.com/lxqt/lxqt-notificationd/releases/download/$pkgver/lxqt-notificationd-$pkgver.tar.xz
	revert-kwindowsystem-bump.patch"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="5ae28b0f49e1e01c8d0fe96f23b961ca962dab33f16025af172777f385d0fd3471c64a7acb78b46e7dc0c370d324a17f10ec9a4c4b3afbde95d47164a5246d0a  lxqt-notificationd-1.4.0.tar.xz
b67f16f2fb37cb50700ecb925e709a3cfe74dcf8a9d4f74909cfd08ec7af9b0ff3e6b20a1a1d3f5d966392be2f25b1a295c9db5b47c7d890e7d026dd6faf2c9a  revert-kwindowsystem-bump.patch"
