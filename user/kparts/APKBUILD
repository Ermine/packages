# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kparts
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework for user interface components"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires X11 running.
license="GPL-2.0+ AND LGPL-2.0+ AND LGPL-2.1+ AND (LGPL-2.0-only OR LGPL-3.0-only) AND CC0-1.0"
depends=""
depends_dev="qt5-qtbase-dev kio-dev kservice-dev ktextwidgets-dev kxmlgui-dev"
docdepends="kio-doc kxmlgui-doc ktextwidgets-doc kcoreaddons-doc kservice-doc
	kcompletion-doc kwidgetsaddons-doc kjobwidgets-doc kbookmarks-doc
	kitemviews-doc kwindowsystem-doc kconfig-doc kconfigwidgets-doc
	solid-doc sonnet-doc ki18n-doc kcodecs-doc kauth-doc"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev kconfig-dev kcoreaddons-dev ki18n-dev kiconthemes-dev
	kjobwidgets-dev kwidgetsaddons-dev $docdepends"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kparts-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="1046617d69c9e67c02a1924d5a20a7e64ee69cb93477d28a8686b4658bdcba8765d9ec7b5f27e4926feaab34db459bbaac9e8f25eb4ee674a7ff622882bec4e8  kparts-5.94.0.tar.xz"
