# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=valgrind
pkgver=3.21.0
pkgrel=0
pkgdesc="A tool to help find memory-management problems in programs"
url="https://valgrind.org/"
arch="all"
# from README_PACKAGERS:
#   Don't strip the debug info off lib/valgrind/$platform/vgpreload*.so
#   in the installation tree.  Either Valgrind won't work at all, or it
#   will still work if you do, but will generate less helpful error
#   messages.
options="!strip !check"
license="GPL-2.0+"
depends=""
makedepends="autoconf automake libtool bash perl cmd:which"
subpackages="$pkgname-dev $pkgname-doc"
source="ftp://sourceware.org/pub/$pkgname/$pkgname-$pkgver.tar.bz2
	0001-Ensure-ELFv2-is-supported-on-PPC64.patch
	arm.patch
	ppc32.patch
	ppc64-support-tkill.patch
	"

prepare() {
	default_prepare
	aclocal && autoconf && automake --add-missing
	echo '#include <linux/a.out.h>' > include/a.out.h
}

build() {
	# fails to build with ccache
	export CC="gcc"
	export CFLAGS="$CFLAGS -fno-stack-protector -no-pie"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--without-mpicc
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	# we have options=!strip above so we strip the /usr/bin/* manually
	if [ -z "$DEBUG" ]; then
		strip "$pkgdir"/usr/bin/valgrind \
			"$pkgdir"/usr/bin/valgrind-di-server \
			"$pkgdir"/usr/bin/vgdb \
			"$pkgdir"/usr/bin/valgrind-listener
	fi
}

sha512sums="3e86cda2f2d6cd30807fac7933ba2c46a97a2b4a868db989e6b0cceeadf029af7ee34ba900466a346948289aacb30f4399799bb83b97cc49a4d2d810441e5cfd  valgrind-3.21.0.tar.bz2
f90aa3cef5d3561d4602ff63b8f355b8427cbe6bb2ecd6bfc2e2bb063915fa40a70d923dbc60a643ee3fb7bcfbbddac049c6f6133c541189dd82b891f7ad9dbc  0001-Ensure-ELFv2-is-supported-on-PPC64.patch
9ee297d1b2b86891584443ad0caadc4977e1447979611ccf1cc55dbee61911b0b063bc4ad936d86c451cedae410cb3219b5a088b2ad0aa17df182d564fe36cfe  arm.patch
25804fafe2372808c29233ec7415a94633ef17c72c9bd1d78a5c1da53fad2ed8e76da2d0923ff0a73fad8a86b7709c5ea3774b35be6a5b4c1ef1a55bc86b2c18  ppc32.patch
daf819d0f9bbbaf4243c7bf931fed840f1f55304df1b45d4a72ceaf8403b8e0e1fc8079343b86106f0741aa812560a466389d7d3878c5547f3ff7a9e46e63df7  ppc64-support-tkill.patch"
