# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=qqc2-desktop-style
pkgver=5.94.0
pkgrel=0
pkgdesc="QtQuickControls 2 style that uses QWidget's QStyle for painting"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0-only AND LGPL-3.0-only"
depends="sonnet"
makedepends="cmake extra-cmake-modules kconfig-dev kconfigwidgets-dev
	kiconthemes-dev kirigami2-dev qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev qt5-qtx11extras-dev sonnet-dev"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/qqc2-desktop-style-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-Bbuild .
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="8f68423603d7de0a286dbf78441b077fc48197d9bc6863e7b13be358f68697f1ad95559e0e0705edb034c54df75ff74ca41f5b2fe70ba94e55e7c30b13c6989f  qqc2-desktop-style-5.94.0.tar.xz"
