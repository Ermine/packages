# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gstreamer
pkgver=1.20.1
pkgrel=0
pkgdesc="GStreamer multimedia framework"
url="https://gstreamer.freedesktop.org/"
arch="all"
license="LGPL-2.0+"
depends=""
depends_dev="libxml2-dev"
makedepends="$depends_dev bison flex gobject-introspection-dev glib-dev
	meson ninja"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools $pkgname-lang"
source="https://gstreamer.freedesktop.org/src/gstreamer/gstreamer-$pkgver.tar.xz
	disable-tests.patch
	test-deadlock.patch
	time64.patch
	"

build() {
	meson \
		-Dprefix=/usr \
		-Dintrospection=enabled \
		-Dpackage-name="GStreamer (${DISTRO_NAME:-Adélie Linux})" \
		-Dpackage-origin="${DISTRO_URL:-https://www.adelielinux.org/}" \
		-Dglib-asserts=disabled \
		-Dnls=enabled \
		-Dptp-helper-permissions=none \
		_build
	ninja -C _build
}

check() {
	ninja -C _build test
}

package() {
	DESTDIR="$pkgdir" ninja -C _build install
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/share/gstreamer-1.0
	mv "$pkgdir"/usr/share/gdb "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/gstreamer-1.0/gdb \
		"$subpkgdir"/usr/share/gstreamer-1.0/
}

tools() {
	pkgdesc="Tools and files for GStreamer streaming media framework"
	# gst-feedback needs this
	depends="cmd:pkg-config"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
	mv "$pkgdir"/usr/lib/libgstcheck-1.0.so.* "$subpkgdir"/usr/lib/
}

sha512sums="d6f67cce81ba15fba3fcf70850e3c84b25e8da3e53fd56e6f2c87c7ee1701071ea44deb754a0ea371b3cb17877f26aab94d9eccb6729cbb370d6d6d5c324aa1a  gstreamer-1.20.1.tar.xz
39e6bfb52ebe85beefa2550eb404f83c5bbb3546cc1733b99e757401d2d182f95d829b90df99d5f43506d7c85d4f44ac797f35653cd42a935f1dc56d0b844c02  disable-tests.patch
d7e574e8715607d3103d46eb05388b781702a9e937d78f8cfab6d8d48d04baa7fbfe547bdeb816bbfddb5a333dd1862e460b057b12ea24704351ef5653f78491  test-deadlock.patch
977a9e689cce68b151a742ef67dbb60a10a55fcfae67f086909e1f0fc4a5998026acd03aeed32068fdb0485dd884c4313f39a63e3bab5baaafa878c439531bc3  time64.patch"
