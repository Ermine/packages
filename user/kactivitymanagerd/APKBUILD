# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=kactivitymanagerd
pkgver=5.24.5
pkgrel=0
pkgdesc="Service to manage KDE Plasma activities"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0-only AND GPL-2.0+ AND GPL-3.0-only AND LGPL-2.1-only AND LGPL-3.0-only"
depends="qt5-qtbase-sqlite"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kdbusaddons-dev ki18n-dev
	boost-dev python3 kconfig-dev kcoreaddons-dev kwindowsystem-dev kio-dev
	kglobalaccel-dev kxmlgui-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/kactivitymanagerd-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild .
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="971105d22e7058b4c62ae00bef160b629cdef59e84cfc1d5fe6f7d9e43d12e71f8c93a56ef933f58729f71ead1d6b5c2247828a5a99bbe8300e0312888421b9c  kactivitymanagerd-5.24.5.tar.xz"
