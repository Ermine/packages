# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=lxqt-openssh-askpass
pkgver=1.4.0
_lxqt=0.13.0
pkgrel=0
pkgdesc="Graphical LXQt utility for inputting passwords for SSH agents"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qttools-dev lxqt-build-tools>=$_lxqt
	liblxqt-dev>=${pkgver%.*}.0 kwindowsystem-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxde/lxqt-openssh-askpass/releases/download/$pkgver/lxqt-openssh-askpass-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="3f6862c70cc184246433eb0d9c3ad4a045f171f25144288b3e244c6e203c3bcfb14da62505b586f0b20b27a5b4b5c39ffbc709bf88863f87deb5b4a610cfd26f  lxqt-openssh-askpass-1.4.0.tar.xz"
