# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kalgebra
pkgver=22.04.2
pkgrel=0
pkgdesc="Graph calculator and plotter"
url="https://www.kde.org/applications/education/kalgebra/"
arch="all"
license="GPL-2.0-only"
depends="kirigami2 qt5-qtquickcontrols"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtsvg-dev analitza-dev ncurses-dev ki18n-dev kdoctools-dev kio-dev
	kconfigwidgets-dev kwidgetsaddons-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kalgebra-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="4b605623e9edef4e5bf8911ebc7e4582df7a584f837b88a37c15eabb40c858dcb456f9ddbeea61c4345d93295152d697cad6fc7dd62fa9fe86f0fd36417fae91  kalgebra-22.04.2.tar.xz"
