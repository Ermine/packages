# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=polkit
pkgver=123
pkgrel=0
pkgdesc="Toolkit for controlling system-wide privileges"
url="https://www.freedesktop.org/wiki/Software/polkit/"
arch="all"
options="!check suid"  # Requires running ConsoleKit and PolKit for JS backend
license="LGPL-2.0+"
depends=""
makedepends="glib-dev gobject-introspection-dev gtk-doc linux-pam-dev meson
	duktape-dev elogind-dev"
pkgusers="polkitd"
pkggroups="polkitd"
install="$pkgname.pre-install $pkgname.pre-upgrade"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://gitlab.freedesktop.org/polkit/polkit/-/archive/$pkgver/polkit-$pkgver.tar.bz2
	fix-consolekit-db-stat.patch
	fix-test-fgetpwent.patch
	"

# secfixes:
#   0.123-r0:
#     - CVE-2021-4034
#   0.115-r2:
#     - CVE-2018-19788

build() {
	meson setup . build \
		-Dsession_tracking=libelogind \
		-Dman=true \
		-Dpam_include='base-auth'
	meson compile -C build
}

check() {
	meson test -C build
}

package() {
	meson install -C build --destdir="$pkgdir"
}

sha512sums="4306363d3ed7311243de462832199bd10ddda35e36449104daff0895725d8189b07a4c88340f28607846fdf761c23470da2d43288199c46aa816426384124bb6  polkit-123.tar.bz2
bfefe2398f97138391ed34630e2994670dddaa0b13585e2e7cb101e7d11e3054dd491244ec84116b908d0f126a69032c467d83a0c52b0bb980d9b10290600745  fix-consolekit-db-stat.patch
966825aded565432f4fda9e54113a773b514ebf7ee7faa83bcb8b97d218ae84a8707d6747bbc3cb8a828638d692fdef34c05038f150ad38e02a29f2c782aba5b  fix-test-fgetpwent.patch"
