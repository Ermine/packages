# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=layer-shell-qt
pkgver=5.24.5
pkgrel=0
pkgdesc="Qt library to consume Wayland wl-layer-shell protocol"
url="https://www.kde.org/"
arch="all"
license="LGPL-3.0+ AND MIT AND BSD-3-Clause AND CC0-1.0"
depends=""
makedepends="cmake extra-cmake-modules libxkbcommon-dev qt5-qtbase-dev
	qt5-qtdeclarative-dev qt5-qtwayland-dev qt5-qtwayland-tools wayland-dev
	wayland-protocols"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/plasma/$pkgver/layer-shell-qt-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="e9aeb94d7200543eef03950f6e3ccf3a60faf66bcf048760ea35d27c621bbced712411501f9d48fdaf98a36708a1a202c0531bbc94df0ccf99278ffd106b7abc  layer-shell-qt-5.24.5.tar.xz"
