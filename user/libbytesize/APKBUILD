# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libbytesize
pkgver=2.3
pkgrel=0
pkgdesc="Library for working with sizes in bytes"
url="https://github.com/storaged-project/libbytesize/"
arch="all"
options="!check"  # Requires locale(1).
license="LGPL-2.1+"
depends=""
makedepends="gmp-dev mpfr-dev pcre2-dev python3"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang py3-bytesize:py:noarch"
source="https://github.com/storaged-project/$pkgname/releases/download/$pkgver/$pkgname-$pkgver.tar.gz
	no-msgcat.patch"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--without-python2
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

py() {
	pkgdesc="$pkgdesc (Python bindings)"
	depends="py3-six"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/python* "$subpkgdir"/usr/lib/
}

sha512sums="23720b90f4dbf7880a640ec04e910c4c1f34637dd3621900772187cb2e1d04ec34d4900ce3c9b4083ac462b411d5a409a644f62ed76b2c57ef1f11432c58be8a  libbytesize-2.3.tar.gz
5f8b46c257553672b7c2501bae99ff44594b91bfcf3f1ee209a390a8cdda693616e8207a99cea2e1263093324807a307dac9e643d1482e14f9ba604f51a05d6d  no-msgcat.patch"
