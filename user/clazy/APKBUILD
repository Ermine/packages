# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=clazy
pkgver=1.12
_llvmver=18
pkgrel=0
pkgdesc="Clang compiler plugin to warn about Qt best practices"
url="https://kde.org/applications/development/org.kde.clazy"
arch="all"
license="LGPL-2.1+"
options="!check"  # need 'clang-tools-extra'
depends=""
makedepends="cmake clang-dev llvm$_llvmver-dev"
subpackages="$pkgname-doc"
source="https://download.kde.org/stable/clazy/$pkgver/src/clazy-$pkgver.tar.xz
	fix-link-fs-lib.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DLLVM_ROOT="/usr/lib/llvm$_llvmver" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="7a2b4a753098f15936a4731c82c660c1cd4d81949279435e87f00f7f72bef3e32cb574c8991c0707798581278e9fbcae34d354c1e6b77a15a5e26166d658fa9e  clazy-1.12.tar.xz
92e10ad26bae84021b6d076d90e46ad570c9bcb676bcdd3f0d53b46b47b379fb0c757310e91b995851703892003cbc870dacef7de5419575502eccc2715a8ea7  fix-link-fs-lib.patch"
