# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=uchardet
pkgver=0.0.8
pkgrel=0
pkgdesc="Universal character encoding detection library"
url="https://www.freedesktop.org/wiki/Software/uchardet/"
arch="all"
license="MPL-1.1 OR GPL-2.0+ OR LGPL-2.1+"
depends=""
makedepends="cmake"
subpackages="$pkgname-dev $pkgname-doc"
source="https://www.freedesktop.org/software/uchardet/releases/uchardet-$pkgver.tar.xz
	no-debug-suffix.patch"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="4a5dcc9ff021352f3b252e103ff1475cec62c974294b264ee9243f024633c3ae44be8c7733608624066113e635f8b156ecb08c8ff87c736d04b07641eb166382  uchardet-0.0.8.tar.xz
2e6f9d34daa6ba7dab07843368017448a17162d4d55bbc01665d00afa61a36f8b44fd6149706b806c8980632bdf33d073e0a93559d809852e18460551f330ecb  no-debug-suffix.patch"
