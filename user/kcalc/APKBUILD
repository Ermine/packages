# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcalc
pkgver=22.04.2
pkgrel=0
pkgdesc="Calculator with many mathematical, scientific, and logic functions"
url="https://utils.kde.org/projects/kcalc/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kconfig-dev ki18n-dev
	kconfigwidgets-dev kdoctools-dev kguiaddons-dev kinit-dev kxmlgui-dev
	knotifications-dev gmp-dev mpfr-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcalc-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="ed650159e1801ef90022205117807420e2d1e9e205bc0f26363d26e76510a2c17622646d7ea4ca5fe8b4edbad89f75854f2b2f6a7c30f8d36705a0c722fffe32  kcalc-22.04.2.tar.xz"
