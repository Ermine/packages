# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kanagram
pkgver=22.04.2
pkgrel=0
pkgdesc="Letter order (anagram) game"
url="https://www.kde.org/applications/education/kanagram/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	ki18n-dev kcrash-dev sonnet-dev kconfig-dev kconfigwidgets-dev kio-dev
	kcoreaddons-dev kdeclarative-dev kdoctools-dev knewstuff-dev
	libkeduvocdocument-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kanagram-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="66790eff8a6d8b8e6b37b27ab757033468d8db239ec8397252a9bb01477a9d1ef184d33cdcfcc5bb7c0da29bd69556caff36059ef463f568c208a57ae498379d  kanagram-22.04.2.tar.xz"
