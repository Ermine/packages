# Contributor: Travis Tilley <ttilley@gmail.com>
# Contributor: Mitch Tishmack <mitch.tishmack@gmail.com>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
_pkgname=llvm
pkgver=18.1.8
_majorver=${pkgver%%.*}
pkgname=$_pkgname$_majorver
pkgrel=0
pkgdesc="Low Level Virtual Machine compiler system, version $_majorver"
url="https://llvm.org/"
arch="all"
options="!checkroot !dbg"
license="NCSA AND (Apache-2.0 WITH LLVM-exception)"
depends=""
depends_dev="$pkgname=$pkgver-r$pkgrel libexecinfo-dev libxml2-dev"
checkdepends="cmd:which"
makedepends="binutils-dev chelf chrpath cmake file libexecinfo-dev libffi-dev
	libxml2-dev python3 zlib-dev"
subpackages="$pkgname-static $pkgname-libs $pkgname-dev
	$pkgname-test-utils:_test_utils"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/llvm-project-$pkgver.src.tar.xz
	llvm-fix-build-with-musl-libc.patch
	disable-FileSystemTest.CreateDir-perms-assert.patch
	dyld-elf-ppc32.patch
	hexagon.patch
	i586-json-test.patch
	m68k-endianness.patch
	macho32.patch
	musl-ppc64-elfv2.patch
	ppc-gcc-bug.patch
	python3-test.patch
	roundeven.patch
	"
builddir="$srcdir/$_pkgname-project-$pkgver.src/llvm"

# ARM has few failures in test suite that we don't care about currently and
# also it takes forever to run them on the builder.
case "$CARCH" in
	arm*) options="$options !check";;
esac

# Whether is this package the default (latest) LLVM version.
_default_llvm="yes"

if [ "$_default_llvm" = yes ]; then
	provides="llvm=$pkgver-r$pkgrel"
	replaces="llvm"
fi

# NOTE: It seems that there's no (sane) way how to change includedir, sharedir
# etc. separately, just the CMAKE_INSTALL_PREFIX. Standard CMake variables and
# even  LLVM-specific variables, that are related to these paths, actually
# don't work (in llvm 3.7).
#
# When building a software that depends on LLVM, utility llvm-config should be
# used to discover where is LLVM installed. It provides options to print
# path of bindir, includedir, and libdir separately, but in its source, all
# these paths are actually hard-coded against INSTALL_PREFIX. We can patch it
# and move paths manually, but I'm really not sure what it may break...
#
# Also note that we should *not* add version suffix to files in llvm bindir!
# It breaks build system of some software that depends on LLVM, because they
# don't expect these files to have a sufix.
#
# So, we install all the LLVM files into /usr/lib/llvm$_majorver.
# BTW, Fedora and Debian do the same thing.
#
_prefix="usr/lib/llvm$_majorver"

prepare() {
	default_prepare

	# https://bugs.llvm.org//show_bug.cgi?id=31870
	rm -v test/tools/llvm-symbolizer/print_context.c

	case $CARCH in
	pmmx|x86|ppc|armhf|armv7)
		# Appears to not work when building 32-bit code on 64-bit host.
		rm -v test/tools/llvm-size/radix.test;;
	esac

	case $CARCH in
	ppc64|ppc|mips64|mips)
		# Appears to be a endianness issue.
		for _broken in eh_frame linker-llvm-union-fwd-decl; do
			rm -v test/tools/dsymutil/X86/$_broken.test;
		done
	esac

	case $CTARGET_ARCH in
	pmmx)
		# Broken by fast precision method.
		rm -v test/tools/llvm-cov/mcdc-export-json.test;
		rm -v test/tools/llvm-xray/X86/graph-diff-simple.txt;
		;;
	ppc)
		# RISC-V issues are uninvestigated as of yet.  #1233
		rm -v test/CodeGen/RISCV/rvv/combine-store-extract-crash.ll
		rm -v test/CodeGen/RISCV/xtheadmempair.ll

		# ExecutionEngine for PPC32 requires R_PPC_REL24 to be
		# implemented in RuntimeDyld.
		for _bad_orc in global-ctor-with-cast.ll \
			global-variable-alignment.ll \
			trivial-call-to-function.ll \
			trivial-call-to-internal-function.ll \
			trivial-reference-to-global-variable.ll \
			trivial-reference-to-internal-variable-nonzeroinit.ll \
			trivial-reference-to-internal-variable-zeroinit.ll \
			trivial-return-zero.ll \
			weak-comdat.ll; do
			rm -v test/ExecutionEngine/Orc/$_bad_orc;
		done
		for _bad_ee in frem.ll mov64zext32.ll \
			test-interp-vec-arithm_float.ll \
			test-interp-vec-arithm_int.ll \
			test-interp-vec-logical.ll \
			test-interp-vec-setcond-fp.ll \
			test-interp-vec-setcond-int.ll; do
			rm -v test/ExecutionEngine/$_bad_ee;
		done
	esac

	#1053
	rm -vfr \
		test/tools/UpdateTestChecks \
		test/ExecutionEngine/MCJIT/load-object-a.ll \
		;

	#1250
	rm -v \
		test/tools/dsymutil/ARM/DWARFLinkerParallel/accel-imported-declarations.test \
		test/tools/dsymutil/X86/DWARFLinkerParallel/odr-member-functions.cpp \
		test/tools/dsymutil/X86/DWARFLinkerParallel/odr-parents.test \
		test/tools/dsymutil/X86/DWARFLinkerParallel/odr-predictable-output2.test \
		test/tools/dsymutil/X86/DWARFLinkerParallel/odr-string.test \
		test/tools/dsymutil/X86/DWARFLinkerParallel/odr-uniquing.cpp
}

build() {
	# Auto-detect it by guessing either.
	ffi_include_dir="$(pkg-config --cflags-only-I libffi | sed 's|^-I||g')"

	arch_config=""
	case $CTARGET_ARCH in
	aarch64)
		# libexecinfo does not work on AArch64 because the GCC port
		# does not implement RETURN_ADDR_RTX with frame > 0.
		arch_config="-DLLVM_ENABLE_BACKTRACES=OFF";;
	pmmx)
		# The standard precision breaks more than it fixes; there are
		# 12 failures in standard mode and only (different) 3 in fast.
		export CXXFLAGS="$CXXFLAGS -fexcess-precision=fast";;
	esac

	case $CTARGET_ARCH in
	pmmx|x86|ppc|armhf|armv7)
		# 32-bit 2GB memory ceiling
		# 'ld' chokes on 2GB .debug_info, 'scanelf' on binary size
		# see also: #1251, #1253, abuild#20
		export CXXFLAGS="$CXXFLAGS -ggdb1";;
	esac

	cmake -G "Unix Makefiles" -Wno-dev \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/$_prefix \
		-DCMAKE_VERBOSE_MAKEFILE=NO \
		-DFFI_INCLUDE_DIR="$ffi_include_dir" \
		-DLLVM_BINUTILS_INCDIR=/usr/include \
		-DLLVM_BUILD_DOCS=OFF \
		-DLLVM_BUILD_EXAMPLES=OFF \
		-DLLVM_BUILD_EXTERNAL_COMPILER_RT=ON \
		-DLLVM_BUILD_LLVM_DYLIB=ON \
		-DLLVM_BUILD_TESTS=ON \
		-DLLVM_DEFAULT_TARGET_TRIPLE="$CBUILD" \
		-DLLVM_ENABLE_ASSERTIONS=OFF \
		-DLLVM_ENABLE_FFI=ON \
		-DLLVM_ENABLE_LIBCXX=OFF \
		-DLLVM_ENABLE_PIC=ON \
		-DLLVM_ENABLE_RTTI=ON \
		-DLLVM_ENABLE_SPHINX=OFF \
		-DLLVM_ENABLE_TERMINFO=ON \
		-DLLVM_ENABLE_ZLIB=ON \
		-DLLVM_HOST_TRIPLE="$CHOST" \
		-DLLVM_INCLUDE_BENCHMARKS=OFF \
		-DLLVM_INCLUDE_EXAMPLES=OFF \
		-DLLVM_INSTALL_GTEST=ON \
		-DLLVM_INSTALL_UTILS=ON \
		-DLLVM_LINK_LLVM_DYLIB=ON \
		-DLLVM_TARGETS_TO_BUILD='AArch64;AMDGPU;ARM;BPF;Hexagon;Lanai;Mips;MSP430;PowerPC;RISCV;Sparc;SystemZ;WebAssembly;X86;XCore' \
		-DLLVM_EXPERIMENTAL_TARGETS_TO_BUILD='M68k' \
		$arch_config \
		-Bbuild .

	make -C build llvm-tblgen
	make -C build

	python3 utils/lit/setup.py build

	# Thread 3 requires a lot of stack space to LTO when targeting ARM.
	# Note that this occurs even when crossing (on a ppc64 host).
	chelf -s 1048576 build/bin/llvm-lto2
	# Needed for parallel-linker support (pthread stack size).
	chelf -s 1048576 build/bin/dsymutil
	chelf -s 1048576 build/bin/llvm-dwarfutil
}

check() {
	make -C build check-llvm
}

package() {
	make DESTDIR="$pkgdir" -C build install

	cd "$pkgdir"/$_prefix

	# Remove RPATHs.
	file lib/*.so bin/* \
		| awk -F: '$2~/ELF/{print $1}' \
		| xargs -r chrpath -d

	# Symlink files from /usr/lib/llvm*/bin to /usr/bin.
	mkdir -p "$pkgdir"/usr/bin
	for full_name in bin/*; do
		bin_name=${full_name##*/}
		# If this package provides=llvm (i.e. it's the default/latest
		# llvm package), omit version infix/suffix.
		if [ "$_default_llvm" = yes ]; then
			link_name=$bin_name
		# ..otherwise, add version infix/suffix to the executable name.
		else case "$bin_name" in
			llvm-*) link_name="llvm$_majorver-${bin_name#llvm-}";;
			*) link_name="$bin_name$_majorver";;
		esac
		fi
		case "$name" in
			FileCheck | obj2yaml | yaml2obj) continue;;
		esac
		ln -s ../lib/llvm$_majorver/bin/$bin_name "$pkgdir"/usr/bin/$link_name
	done

	# Move /usr/lib/$pkgname/include/ into /usr/include/$pkgname/
	# and symlink it back.
	mkdir -p "$pkgdir"/usr/include/$pkgname
	mv include/* "$pkgdir"/usr/include/$pkgname/
	rmdir include
	ln -s ../../include/$pkgname include

	ln -s "$pkgdir"/usr/lib/cmake/llvm ../$pkgname/lib/cmake/llvm

	# Thread 3 requires a lot of stack space to LTO when targeting ARM.
	# Note that this occurs even when crossing (on a ppc64 host).
	chelf -s 1048576 bin/llvm-lto2
	# Needed for parallel-linker support (pthread stack size).
	chelf -s 1048576 bin/dsymutil
	chelf -s 1048576 bin/llvm-dwarfutil
}

static() {
	pkgdesc="LLVM $_majorver static libraries"
	_common_subpkg

	mkdir -p "$subpkgdir"/$_prefix/lib
	mv "$pkgdir"/$_prefix/lib/*.a "$subpkgdir"/$_prefix/lib/
	strip -d "$subpkgdir"/$_prefix/lib/*.a
}

libs() {
	pkgdesc="LLVM $_majorver runtime library"
	main_soname="libLLVM-$_majorver.so"
	ver_soname="libLLVM-$pkgver.so"
	_common_subpkg

	# libLLVM should be in /usr/lib. This is needed for binaries that are
	# dynamically linked with libLLVM, so they can find it on default path.
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/$_prefix/lib/$main_soname "$subpkgdir"/usr/lib/
	_solinks=$(ls "$pkgdir"/$_prefix/lib/libLLVM.so.$_majorver*)
	for link in $_solinks; do
		_real_link=$(basename $link)
		mv "$pkgdir"/$_prefix/lib/$_real_link "$subpkgdir"/usr/lib/
	done
	ln -s $main_soname "$subpkgdir"/usr/lib/$ver_soname

	# And also symlink it back to the LLVM prefix.
	mkdir -p "$subpkgdir"/$_prefix/lib
	ln -s ../../$main_soname "$subpkgdir"/$_prefix/lib/$main_soname
	ln -s ../../$main_soname "$subpkgdir"/$_prefix/lib/$ver_soname
	ln -s ../../$main_soname "$subpkgdir"/$_prefix/lib/libLLVM.so
	for link in $_solinks; do
		_real_link=$(basename $link)
		ln -s ../../$main_soname "$subpkgdir"/$_prefix/lib/$_real_link
	done
}

dev() {
	_common_subpkg
	default_dev

	mkdir -p "$subpkgdir"/$_prefix/bin

	mv "$pkgdir"/$_prefix/lib "$subpkgdir"/$_prefix/
	mv "$pkgdir"/$_prefix/include "$subpkgdir"/$_prefix/

	mv "$pkgdir"/$_prefix/bin/llvm-config "$subpkgdir"/$_prefix/bin/
}

_test_utils() {
	pkgdesc="LLVM $_majorver utilities for executing LLVM and Clang style test suites"
	depends="python3"
	_common_subpkg
	replaces=""

	litver="$(python3 "$builddir"/utils/lit/setup.py --version 2>/dev/null \
		| sed 's/\.dev.*$//')"
	test -n "$litver" || return 1
	provides="$provides lit=$litver-r$pkgrel"

	cd "$builddir"/build

	install -D -m 755 bin/FileCheck "$subpkgdir"/$_prefix/bin/FileCheck
	install -D -m 755 bin/count "$subpkgdir"/$_prefix/bin/count
	install -D -m 755 bin/not "$subpkgdir"/$_prefix/bin/not

	python3 ../utils/lit/setup.py install --prefix=/usr --root="$subpkgdir"
	ln -s ../../../bin/lit "$subpkgdir"/$_prefix/bin/lit
}

_common_subpkg() {
	if [ "$_default_llvm" = yes ]; then
		replaces="llvm${subpkgname#$pkgname}"
		provides="$replaces=$pkgver-r$pkgrel"
	fi
}

sha512sums="25eeee9984c8b4d0fbc240df90f33cbb000d3b0414baff5c8982beafcc5e59e7ef18f6f85d95b3a5f60cb3d4cd4f877c80487b5768bc21bc833f107698ad93db  llvm-project-18.1.8.src.tar.xz
f84cd65d7042e89826ba6e8d48c4c302bf4980da369d7f19a55f217e51c00ca8ed178d453df3a3cee76598a7cecb94aed0775a6d24fe73266f82749913fc3e71  llvm-fix-build-with-musl-libc.patch
d56945bb0476561028616222846257f990d66e68b4458894f8791252411038b269831f9400ed9df3b99f571a82443caaac347a8b38a5516c77c3583469118309  disable-FileSystemTest.CreateDir-perms-assert.patch
9e919d7988b18c7184d9fccd14911d70bbe2acf82d38111bfd41f1ba2976fea07e74527a43c552dad158c982a2cbaaf03f6a1e98c7a1fc02b4e75382cfe6d0b2  dyld-elf-ppc32.patch
807587a0c897d47a01a846c5c4f7cbf1f2d16437a163b66ee2381a7147e9d04b0141b2c76d022db61903d0d2841ddb267ba98c66c9e41501ca41837659743df8  hexagon.patch
22dedbbdc99ab4df6c64921186803552fc61cfc00a5cd052295a5450809e7205ac21dd07df158b7e99b960084b2ec3d8832480a8cd073fe1c2d613cd21c90b60  i586-json-test.patch
b186ce027dca389cfd23dc91b03e023e688c7610745cf62d0b277b6178972bcf55abf0df08453f2a6b85dc61dba929cb565cb30633a2fe0f193a443f54025bf7  m68k-endianness.patch
64a28241b9ea87cfc4313440bf53b1c85dff9a1c84846f5b7619b67842acd68da878a1196baccb1e28f44a1484c09d5bffd4cf14341ed2b24642006e6d87b90a  macho32.patch
bec1fe4761dae33a8012a54b7bbdd179cac82febcd46ace1844772c06f207661c903f05fcb7a5ebbbece3e33a88b20a5d9b31b40e8a8cd5c626aec04cb6735be  musl-ppc64-elfv2.patch
bda83678acb97be4d4236179792b3555b8000242fa6bd9e2e78dcd4f6182395df7fa9b8742fde946f19b668615ab37db8670340d43683787d36c540937bca523  ppc-gcc-bug.patch
89ef5fbab039b017a5652656adf17b680525f1c5dd0b1afc8034fe0b34a2ca196d87640f54cc826356cfcd88c6e5f10754b7e38f04ca49ede4f3864080b787bd  python3-test.patch
3dc797036dd976db76c774c4b87609d8548cfcd6575dd1e5322a2170fca4419b6934e12874f1c6650d73b858527d22d70e7abe5270dff79c190feeeb1d965d0a  roundeven.patch"
