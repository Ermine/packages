# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=tmux
pkgver=3.5a
pkgrel=0
pkgdesc="Tool to control multiple terminals from a single terminal"
url="https://github.com/tmux/tmux/wiki"
arch="all"
license="MIT"
depends="ncurses-terminfo"
makedepends="bsd-compat-headers libevent-dev libutempter-dev ncurses-dev"
subpackages="$pkgname-doc"
source="https://github.com/tmux/tmux/releases/download/$pkgver/$pkgname-$pkgver.tar.gz
	no-yacc-needed.patch
	"

prepare() {
	default_prepare
	autoreconf -fiv
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	install -Dm644 example_tmux.conf \
		"$pkgdir"/usr/share/doc/$pkgname/examples/$pkgname.conf
	install -m644 "CHANGES" "$pkgdir"/usr/share/doc/$pkgname/
	install -m644 "README" "$pkgdir"/usr/share/doc/$pkgname/
}

sha512sums="2383e99aec2dcdb1d899793d5ecab40a68b921194f84770d3f4d19712bfc85590657a99d2a9a7bec36d4ba5ab39fa713f13937b0acad3b61cd9b2302dba61d43  tmux-3.5a.tar.gz
54494c910f361e23433f276c49e6257b1ccb028ffba1af032e5494e786eb35506d495714916774956db875306e090edd0c2bf8433b2cffc50c4a25d45dfb0594  no-yacc-needed.patch"
