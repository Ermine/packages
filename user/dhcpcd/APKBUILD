# Contributor: Michael Mason <ms13sp@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=dhcpcd
pkgver=9.5.2
pkgrel=0
pkgdesc="RFC2131 compliant DHCP client"
url="https://roy.marples.name/projects/dhcpcd"
arch="all"
license="BSD-2-Clause"
makedepends="linux-headers bsd-compat-headers eudev-dev"
install="$pkgname.post-upgrade"
subpackages="$pkgname-doc $pkgname-openrc"
source="https://github.com/NetworkConfiguration/dhcpcd/releases/download/v${pkgver}/$pkgname-$pkgver.tar.xz
	fix-chrony-conf-location.patch
	dhcpcd.initd
	"

build() {
	CFLAGS="$CFLAGS -D_GNU_SOURCE -DHAVE_PRINTF_M"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--libexecdir=/usr/lib/$pkgname \
		--dbdir=/var/lib/$pkgname \
		--rundir=/run \
		--enable-ipv6
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm755 "$srcdir"/dhcpcd.initd \
		"$pkgdir"/etc/init.d/dhcpcd
}

sha512sums="de9040b6ce6b4eb6dbf193fe3d983984a58a3bc14384bdd7ffbd2040056d72a786bc033a8bc69f7df37fbf9202b35ff3bca4196ae31ee78670eed0d779e5fd6a  dhcpcd-9.5.2.tar.xz
1c19eed0f7a008ee96ea392beb327169ff8c83fc27fed20f65f05c9125f60629ebe3474c5e6a7cf4aeeea448fde4264c9b84916efacd67d47ab908c47b1fc3a5  fix-chrony-conf-location.patch
e777432c2efc84285b41e63a4687f3bd543f6864218d037529ab78b5ad934de154f28f478bd9facb56628f2953aad8a932bc2eb8b1dfffa0ce2278ffcfc4d880  dhcpcd.initd"
