# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=akonadi-search
pkgver=22.04.2
pkgrel=0
pkgdesc="Search functionality for Akonadi"
url="https://kde.org/"
arch="all"
license="LGPL-2.1+ AND (LGPL-2.1-only OR LGPL-3.0-only) AND GPL-2.0-only AND GPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev cmake extra-cmake-modules kcmutils-dev kconfig-dev
	kcrash-dev kdbusaddons-dev ki18n-dev kio-dev krunner-dev akonadi-dev
	akonadi-mime-dev boost-dev kcalendarcore-dev kcontacts-dev kmime-dev
	xapian-core-dev kauth-dev kcodecs-dev kcoreaddons-dev kitemmodels-dev
	kjobwidgets-dev kpackage-dev kservice-dev solid-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-search-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	# sqlite backend requires D-Bus server.
	QT_QPA_PLATFORM=offscreen CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E 'akonadi-sqlite*|akonadi-pgsql*'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="ddffb521c8e0ac368b41bf9c3e93dca84b977affa19aa70525a51fa146a337fdf25bb804abf98aab0f0666592c72b09e998322253b852ab8b6e0e8266594a953  akonadi-search-22.04.2.tar.xz"
