# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kemoticons
pkgver=5.94.0
pkgrel=0
pkgdesc="Emoticons to express emotions in KDE"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test requires accelerated X11 environment.
license="LGPL-2.1+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev karchive-dev kconfig-dev kservice-dev"
docdepends="kservice-doc kconfig-doc kcoreaddons-doc"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen
	graphviz $docdepends"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kemoticons-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="1b8d34fb58ef6b8ba83cc13eac719143606d1011b39551513773e78f660a3c7c4f3ed3402cb68bac9c216275cba7c1fdf6fdf2d905d377f9bf79a1eeac850b46  kemoticons-5.94.0.tar.xz"
