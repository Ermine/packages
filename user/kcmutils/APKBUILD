# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcmutils
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework for writing System Settings modules"
url="https://api.kde.org/frameworks/kcmutils/html/index.html"
arch="all"
options="!check"  # Test suite requires running X11 session.
license="LGPL-2.1-only AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev kitemviews-dev kservice-dev
	kconfigwidgets-dev kiconthemes-dev kdeclarative-dev kxmlgui-dev"
docdepends="kconfigwidgets-doc kservice-doc kcodecs-doc kwidgetsaddons-doc
	kconfig-doc kauth-doc kcoreaddons-doc"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev $docdepends"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcmutils-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="c5ca1e1b61da4c7c96bb8e95593e4799c86345a6d4ae222284c31e31ac3c60ace81bd1f8e62c5e1956b25f2f6f32d9586912505380dce888a7e76ed86f45fdca  kcmutils-5.94.0.tar.xz"
