From da3ad6ffc9ecf75ebb734bbb863c1412d11b9beb Mon Sep 17 00:00:00 2001
From: "A. Wilcox" <AWilcox@Wilcox-Tech.com>
Date: Sat, 10 Aug 2024 21:26:57 -0500
Subject: [PATCH] xfs_scrub: Use POSIX-conformant strerror_r

When building xfsprogs with musl libc, strerror_r returns int as
specified in POSIX.  This differs from the glibc extension that returns
char*.  Successful calls will return 0, which will be dereferenced as a
NULL pointer by (v)fprintf.

Signed-off-by: A. Wilcox <AWilcox@Wilcox-Tech.com>
---
 scrub/common.c | 3 ++-
 scrub/inodes.c | 4 ++--
 2 files changed, 4 insertions(+), 3 deletions(-)

diff --git a/scrub/common.c b/scrub/common.c
index 283ac84e..bd8bde35 100644
--- a/scrub/common.c
+++ b/scrub/common.c
@@ -122,7 +122,8 @@ __str_out(
 	fprintf(stream, "%s%s: %s: ", stream_start(stream),
 			_(err_levels[level].string), descr);
 	if (error) {
-		fprintf(stream, _("%s."), strerror_r(error, buf, DESCR_BUFSZ));
+		strerror_r(error, buf, DESCR_BUFSZ);
+		fprintf(stream, _("%s."), buf);
 	} else {
 		va_start(args, format);
 		vfprintf(stream, format, args);
diff --git a/scrub/inodes.c b/scrub/inodes.c
index 16c79cf4..f0e7289c 100644
--- a/scrub/inodes.c
+++ b/scrub/inodes.c
@@ -65,9 +65,9 @@ bulkstat_for_inumbers(
 	error = -xfrog_bulkstat(&ctx->mnt, breq);
 	if (error) {
 		char	errbuf[DESCR_BUFSZ];
+		strerror_r(error, errbuf, DESCR_BUFSZ);
 
-		str_info(ctx, descr_render(dsc), "%s",
-			 strerror_r(error, errbuf, DESCR_BUFSZ));
+		str_info(ctx, descr_render(dsc), "%s", errbuf);
 	}
 
 	/*
-- 
2.40.0

