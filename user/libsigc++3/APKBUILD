# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=libsigc++3
_pkgreal=${pkgname%3}
pkgver=3.2.0
pkgrel=0
pkgdesc="Library for type-safe callbacks in C++"
url="https://libsigcplusplus.github.io/libsigcplusplus/download.html"
arch="all"
license="LGPL-2.1+ AND LGPL-3.0+"
depends=""
makedepends="boost-dev docbook-xsl doxygen graphviz meson ninja cmd:xsltproc"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.gnome.org/sources/$_pkgreal/${pkgver%.*}/$_pkgreal-$pkgver.tar.xz"
builddir="$srcdir/$_pkgreal-$pkgver"

build() {
	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--buildtype=release \
		--wrap-mode=nofallback \
		-Dbuild-examples=false \
		-Dbuild-documentation=true \
		. output
	ninja -C output
}

check() {
	ninja -C output test
}

package() {
	DESTDIR="$pkgdir" ninja -C output install
}

sha512sums="91315cecc79a1ad6ea165b66a13a5afd4e5bc101842f9d4c58811ea78536c07fc8821c51aa5110a032ed71c09f85790b3a02f2ad7fe8cc3aed6e03b2bafcd70c  libsigc++-3.2.0.tar.xz"
