From 72d82ee1f738355d5f76374d487d1623da57384e Mon Sep 17 00:00:00 2001
From: "Ruslan N. Marchenko" <me@ruff.mobi>
Date: Fri, 17 Dec 2021 08:58:37 +0100
Subject: [PATCH] Cleanup some code around tls-unique binding in TLSv1.3

 * The binding call should fail first of all, as tls-unique binding type
   is not defined under TLSv1.3. The test unit is updated accordingly.
 * New GnuTLS backend is handling it properly but old returns success/empty
   data - handle empty data and return error.
 * OpenSSL returns success (or rather Finished packet, as it is asked for)
   hence catch this condition before the call and return error.
---
 tls/gnutls/gtlsconnection-gnutls.c   | 10 ++++++++++
 tls/openssl/gtlsconnection-openssl.c |  9 +++++++++
 tls/tests/connection.c               |  8 +++++++-
 3 files changed, 26 insertions(+), 1 deletion(-)

diff --git a/tls/gnutls/gtlsconnection-gnutls.c b/tls/gnutls/gtlsconnection-gnutls.c
index 1b607408..387f14d3 100644
--- a/tls/gnutls/gtlsconnection-gnutls.c
+++ b/tls/gnutls/gtlsconnection-gnutls.c
@@ -1172,6 +1172,16 @@ gnutls_get_binding (GTlsConnectionGnutls      *gnutls,
 
   if (ret == GNUTLS_E_SUCCESS)
     {
+      /* Older GnuTLS versions are known to return SUCCESS and empty data for TLSv1.3 tls-unique binding.
+       * While it may look prudent to catch here that specific corner case, the empty binding data is
+       * definitely not a SUCCESS, regardless of the version and type. */
+      if (cb.size == 0)
+        {
+          g_set_error (error, G_TLS_CHANNEL_BINDING_ERROR, G_TLS_CHANNEL_BINDING_ERROR_GENERAL_ERROR,
+                       _("Empty channel binding data indicates a bug in the TLS library implementation"));
+          return FALSE;
+        }
+
       if (data != NULL)
         {
           g_tls_log_debug (gnutls, "binding size %d", cb.size);
diff --git a/tls/openssl/gtlsconnection-openssl.c b/tls/openssl/gtlsconnection-openssl.c
index 9cf6ad74..265845ee 100644
--- a/tls/openssl/gtlsconnection-openssl.c
+++ b/tls/openssl/gtlsconnection-openssl.c
@@ -653,6 +653,15 @@ openssl_get_binding_tls_unique (GTlsConnectionOpenssl  *tls,
   gboolean resumed = SSL_session_reused (ssl);
   size_t len = 64;
 
+#if OPENSSL_VERSION_NUMBER >= 0x10101000L
+  if (SSL_version (ssl) >= TLS1_3_VERSION)
+    {
+      g_set_error (error, G_TLS_CHANNEL_BINDING_ERROR, G_TLS_CHANNEL_BINDING_ERROR_GENERAL_ERROR,
+                   _("The request is invalid."));
+      return FALSE;
+    }
+#endif
+
   /* This is a drill */
   if (!data)
     return TRUE;
diff --git a/tls/tests/connection.c b/tls/tests/connection.c
index f9940986..f6f1cf87 100644
--- a/tls/tests/connection.c
+++ b/tls/tests/connection.c
@@ -2605,6 +2605,8 @@ test_connection_binding_match_tls_unique (TestConnection *test,
   /* Real test: retrieve bindings and compare */
   if (client_supports_tls_unique)
     {
+      g_assert_false (g_tls_connection_get_protocol_version (
+            G_TLS_CONNECTION (test->client_connection)) == G_TLS_PROTOCOL_VERSION_TLS_1_3);
       client_cb = g_byte_array_new ();
       server_cb = g_byte_array_new ();
       g_assert_true (g_tls_connection_get_channel_binding_data (G_TLS_CONNECTION (test->client_connection),
@@ -2624,7 +2626,11 @@ test_connection_binding_match_tls_unique (TestConnection *test,
       g_byte_array_unref (server_cb);
     }
   else
-    g_test_skip ("tls-unique is not supported");
+    {
+      g_assert_true (g_tls_connection_get_protocol_version (
+            G_TLS_CONNECTION (test->client_connection)) == G_TLS_PROTOCOL_VERSION_TLS_1_3);
+      g_test_skip ("tls-unique is not supported");
+    }
 
   /* drop the mic */
   close_server_connection (test);
-- 
GitLab

