From c905caaecf33c1820eebae34e7b5ef1e71642af0 Mon Sep 17 00:00:00 2001
From: Allan Sandfeld Jensen <allan.jensen@qt.io>
Date: Mon, 25 Jan 2021 15:34:22 +0100
Subject: [PATCH 1/3] Clear frame on reconnect
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Otherwise partial data could leak through to next connection and cause
odd behavior

* asturmlechner 2021-12-22: Fix conflict with dev branch aeeaa00f

Pick-to: 5.15
Fixes: QTBUG-88923
Change-Id: I6c75e6325527379bcdca0b9819a89437d0658893
Reviewed-by: Alex Blasche <alexander.blasche@qt.io>
Reviewed-by: Mårten Nordheim <marten.nordheim@qt.io>
(cherry picked from commit 9f6449ef1b6d1e7651f181585f1c35d6722bb87a)
---
 src/websockets/qwebsocketdataprocessor.cpp | 1 +
 1 file changed, 1 insertion(+)

diff --git a/src/websockets/qwebsocketdataprocessor.cpp b/src/websockets/qwebsocketdataprocessor.cpp
index e2cc291..69c4fa8 100644
--- a/src/websockets/qwebsocketdataprocessor.cpp
+++ b/src/websockets/qwebsocketdataprocessor.cpp
@@ -269,6 +269,7 @@ void QWebSocketDataProcessor::clear()
     if (!m_pConverterState)
         m_pConverterState = new QTextCodec::ConverterState(QTextCodec::ConvertInvalidToNull |
                                                            QTextCodec::IgnoreHeader);
+    frame.clear();
 }
 
 /*!
-- 
2.36.0

From 6b318a2f39942b248574438925e23b46235009b2 Mon Sep 17 00:00:00 2001
From: Jens Trillmann <jens.trillmann@governikus.de>
Date: Fri, 26 Feb 2021 13:06:49 +0100
Subject: [PATCH 2/3] Pass ignoreSslErrors to unterlying QSslSocket

When setting ignoreSslErrors(List<QSslError>) during the sslErrors
signal handling the call got ignored. Only the internal config for
creating a QSslSocket would be updated but not the current QSslSocket.
The request of the caller to ignore specific QSslErrors would be
ignored.

Pick-to: 5.12 5.15 6.2
Change-Id: I6aaea2111fe1d26e07e6eaaa7532ae1b14a187a8
Reviewed-by: Allan Sandfeld Jensen <allan.jensen@qt.io>
(cherry picked from commit b13f23d232cfdd372ef812d427872d52eed2337e)
---
 src/websockets/qwebsocket_p.cpp | 5 +++++
 1 file changed, 5 insertions(+)

diff --git a/src/websockets/qwebsocket_p.cpp b/src/websockets/qwebsocket_p.cpp
index aedc3c6..824184a 100644
--- a/src/websockets/qwebsocket_p.cpp
+++ b/src/websockets/qwebsocket_p.cpp
@@ -270,6 +270,11 @@ QSslConfiguration QWebSocketPrivate::sslConfiguration() const
 void QWebSocketPrivate::ignoreSslErrors(const QList<QSslError> &errors)
 {
     m_configuration.m_ignoredSslErrors = errors;
+    if (Q_LIKELY(m_pSocket)) {
+        QSslSocket *pSslSocket = qobject_cast<QSslSocket *>(m_pSocket);
+        if (Q_LIKELY(pSslSocket))
+            pSslSocket->ignoreSslErrors(errors);
+    }
 }
 
 /*!
-- 
2.36.0

From 6a4991250793cd38efa5e8db858cd49c82439130 Mon Sep 17 00:00:00 2001
From: Marc Mutz <marc.mutz@qt.io>
Date: Fri, 17 Dec 2021 09:08:45 +0100
Subject: [PATCH 3/3] QWebSocketProtocol: fix potential UB (signed overflow) in
 masking operation
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

The size of the payload is a 64-bit integer, which the loop counts
down. If the size is > INT_MAX, then we'll overflow the int i used to
track the current position in the mask.

Fix by using an unsigned integer type instead.

Pick-to: 6.3 6.2 5.15
Change-Id: Ia3b8d42ae906eb03c1c7399cb1137a08121fcde3
Reviewed-by: Mårten Nordheim <marten.nordheim@qt.io>
(cherry picked from commit 38218494a65049b5f9da7a8aab012a969c7dac86)
---
 src/websockets/qwebsocketprotocol.cpp | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/src/websockets/qwebsocketprotocol.cpp b/src/websockets/qwebsocketprotocol.cpp
index df87a93..d0465f1 100644
--- a/src/websockets/qwebsocketprotocol.cpp
+++ b/src/websockets/qwebsocketprotocol.cpp
@@ -210,7 +210,7 @@ void QWebSocketProtocol::mask(char *payload, quint64 size, quint32 maskingKey)
                             quint8((maskingKey & 0x0000FF00u) >> 8),
                             quint8((maskingKey & 0x000000FFu))
                           };
-    int i = 0;
+    quint64 i = 0;
     while (size-- > 0)
         *payload++ ^= mask[i++ % 4];
 }
-- 
2.36.0

