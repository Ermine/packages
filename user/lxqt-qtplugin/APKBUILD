# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=lxqt-qtplugin
pkgver=1.4.0
_lxqt=0.13.0
pkgrel=0
pkgdesc="Qt plugin for platform integration with LXQt"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=$_lxqt qt5-qttools-dev
	libfm-qt-dev>=${pkgver%.*}.0 liblxqt-dev>=${pkgver%.*}.0
	libdbusmenu-qt-dev"
source="https://github.com/lxqt/lxqt-qtplugin/releases/download/$pkgver/lxqt-qtplugin-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="1d6306d396e560fbab1c64e2dda47e0456e6d8fcff077bcb7104adb8f89ef73bfef9457ab5b65aa07101acc3d7d2701efb88e8d36a0c74780a187480288acbea  lxqt-qtplugin-1.4.0.tar.xz"
