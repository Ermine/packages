# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=libksysguard
pkgver=5.24.5
pkgrel=0
pkgdesc="KDE system monitor library"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test requires accelerated X11 session
license="LGPL-2.1+ AND (GPL-2.0-only OR GPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev kconfig-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 libx11-dev zlib-dev
	qt5-qtscript-dev ki18n-dev kauth-dev kcompletion-dev kconfigwidgets-dev
	kcoreaddons-dev kiconthemes-dev plasma-framework-dev kservice-dev
	kwindowsystem-dev kwidgetsaddons-dev kio-dev libnl3-dev libpcap-dev
	kdeclarative-dev kglobalaccel-dev knewstuff-dev lm_sensors-dev
	libcap-dev qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/libksysguard-$pkgver.tar.xz
	underlinking.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="133c5923ee616515b1934c4e2c30509d407f40b7afc3914ac2417d7c961f3a1921bd3b5bfd1ca73942ddb17777fbf4d96950c130129135eae2b8d1b8daac9fa5  libksysguard-5.24.5.tar.xz
3f273d73ef721b08dee2d0b74f8bb14bb0ee6085eb43b68704abcbca14d60b29cfe651f4c2ad00d13270a783ef94e66982e2e4ae0c6109cf2c895c3562af89f5  underlinking.patch"
