# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=umbrello
pkgver=22.04.2
pkgrel=0
pkgdesc="Software modelling tool and code generator"
url="https://umbrello.kde.org/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	qt5-qtwebkit-dev karchive-dev kcompletion-dev kcoreaddons-dev ki18n-dev
	kcrash-dev kdoctools-dev kiconthemes-dev kdelibs4support-dev kio-dev
	ktexteditor-dev kwindowsystem-dev kwidgetsaddons-dev kxmlgui-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/umbrello-$pkgver.tar.xz
	hax.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="67b3d573cccd0bdf4ec7accea53c5059e6b676525952ea4f66aa1eff0c8cfe3593587dc1e3302fc6a0e3ed9e5a08197bc4abf9d1138d7affbda9579913e4ac3b  umbrello-22.04.2.tar.xz
fcbe03a49d5420bf17383e5e6d4bac4377c1abfcafacea20ac37409457471537067efe3236647fb3570abfa410a4a3870b1638bb1ef880a24e6f60e2b189a562  hax.patch"
