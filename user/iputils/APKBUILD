# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=iputils
pkgver=20211215
pkgrel=0
pkgdesc="IP Configuration Utilities (and Ping)"
url="https://github.com/iputils/iputils/"
arch="all"
options="!check suid"  # tests require binaries with suid/capabilities
license="BSD-3-Clause AND GPL-2.0-or-later"
depends=""
makedepends="docbook-xsl docbook-xsl-ns libcap-dev libgcrypt-dev libxslt meson
	openssl-dev iproute2"
subpackages="$pkgname-doc $pkgname-lang $pkgname-openrc"
source="$pkgname-$pkgver.tgz::https://github.com/$pkgname/$pkgname/archive/$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgver"

build() {
        # we're waiting for idn support in musl so that we can enable it here
        # https://wiki.musl-libc.org/functional-differences-from-glibc.html#Name-Resolver/DNS
	meson   --prefix /usr \
		--buildtype=plain \
		-DBUILD_ARPING=true \
		-DUSE_IDN=false \
		-DNO_SETCAP_OR_SUID=false \
		builddir
	ninja -v -C builddir
}

check() {
	ninja -C builddir test
}

package() {
	DESTDIR=$pkgdir ninja -C builddir install

	ln -s tracepath "$pkgdir"/usr/bin/tracepath6
	ln -s ping "$pkgdir"/usr/bin/ping6
}

sha512sums="191062e51f7c8e8b38e3e4a96845adb77cd69f487d548c7cc578fad544443b4bc0dbe965d8f8e6fbda0a2f5b2fe2829789c05f873190c53d773245959298f6e9  iputils-20211215.tgz"
