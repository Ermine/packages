# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=plasma-pa
pkgver=5.24.5
pkgrel=0
pkgdesc="PulseAudio integration for KDE Plasma desktop"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="kirigami2 libcanberra-pulse sound-theme-freedesktop"
makedepends="cmake extra-cmake-modules glib-dev kcoreaddons-dev kdeclarative-dev
	kdoctools-dev kglobalaccel-dev ki18n-dev knotifications-dev
	libcanberra-dev plasma-framework-dev pulseaudio-dev qt5-qtbase-dev
	qt5-qtdeclarative-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-pa-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="fdf18b0baa1fb43a8d576d454ba6625bf7d376eb30fcbe8e09df388441a04d1b373b6d53a56b4f14552fc06cbdddea193248044d0cc5e3b71fb90651708afaa1  plasma-pa-5.24.5.tar.xz"
