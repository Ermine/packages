# Contributor: Ariadne Conill <ariadne@dereferenced.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=talloc
pkgver=2.3.4
pkgrel=0
pkgdesc="Memory pool management library"
url="https://talloc.samba.org"
arch="all"
license="LGPL-3.0+ AND GPL-3.0+ AND ISC AND LGPL-2.1+ AND BSD-3-Clause AND PostgreSQL"
depends=""
makedepends="cmd:which docbook-xsl libxslt python3-dev"
replaces="samba-common"
subpackages="$pkgname-dev py3-$pkgname:py3 $pkgname-doc"
source="https://samba.org/ftp/$pkgname/$pkgname-$pkgver.tar.gz"

build() {
	PYTHON=python3 ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--bundled-libraries=NONE \
		--builtin-libraries=replace \
		--disable-rpath \
		--disable-rpath-install
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

py3() {
	pkgdesc="Python 3 binding for libtalloc"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libpytalloc-util.cpython* \
		"$pkgdir"/usr/lib/python3* "$subpkgdir"/usr/lib/
}

sha512sums="c46488deda99753fd79566d42cae88899b71196513a127813be2cb855e7f36b77132f0552297ee4153ba4d8f177cea3bb0dc93340caabf321c026657744684d9  talloc-2.3.4.tar.gz"
