# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=transmission
pkgver=3.00
pkgrel=0
pkgdesc="Lightweight GTK BitTorrent client"
url="https://transmissionbt.com/"
arch="all"
license="GPL-2.0+ AND MIT"
depends="" 
makedepends="bsd-compat-headers cmake curl-dev dbus-glib-dev gtk+3.0-dev intltool
	libevent-dev libnotify-dev openssl-dev qt5-qtbase-dev glib-dev
	miniupnpc-dev libnatpmp-dev wayland-protocols qdbus qt5-qttools-dev"
install="transmission.post-install transmission-daemon.pre-install transmission-daemon.post-upgrade"
pkgusers="transmission"
pkggroups="transmission"
source="https://github.com/transmission/$pkgname-releases/raw/master/$pkgname-$pkgver.tar.xz
	transmission-daemon.initd
	transmission-daemon.confd
	fix-release-tarball.patch
	"
subpackages="$pkgname-qt $pkgname-gtk $pkgname-cli $pkgname-daemon $pkgname-doc $pkgname-lang"

build() {
	mkdir build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-DWITH_LIBAPPINDICATOR=NO \
		-DENABLE_QT=ON \
		-DENABLE_GTK=ON \
		-DENABLE_CLI=ON \
		-DINSTALL_LIB=ON \
		..
	make
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd build
	make DESTDIR="$pkgdir" install
}

qt() {
	pkgdesc="Lightweight BitTorrent client (Qt GUI interface)"
	depends="$pkgname=$pkgver-r$pkgrel"

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/transmission-qt "$subpkgdir"/usr/bin
}

gtk() {
	pkgdesc="Lightweight BitTorrent client (Qt GUI interface)"
	depends="$pkgname=$pkgver-r$pkgrel"

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/transmission-gtk "$subpkgdir"/usr/bin
}

daemon() {
	pkgdesc="Lightweight BitTorrent client (daemon and Web interface)"

	install -d "$subpkgdir"/usr/share \
		"$subpkgdir"/usr/bin
	install -d -o transmission -g transmission \
		"$subpkgdir"/var/lib/transmission \
		"$subpkgdir"/var/log/transmission
	mv "$pkgdir"/usr/bin/transmission-daemon \
	   "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/share/transmission \
	   "$subpkgdir"/usr/share/
	install -D -m755 "$srcdir"/transmission-daemon.initd \
		"$subpkgdir"/etc/init.d/transmission-daemon
	install -D -m644 "$srcdir"/transmission-daemon.confd \
		"$subpkgdir"/etc/conf.d/transmission-daemon
}

cli() {
	pkgdesc="Lightweight BitTorrent client (CLI and remote)"

	install -d "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/transmission-cli \
	   "$pkgdir"/usr/bin/transmission-create \
	   "$pkgdir"/usr/bin/transmission-edit \
	   "$pkgdir"/usr/bin/transmission-show \
	   "$pkgdir"/usr/bin/transmission-remote \
	   "$subpkgdir"/usr/bin/
}

sha512sums="eeaf7fe46797326190008776a7fa641b6341c806b0f1684c2e7326c1284832a320440013e42a37acda9fd0ee5dca695f215d6263c8acb39188c5d9a836104a61  transmission-3.00.tar.xz
d31275fba7eb322510f9667e66a186d626889a6e3143be2923aae87b9c35c5cf0c508639f1cb8c1b88b1e465bc082d80bb1101385ebde736a34d4eeeae0f6e15  transmission-daemon.initd
a3b9ac2b7bbe30e33060c8b6a693dc7072d3c6ac44f92ddd567969d8f57a0bfc1a561e781ae167703ccb4b2fd5b0e6d8f8a66c5ba14fe01d8d89a501d4501474  transmission-daemon.confd
4d6a9fa4dc49ff785847c20097e1968373b4636da7c70e17912aadff31ff182a8fb31a81e278531296040cf2de6077c6a446a54146c9a92571f6a9d9b4aed661  fix-release-tarball.patch"
