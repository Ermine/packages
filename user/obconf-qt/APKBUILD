# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=obconf-qt
pkgver=0.16.4
_lxqt=0.9.0
pkgrel=0
pkgdesc="Qt5-based Openbox configuration tool"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=$_lxqt openbox-dev
	liblxqt-dev qt5-qttools-dev"
source="https://github.com/lxqt/obconf-qt/releases/download/$pkgver/obconf-qt-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="4f75a5275e14cd7e8f0abc1bfcd40b4d860d613bc37c06c0e68d6f622ed126dce2fea98b5dd8e629b5de472ab5e668a5b0af7f6621d09fcb0b2287a11b0fb662  obconf-qt-0.16.4.tar.xz"
