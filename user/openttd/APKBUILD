# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=openttd
pkgver=14.1
pkgrel=0
pkgdesc="Simulation game based on Transport Tycoon Deluxe"
url="https://www.openttd.org/en/"
arch="all"
#options="!check"  # Test files not shipped in release package.
license="GPL-2.0-only"
depends="opengfx openmsx opensfx"
makedepends="cmake curl-dev dbus-dev fluidsynth-dev fontconfig-dev freetype-dev
	grfcodec harfbuzz-dev icu-dev libpng-dev lzo-dev mesa-dev sdl2-dev
	xz-dev zlib-dev"
subpackages="$pkgname-doc"
source="https://cdn.openttd.org/openttd-releases/$pkgver/openttd-$pkgver-source.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -B build
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="194135cc01457d9ed069c09d8961ad3de681bc9936533ca81ff23099f394bb9a83ef84a0852e85525e258f6149e14fd41cfc5f8e683c0643515005c42fb77e3b  openttd-14.1-source.tar.xz"
