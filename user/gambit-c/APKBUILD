# Contributor: Zach van Rijn <me@zv.io>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=gambit-c
pkgver=4.9.5
_pkgver=$(printf "%s" "${pkgver}" | tr . _)
pkgrel=0
pkgdesc="An efficient implementation of the Scheme programming language."
url="https://gambitscheme.org/"
arch="all"
license="Apache-2.0"
depends=""
makedepends="openssl-dev texinfo gnu-ghostscript"
subpackages="$pkgname-dev $pkgname-doc"
source="https://gambitscheme.org/$pkgver/gambit-v${_pkgver}.tgz
	fix-incompatible-stat-usage.patch
	reproducibility.patch
	"
builddir="$srcdir/gambit-v$_pkgver"

prepare() {
	default_prepare

	#934
	for srfi in 179 231; do
		sed -i lib/srfi/makefile -e "s/\b${srfi}\b//g";
	done
}

build() {
	# https://github.com/gambit/gambit/issues/249
	export CFLAGS="${CFLAGS} --param max-gcse-memory=600000000";

	# We need to bootstrap if we patch .scm files that are
	# used to generate .c files; using unpatched official
	# releases should be OK if the patches are upstream.
	#
	# Disable versioned shlibs b/c buggy build scripts.
	#
	# There is no 'makefile' without an initial 'configure'.
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--enable-single-host \
		--enable-c-opt \
		--enable-dynamic-clib \
		--enable-debug-c-backtrace \
		--enable-gcc-opts \
		;
	make bootstrap
	make bootclean

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-compiler-name=gsc${pkgver%%.*} \
		--enable-interpreter-name=gsi${pkgver%%.*} \
		--enable-single-host \
		--enable-c-opt \
		--enable-dynamic-clib \
		--enable-debug-c-backtrace \
		--enable-gcc-opts \
		--enable-shared \
		--enable-versioned-shared-libs \
		--disable-absolute-shared-libs \
		--enable-openssl \
		;
	make
	make modules
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

sha512sums="5b5a6fc783851eac140590ef81e017de8301490095d56afa9e670d1a00ed0a31c621b6d48215af8ace9682ca21cd317200167cb95bfdc01a7312394c944597ff  gambit-v4_9_5.tgz
a878994ea7c40b301d30b4d05879c335ad42e1ca8c25387556691192b6945276d6d6ff017ff568302558b23c42c61f223bddb3596b8203fbee8edbf4fdd9b813  fix-incompatible-stat-usage.patch
133ef5442c1bfe51e57caecd509c12cdecd86cfb941114a5038fad2c3e2bda1770fde7e367561039d523db5ea9cba5cebae7a9ed9daca699863267e7224f1ad6  reproducibility.patch"
