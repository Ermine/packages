# Contributor: Christian Kampka <christian@kampka.net>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=po4a
pkgver=0.69
pkgrel=0
pkgdesc="Tools for helping translation of documentation"
url="https://po4a.org"
arch="noarch"
# most of t/cfg-* requires GNU extensions to gettext tools
# t/fmt-sgml requires onsgmls(1), part of opensp
# t/fmt-tex requires kpsewhich(1), part of TeX
options="!check"
license="GPL-2.0-only"
depends="perl gettext-tiny perl-pod-parser perl-sgmls perl-unicode-linebreak
	perl-yaml-tiny"
makedepends="docbook-xsl perl-module-build diffutils"
# -lang would require Locale::gettext
subpackages="$pkgname-doc"
source="https://github.com/mquinson/po4a/releases/download/v$pkgver/po4a-$pkgver.tar.gz
	disable-stats.patch
	"

build() {
	perl Build.PL installdirs=vendor create_packlist=0
	perl Build
}

check() {
	perl Build test
}

package() {
	perl Build destdir=${pkgdir} install
	# remove perllocal.pod and .packlist
	find ${pkgdir} -name .packlist -o -name perllocal.pod -delete
}

sha512sums="9cb5eec547ab18d1c3ebdda212b909fc4f5489a74641ba2d7e0a3a1d060f245d23667c16e687c678c5ccc3809c9315d20673266dcc3764172a899caa397238e3  po4a-0.69.tar.gz
be457a023383c60864bd155b13d8952f8ae523b709a464af2419695a3fb64c1ee6b4176b23811241fa9bed87c2d0c44dbf8c19178046b052b49ea191d03ccc5a  disable-stats.patch"
