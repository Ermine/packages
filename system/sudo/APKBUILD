# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Horst Burkhardt <horst@adelielinux.org>
pkgname=sudo
pkgver=1.9.10
if [ "${pkgver%_*}" != "$pkgver" ]; then
	_realver=${pkgver%_*}${pkgver#*_}
else
	_realver=$pkgver
fi
pkgrel=0
pkgdesc="Give certain users the ability to run some commands as root"
url="https://www.sudo.ws/sudo/"
arch="all"
options="suid"
license="ISC AND MIT AND BSD-3-Clause AND BSD-2-Clause AND Zlib"
depends=""
makedepends_host="linux-pam-dev zlib-dev utmps-dev"
makedepends_build="bash"
makedepends="$makedepends_host $makedepends_build"
subpackages="$pkgname-doc $pkgname-dev $pkgname-lang"
source="https://www.sudo.ws/dist/sudo-${_realver}.tar.gz"
builddir="$srcdir"/$pkgname-$_realver
somask="audit_json.so
	group_file.so
	libsudo_noexec.so
	libsudo_util.so.0
	sample_approval.so
	sudoers.so
	system_group.so
	"

# secfixes:
#   1.9.9-r0:
#     - CVE-2021-3156
#     - CVE-2021-23239
#     - CVE-2021-23240
#   1.8.20_p2-r0:
#     - CVE-2017-1000368
#   1.8.28-r0:
#     - CVE-2019-14287
#   1.9.0-r0:
#     - CVE-2019-19232
#   1.9.2-r2:
#     - CVE-2021-3156

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libexecdir=/usr/lib \
		--mandir=/usr/share/man \
		--enable-pie \
		--with-env-editor \
		--with-pam \
		--without-skey \
		--with-passprompt="[sudo] Password for %p: " \
		--with-insults=disabled \
		--with-all-insults \
		--enable-python=no \
		--disable-log-server \
		--disable-log-client \
		--with-secure-path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
	make
}

check() {
	make check
}

package() {
	# the sudo's mkinstalldir script miscreates the leading
	# path components with bad permissions. fix this.
	install -d -m0755 "$pkgdir"/var "$pkgdir"/var/db
	make -j1 DESTDIR="$pkgdir" install
	rm -rf "$pkgdir"/var/run
}

sha512sums="65cf92b67b64413cb807da8b9602fc90b75e5b30dd1402d682ca36f276a3d6209a8a59c14e463898abc9856bc56263e5ba4bb6d44774f56a2885a9eea4a35375  sudo-1.9.10.tar.gz"
