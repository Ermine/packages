From: Uros Bizjak <ubizjak@gmail.com>
Date: Wed, 10 Jul 2024 07:27:27 +0000 (+0200)
Subject: middle-end: Fix stalled swapped condition code value [PR115836]
X-Git-Url: https://gcc.gnu.org/git/?p=gcc.git;a=commitdiff_plain;h=47a8b464d2dd9a586a9e15242c9825e39e1ecd4c;hp=efa30f619361b043616e624e57366a50982e11df

middle-end: Fix stalled swapped condition code value [PR115836]

emit_store_flag_1 calculates scode (swapped condition code) at the
beginning of the function from the value of code variable.  However,
code variable may change before scode usage site, resulting in
invalid stalled scode value.

Move calculation of scode value just before its only usage site to
avoid stalled scode value.

	PR middle-end/115836

gcc/ChangeLog:

	* expmed.cc (emit_store_flag_1): Move calculation of
	scode just before its only usage site.

(cherry picked from commit 44933fdeb338e00c972e42224b9a83d3f8f6a757)
---

diff --git a/gcc/expmed.cc b/gcc/expmed.cc
index 4ec035e4843b..19765311b954 100644
--- a/gcc/expmed.cc
+++ b/gcc/expmed.cc
@@ -5607,11 +5607,9 @@ emit_store_flag_1 (rtx target, enum rtx_code code, rtx op0, rtx op1,
   enum insn_code icode;
   machine_mode compare_mode;
   enum mode_class mclass;
-  enum rtx_code scode;
 
   if (unsignedp)
     code = unsigned_condition (code);
-  scode = swap_condition (code);
 
   /* If one operand is constant, make it the second one.  Only do this
      if the other operand is not constant as well.  */
@@ -5726,6 +5724,8 @@ emit_store_flag_1 (rtx target, enum rtx_code code, rtx op0, rtx op1,
 
 	  if (GET_MODE_CLASS (mode) == MODE_FLOAT)
 	    {
+	      enum rtx_code scode = swap_condition (code);
+
 	      tem = emit_cstore (target, icode, scode, mode, compare_mode,
 				 unsignedp, op1, op0, normalizep, target_mode);
 	      if (tem)
