# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kbd
pkgver=2.2.0_git20190823
pkgrel=2
pkgdesc="Console keyboard and font management utilities"
url="https://kbd-project.org/"
arch="all"
options="!check"  # padding error on at least ppc64
license="GPL-2.0+"
depends=""
checkdepends="check-dev"
makedepends="linux-headers linux-pam-dev autoconf automake libtool"
provides="$pkgname-keymaps"
replaces="$pkgname-keymaps"
subpackages="$pkgname-doc $pkgname-fonts::noarch
	$pkgname-lang $pkgname-openrc"
source="https://dev.sick.bike/dist/$pkgname-$pkgver.tar.xz
	consolefont.confd
	consolefont.initd
	keymaps.confd
	keymaps.initd
	"

prepare() {
	default_prepare
	autoreconf -vif
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	for i in consolefont keymaps; do
		install -Dm644 "$srcdir"/$i.confd \
			"$pkgdir"/etc/conf.d/$i
		install -Dm755 "$srcdir"/$i.initd \
			"$pkgdir"/etc/init.d/$i
	done
}

openrc() {
	default_openrc
	license="BSD-2-Clause"
	replaces="openrc<0.24.1-r10"
}

fonts() {
	pkgdesc="Fonts shipped with Linux kbd tools"
	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/consolefonts "$subpkgdir"/usr/share/
}
sha512sums="7f6202aeb17d6224095447a92d999478220457e1e7cadb90a7c40ca7f3b0c5b1f672db1995fb69652ca37558a75df582bfb5fea5772f3b1e040fe39f8f54504e  kbd-2.2.0_git20190823.tar.xz
837054b29050b955b4c3f1ddd6481c79792a66e2415bafbcfbf3e12a27066ddd3ce35e069448dfb1b2a00e843cec180dd6940edbd9014cd2a0c2aaa1b76c9fb0  consolefont.confd
1e67f75f27ec5de8f71489c83f78e53e8fd0925ae2d82dabf516f98905c5aff206a8e943e92b6af413eae5f0ecb9c2240526dfc3c56811a8dc1d19ced561bba3  consolefont.initd
9391671bbe8b3b7b4eba63ff4e4b7f17297712393abca69f0e24e3a5304a545faa213de8bc799de922b69ee52048d6e6cf352d7213b459991286ef6dea77e8c3  keymaps.confd
510d7eb582a914fa7cfb868d0c3d55a21ee1519033117d0943a153528b5810d09f6421ddc02d93906c1a66fb7e07e86240f38dad0d38cfb5a8c7e9a6335a937f  keymaps.initd"
