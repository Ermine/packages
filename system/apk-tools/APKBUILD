# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Adelie Platform Group <adelie-devel@lists.adelielinux.org>
pkgname=apk-tools
pkgver=2.14.0
pkgrel=0
pkgdesc="Alpine Package Keeper - package manager"
url="https://git.adelielinux.org/adelie/apk-tools"
arch="all"
license="GPL-2.0-only"
depends="ca-certificates"
makedepends_build="scdoc"
makedepends_host="zlib-dev openssl openssl-dev linux-headers"
makedepends="$makedepends_build $makedepends_host"
subpackages="$pkgname-dev $pkgname-static $pkgname-doc"
source="https://git.adelielinux.org/adelie/apk-tools/-/archive/v${pkgver}+adelie1/${pkgname}-v${pkgver}+adelie1.tar.bz2
	https://git.adelielinux.org/adelie/apk-tools/uploads/58cb5e01b6ac8218ba738df0e0bedc7c/apk-tools-docs-v2.14.0.tar.xz
	apk.zsh-completion
	s6-linux-init.patch
	"
builddir="$srcdir/$pkgname-v$pkgver+adelie1"

prepare() {
	default_prepare
	sed -i -e 's:-Werror::' Make.rules
	cat >config.mk <<-EOF
		FULL_VERSION=$pkgver-r$pkgrel
		LUA=no
		export LUA
	EOF
	rm "$builddir"/doc/apk-add.8
}

build() {
	make
	make static
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -d "$pkgdir"/var/lib/apk \
		"$pkgdir"/var/cache/misc \
		"$pkgdir"/etc/apk/keys \
		"$pkgdir"/etc/apk/protected_paths.d
	# the shipped README is not useful
	rm -r "$pkgdir"/usr/share/doc

	install -Dm644 "$srcdir"/apk.zsh-completion \
		"$pkgdir"/usr/share/zsh/site-functions/_apk
}

static() {
	pkgdesc="Alpine Package Keeper - static binary"
	install -Dm755 "$builddir"/src/apk.static \
		"$subpkgdir"/sbin/apk.static

	# Sign the static binary so it can be vefified from distros
	# that do not have apk-tools
	local abuild_conf="${ABUILD_CONF:-"/etc/abuild.conf"}"
	local abuild_home="${ABUILD_USERDIR:-"$HOME/.abuild"}"
	local abuild_userconf="${ABUILD_USERCONF:-"$abuild_home/abuild.conf"}"
	[ -f "$abuild_userconf" ] && . "$abuild_userconf"
	local privkey="$PACKAGER_PRIVKEY"
	local pubkey="${PACKAGER_PUBKEY:-"${privkey}.pub"}"
	local keyname="${pubkey##*/}"
	${CROSS_COMPILE}strip "$subpkgdir"/sbin/apk.static
	openssl dgst -sha1 -sign "$privkey" \
		-out "$subpkgdir/sbin/apk.static.SIGN.RSA.$keyname" \
		"$subpkgdir"/sbin/apk.static
}

sha512sums="e71fd14655cd5907ea7046d64a877cf5128d062a7e7ca82dd5d1c254bf3ca59443c5643571734c9f558301fe0ffd16956bb89b680c5101b653eaaf64259d2959  apk-tools-v2.14.0+adelie1.tar.bz2
c59bdd7fe77bd2cf565a1c050100bebd90da80b9d46b706237c6e5d2a3f5bf3f5a23a69940074b842c8e9f49e788d3c27492b952e52ef881bd1c22e1046a0743  apk-tools-docs-v2.14.0.tar.xz
cedda9bf11e0a516c9bf0fd1a239ffa345cdd236419cbd8b10273410610ae7d0f0f61fd36e1e9ccc3bbf32f895508cdca4fb57a0e04f78dd88469b33bf64a32a  apk.zsh-completion
f92e2e0c062b71e1e5cf95f0d4997166ccc7f7a5e45af8d1650f5951a1d552d89217c8c60d24f31fa626e8e0675c5e882e6b36ef1af8f7624e54627b22801381  s6-linux-init.patch"
