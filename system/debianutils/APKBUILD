# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=debianutils
pkgver=4.11.2
pkgrel=0
pkgdesc="Miscellaneous utilities from Debian"
url="https://packages.qa.debian.org/d/debianutils.html"
arch="all"
options="!check"  # No test suite.
license="BSD-3-Clause AND GPL-2.0+"
depends="coreutils mawk"  # awk, cat, and rm are required by add-shell
makedepends="grep"  # early package, declare these
subpackages="$pkgname-doc $pkgname-which::noarch"
# find timestamp here: https://snapshot.debian.org/package/debianutils/
source="https://snapshot.debian.org/archive/debian/20200927T204254Z/pool/main/d/$pkgname/${pkgname}_${pkgver}.tar.xz"
builddir="$srcdir/debianutils"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

which() {
	provider_priority=10
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/which "$subpkgdir"/usr/bin/
	# This will emit a warning about docs on abuild, but this is expected.
	mkdir -p "$subpkgdir"/usr/share/man/man1
	mv "${pkgdir}-doc"/usr/share/man/man1/which* "$subpkgdir"/usr/share/man/man1/
}

sha512sums="0bd9098beee78b3c8dae839f0c29e9f142cbb22f2ced473cf7ae47a14d9493ba882c1829eba213780392a87a3223b3689729754c8ded80a091efaef3f6f903fd  debianutils_4.11.2.tar.xz"
