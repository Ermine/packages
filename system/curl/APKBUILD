# Contributor: Sergey Lukin <sergej.lukin@gmail.com>
# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=curl
pkgver=8.11.0
pkgrel=0
pkgdesc="A URL retrival utility and library"
url="https://curl.haxx.se"
arch="all"
license="MIT"
depends="ca-certificates"
makedepends_build="perl"
makedepends_host="libssh2-dev nghttp2-dev openssl-dev zlib-dev zstd-dev"
makedepends="$makedepends_build $makedepends_host"
source="https://curl.haxx.se/download/$pkgname-$pkgver.tar.xz
	errorcodes.pl
	"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-dev libcurl"

# secfixes:
#   8.0.1-r0:
#     - CVE-2023-27538
#     - CVE-2023-27536
#     - CVE-2023-27535
#     - CVE-2023-27534
#     - CVE-2023-27533
#     - CVE-2023-23916
#     - CVE-2023-23915
#     - CVE-2023-23914
#   7.79.1-r0:
#     - CVE-2021-22947
#     - CVE-2021-22946
#     - CVE-2021-22945
#   7.78.0-r0:
#     - CVE-2021-22925
#     - CVE-2021-22924
#     - CVE-2021-22923
#     - CVE-2021-22922
#     - CVE-2021-22898
#     - CVE-2021-22890
#     - CVE-2021-22876
#   7.66.0-r0:
#     - CVE-2019-5481
#     - CVE-2019-5482
#   7.65.1-r0:
#     - CVE-2019-5435
#     - CVE-2019-5436
#   7.64.0-r0:
#     - CVE-2019-3823
#     - CVE-2019-3822
#     - CVE-2018-16890
#     - CVE-2018-16842
#     - CVE-2018-16840
#     - CVE-2018-16839
#   7.60.0-r0:
#     - CVE-2017-8816
#     - CVE-2017-8817
#     - CVE-2017-8818
#     - CVE-2018-1000005
#     - CVE-2018-1000007
#     - CVE-2018-1000120
#     - CVE-2018-1000121
#     - CVE-2018-1000122
#     - CVE-2018-1000300
#     - CVE-2018-1000301
#   7.56.1-r0:
#     - CVE-2017-1000257
#   7.55.0-r0:
#     - CVE-2017-1000099
#     - CVE-2017-1000100
#     - CVE-2017-1000101
#   7.54.0-r0:
#     - CVE-2017-7468
#   7.53.1-r2:
#     - CVE-2017-7407
#   7.53.0:
#     - CVE-2017-2629
#   7.52.1:
#     - CVE-2016-9594
#   7.51.0:
#     - CVE-2016-8615
#     - CVE-2016-8616
#     - CVE-2016-8617
#     - CVE-2016-8618
#     - CVE-2016-8619
#     - CVE-2016-8620
#     - CVE-2016-8621
#     - CVE-2016-8622
#     - CVE-2016-8623
#     - CVE-2016-8624
#     - CVE-2016-8625
#   7.50.3:
#     - CVE-2016-7167
#   7.50.2:
#     - CVE-2016-7141
#   7.50.1:
#     - CVE-2016-5419
#     - CVE-2016-5420
#     - CVE-2016-5421
#   7.36.0:
#     - CVE-2014-0138
#     - CVE-2014-0139

prepare() {
	default_prepare

	cp "$srcdir"/errorcodes.pl "$builddir"/tests/errorcodes.pl
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-ipv6 \
		--enable-unix-sockets \
		--with-libssh2 \
		--without-libidn2 \
		--disable-ldap \
		--with-pic \
		--with-openssl \
		--with-nghttp2 \
		--without-libpsl
	make
}

check() {
	# -p: print log contents on test failure
	# ignore:  557: fails under valgrind only
	make check TFLAGS='-p ~557'
}

package() {
	make DESTDIR="$pkgdir" install
}

libcurl() {
	pkgdesc="The multiprotocol file transfer library"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/lib "$subpkgdir"/usr
}

sha512sums="3a642d421e0a5c09ecb681bea18498f2c6124e9af4d8afdc074dfb85a9b0211d8972ade9cf00ab44b5dfed9303262cd83551dd3b5e0976d11fc19da3c4a0987e  curl-8.11.0.tar.xz
7848b1271e0bfe3be40221fb0582712d4af3ce1e1bdf16b5f0cac731d81bda145efc039f945a311af70caff279a44435a8ead6bb6e1db7570a4bd22df0a77fdb  errorcodes.pl"
